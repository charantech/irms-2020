using Irms.Infrastructure.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Irms.Tests.Unit
{
    [TestClass]
    public class PhoneNumberValidatorTest
    {
        [TestMethod]
        public void CanValidateNumbers()
        {
            var sut = new PhoneNumberValidator();
            var r1a = sut.Validate("+380-50-123-12-12", "UA").isValid;
            var r1b = sut.Validate("+38(050) 123 12 12", "UA").isValid;
            var r2a = sut.Validate("+31 (0)24 820 0034", "UA").isValid;

            var r3 = sut.Validate("sdasdsadasddasd", "UA").isValid;
            var r4 = sut.Validate("+9663004804192").isValid;
            var r6 = sut.Validate("+121321321321312312321123123", "UA").isValid;

            var s1 = sut.CheckPhoneValidation("+9663004804192");
            var s2 = sut.CheckPhoneValidation("+966559962983");

            Assert.IsTrue(r1a && r1b && r2a && s2);
            Assert.IsFalse(r3 || r4 || r6 || s1);
        }

        [TestMethod]
        public void CanFormatNumbers()
        {
            var sut = new PhoneNumberValidator();
            var r1 = sut.Validate("+38(050) 207-94-04", "SA").number.Number;
            var r3 = sut.Validate("+380502079404", "UA").number.Number;

            var valid = "+380502079404";
            Assert.AreEqual(r1, valid);
            Assert.AreEqual(r3, valid);
        }

        [TestMethod]
        public void CanValidatePhoneBulk()
        {
            var sut = new PhoneNumberValidator();

            var list = new List<string>() { "+9663004804192", "+380-50-123-12-12", "+966559962983" };
            var result = sut.ValidateBulk(list);

            var first = result.First();
            var last = result.Last();
            Assert.AreEqual(first.isValid, false);
            Assert.AreEqual(last.isValid, true);
        }
    }
}
