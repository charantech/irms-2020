﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.WebApi.AdminControllers.Settings;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Product.Queries;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Product.ReadModels;
using Irms.WebApi.AdminControllers;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class DictionaryControllerTests
    {
        /// <summary>
        /// get country list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCountryList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var items = new[]
            {
                new CountryItem(id,"Pakistan","PK"),
                new CountryItem(Guid.NewGuid(),"Saudi arabia","SA"),
            };
            IEnumerable<CountryItem> countryListItems = new List<CountryItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCountries>(), CancellationToken.None))
                .Returns((GetCountries q, CancellationToken t) => Task.FromResult(countryListItems));

            var sut = new DictionaryController(mediator.Object);

            //Act
            var result = await sut.GetCountries(CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

            var country = result.First();
            Assert.IsNotNull(country.Id);
            Assert.IsNotNull(country.Value);

            Assert.AreEqual(country.Id, id);

        }

        /// <summary>
        /// get products
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetProductsList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            var id = Guid.NewGuid();

            var items = new[]
            {
                new ProductListItem(id,"Product 1", 10,0,10,1,"test license", 20),
                new ProductListItem(Guid.NewGuid(),"rsvp", 10,0,10,1,"test license", 20),
            };

            IEnumerable<ProductListItem> itemslist = new List<ProductListItem>(items);
            IPageResult<ProductListItem> list = new PageResult<ProductListItem>(itemslist, 2);
            mediator
                .Setup(x => x.Send(It.IsAny<GetProductListQuery>(), CancellationToken.None))
                .Returns((GetProductListQuery q, CancellationToken t) => Task.FromResult(list));

            var sut = new ProductController(mediator.Object);

            var request = new GetProductListQuery(1, 10, "", Data.Read.TimeFilterRange.AllTime);
            //Act
            var result = await sut.GetProductList(request, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Any());

            var country = result.Items.First();
            Assert.IsNotNull(country.Id);
            Assert.IsNotNull(country.Title);

            Assert.AreEqual(country.Id, id);
        }

        /// <summary>
        /// get tenants list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantsList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var items = new[]
            {
                new DictionaryItem(id,"tenant 1"),
                new DictionaryItem(Guid.NewGuid(),"tenant 2"),
            };
            IEnumerable<DictionaryItem> list = new List<DictionaryItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetTenants>(), CancellationToken.None))
                .Returns((GetTenants q, CancellationToken t) => Task.FromResult(list));

            var sut = new DictionaryController(mediator.Object);

            //Act
            var result = await sut.GetTenants(CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

            var country = result.First();
            Assert.IsNotNull(country.Id);
            Assert.IsNotNull(country.Value);

            Assert.AreEqual(country.Id, id);
        }

        /// <summary>
        /// can get event types
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetEventTypes()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            short id = 1;

            var items = new[]
            {
                new EventTypeItem(id,"eventtype1"),
                new EventTypeItem(2,"eventtype2"),
            };
            IEnumerable<EventTypeItem> list = new List<EventTypeItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetEventTypes>(), CancellationToken.None))
                .Returns((GetEventTypes q, CancellationToken t) => Task.FromResult(list));

            var sut = new DictionaryController(mediator.Object);

            //Act
            var result = await sut.GetEventTypes(CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

            var eventType = result.First();
            Assert.IsNotNull(eventType.Id);
            Assert.IsNotNull(eventType.Value);

            Assert.AreEqual(eventType.Id, id);
        }

        /// <summary>
        /// can get time zones
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void CanGetTimeZones()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var sut = new DictionaryController(mediator.Object);

            //Act
            var result = sut.GetTimeZones(CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

            var eventType = result.First();
            Assert.IsNotNull(eventType.Id);
            Assert.IsNotNull(eventType.Value);

            Assert.IsTrue(result.Count() > 100);
        }

        /// <summary>
        /// get campaign criteria types
        /// </summary>
        [TestMethod]
        public async Task CanGetCampaignCriteriaType()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            byte id = 1;

            var items = new[]
            {
                new CampaignCriteriaTypeListItem(id,"eventtype1"),
                new CampaignCriteriaTypeListItem(2,"eventtype2"),
            };
            IEnumerable<CampaignCriteriaTypeListItem> list = new List<CampaignCriteriaTypeListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignCriteriaType>(), CancellationToken.None))
                .Returns((GetCampaignCriteriaType q, CancellationToken t) => Task.FromResult(list));

            var sut = new DictionaryController(mediator.Object);

            //Act
            var result = await sut.GetCampaignCriteriaType(CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

            var ct = result.First();
            Assert.IsNotNull(ct.Id);
            Assert.IsNotNull(ct.Value);
        }


        /// <summary>
        /// can get event guest lists
        /// </summary>
        [TestMethod]
        public async Task CanGetEventGuestLists()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            Guid id = Guid.NewGuid();

            var items = new[]
            {
                new DictionaryItem(id,"list1"),
                new DictionaryItem(Guid.NewGuid(),"list2"),
            };

            IEnumerable<DictionaryItem> list = new List<DictionaryItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetEventGuestList>(), CancellationToken.None))
                .Returns((GetEventGuestList q, CancellationToken t) => Task.FromResult(list));

            var sut = new DictionaryController(mediator.Object);

            //Act
            var result = await sut.GetEventGuestList(id, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

            var ct = result.First();
            Assert.IsNotNull(ct.Id);
            Assert.IsNotNull(ct.Value);
            Assert.AreEqual(ct.Value, "list1");
        }
    }
}
