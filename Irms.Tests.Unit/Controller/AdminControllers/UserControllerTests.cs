﻿using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Application.UsersInfo.Commands;
using Irms.Data.IdentityClasses;
using Irms.Data.Read.UserInfo.Queries;
using Irms.Data.Read.UserInfo.ReadModels;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.WebApi.AdminControllers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class UserControllerTests
    {

        private readonly IMediator _mediator = new Mock<IMediator>().Object;
        [TestMethod]
        public async Task CanGetUserInfo()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();
            var mediator = new Mock<IMediator>();

            var response = new MyProfile(
                "full name",
                "+380972197945",
                "test@gmail.com",
                0);

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };

            mediator
               .Setup(x => x.Send(It.IsAny<GetMyProfile>(), CancellationToken.None))
               .Returns((GetMyProfile q, CancellationToken t) => Task.FromResult(response));

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            var userI = new Mock<ClaimsPrincipal>();

            var sut = new UserController(mediator.Object, userManager.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = userI.Object
                    }
                }
            };

            //Act
            var apiResponse = await sut.Get(CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResponse);
            Assert.AreEqual(apiResponse, response);
        }


        [TestMethod]
        public async Task CanUpdateUserInfo()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();
            var mediator = new Mock<IMediator>();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };

            mediator
               .Setup(x => x.Send(It.IsAny<UpdateUserMainInfoCmd>(), CancellationToken.None))
               .Returns((UpdateUserMainInfoCmd q, CancellationToken t) => Task.FromResult(true));

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            var userI = new Mock<ClaimsPrincipal>();

            var sut = new UserController(mediator.Object, userManager.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = userI.Object
                    }
                }
            };

            var request = new UpdateUserMainInfoCmd(
                "first name",
                "last name",
                 Domain.Entities.Gender.Male,
                DateTime.Now.AddYears(-20),
                "+380972197945",
                "j.doe@example.com",
                "pasport",
                "national id",
                "search text");

            //Act
            var apiResponse = await sut.UpdateUserMainInfo(request, CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResponse);
            Assert.IsTrue(apiResponse);
        }
    }
}
