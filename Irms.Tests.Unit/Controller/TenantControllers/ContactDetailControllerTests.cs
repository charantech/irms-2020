﻿using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.ReadModels;
using Irms.Data.Read.CustomField.ReadModels;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.WebApi.TenantControllers;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Controller.TenantControllers
{
    [TestClass]
    public class ContactDetailControllerTests
    {
        [TestMethod]
        public async Task FetchPersonalDetailsTest()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var personalDetailsReadModel = new PersonalDetailsReadModel
            {
                Id = Guid.NewGuid(),
                DateAdded = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
                FullName = "Full Name",
                Status = "Pending"
            };

            mediator.Setup(x => x.Send(It.IsAny<GetPersonalDetailsQuery>(), CancellationToken.None))
                .Returns((GetPersonalDetailsQuery q, CancellationToken t) => Task.FromResult(personalDetailsReadModel));

            // Act
            var ctr = new ContactDetailController(mediator.Object);
            var result = await ctr.GetPersonalDetails(new GetPersonalDetailsQuery(string.Empty, Guid.NewGuid()), CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(personalDetailsReadModel.Id, result.Id);
            Assert.AreEqual(personalDetailsReadModel.DateAdded, result.DateAdded);
            Assert.AreEqual(personalDetailsReadModel.DateUpdated, result.DateUpdated);
            Assert.AreEqual(personalDetailsReadModel.FullName, result.FullName);
            Assert.AreEqual(personalDetailsReadModel.Status, result.Status);
        }

        [TestMethod]
        public async Task FetchOrganizationInfo()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var organizationReadModel = new OrganizationReadModel
            {
                Id = Guid.NewGuid(),
                Organization = "123",
                Position = "123"
            };

            mediator.Setup(x => x.Send(It.IsAny<GetOrganizationDetailsQuery>(), CancellationToken.None))
                .Returns((GetOrganizationDetailsQuery q, CancellationToken t) => Task.FromResult(organizationReadModel));

            // Act
            var ctr = new ContactDetailController(mediator.Object);
            var result = await ctr.GetOrganizationInfo(Guid.NewGuid(), CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(organizationReadModel.Id, result.Id);
            Assert.AreEqual(organizationReadModel.Organization, result.Organization);
            Assert.AreEqual(organizationReadModel.Position, result.Position);
        }

        [TestMethod]
        public async Task FetchPersonalInfo()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var personalReadModel = new PersonalInfoReadModel
            {
                Id = Guid.NewGuid(),
                AlternativeEmail = "email@email.com",
                Email = "email@email.com",
                ExpirationDate = DateTime.UtcNow,
                DocumentNumber = "123",
                DocumentTypeId = 0,
                FullName = "Full Name",
                Gender = "male",
                IssuingCountryId = null,
                MobileNumber = "+1230481230481234",
                NationalityId = null,
                PreferredName = "Pref Name",
                SecondaryMobileNumber = "+3214-12349",
                Title = "123",
                WorkNumber = "+2213421341234"
            };

            mediator.Setup(x => x.Send(It.IsAny<GetPersonalInfoQuery>(), CancellationToken.None))
                .Returns((GetPersonalInfoQuery q, CancellationToken t) => Task.FromResult(personalReadModel));

            // Act
            var ctr = new ContactDetailController(mediator.Object);
            var result = await ctr.GetPersonalInfo(Guid.NewGuid(), CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(personalReadModel.Id, result.Id);
            Assert.AreEqual(personalReadModel.AlternativeEmail, result.AlternativeEmail);
            Assert.AreEqual(personalReadModel.Email, result.Email);
            Assert.AreEqual(personalReadModel.ExpirationDate, result.ExpirationDate);
            Assert.AreEqual(personalReadModel.DocumentNumber, result.DocumentNumber);
            Assert.AreEqual(personalReadModel.DocumentTypeId, result.DocumentTypeId);
            Assert.AreEqual(personalReadModel.FullName, result.FullName);
            Assert.AreEqual(personalReadModel.Gender, result.Gender);
            Assert.AreEqual(personalReadModel.IssuingCountryId, result.IssuingCountryId);
            Assert.AreEqual(personalReadModel.MobileNumber, result.MobileNumber);
            Assert.AreEqual(personalReadModel.NationalityId, result.NationalityId);
            Assert.AreEqual(personalReadModel.PreferredName, result.PreferredName);
            Assert.AreEqual(personalReadModel.SecondaryMobileNumber, result.SecondaryMobileNumber);
            Assert.AreEqual(personalReadModel.Title, result.Title);
            Assert.AreEqual(personalReadModel.WorkNumber, result.WorkNumber);
        }

        [TestMethod]
        public async Task FetchCustomFields()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var customFieldReadModel = new Irms.Data.Read.DataModule.ReadModels.CustomFieldReadModel
                (
                    new List<CustomFieldEntryModel>
                    {
                        new CustomFieldEntryModel
                        {
                         Id = Guid.NewGuid(),
                         Title = "title",
                         CustomFieldType = 0,
                         MaxValue = 5,
                         MinValue = 4,
                         Value = "123"
                        }
                    }
                );

            mediator.Setup(x => x.Send(It.IsAny<GetCustomFieldsQuery>(), CancellationToken.None))
                .Returns((GetCustomFieldsQuery q, CancellationToken t) => Task.FromResult(customFieldReadModel));

            // Act
            var ctr = new ContactDetailController(mediator.Object);
            var result = await ctr.GetCustomFields(Guid.NewGuid(), CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Fields.Count());
            Assert.AreEqual(customFieldReadModel.Fields.First().Id, result.Fields.First().Id);
            Assert.AreEqual(customFieldReadModel.Fields.First().Title, result.Fields.First().Title);
            Assert.AreEqual(customFieldReadModel.Fields.First().CustomFieldType, result.Fields.First().CustomFieldType);
            Assert.AreEqual(customFieldReadModel.Fields.First().MaxValue, result.Fields.First().MaxValue);
            Assert.AreEqual(customFieldReadModel.Fields.First().MinValue, result.Fields.First().MinValue);
            Assert.AreEqual(customFieldReadModel.Fields.First().Value, result.Fields.First().Value);
        }

        [TestMethod]
        public async Task FetchAssociatedFields()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var associatedList = new List<AssociatedListReadModel>
            {
                new AssociatedListReadModel
                {
                    Id = Guid.NewGuid(),
                    ListName = "123"
                }
            };

            mediator.Setup(x => x.Send(It.IsAny<GetAssociatedListQuery>(), CancellationToken.None))
                .Returns((GetAssociatedListQuery q, CancellationToken t) => Task.FromResult((IEnumerable<AssociatedListReadModel>)associatedList));

            // Act
            var ctr = new ContactDetailController(mediator.Object);
            var result = await ctr.GetAssociatedLists(Guid.NewGuid(), CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, associatedList.Count());
            Assert.AreEqual(associatedList.First().Id, result.First().Id);
            Assert.AreEqual(associatedList.First().ListName, result.First().ListName);
        }

        [TestMethod]
        public async Task CanUpdateGlobalContactPersonalInfo()
        {
            //Arrange
            var id = Guid.NewGuid();
            var mediator = new Mock<IMediator>();

            var personalReadModel = new PersonalInfoModel //PersonalInfoReadModel
            {
                Id = id,
                FullName = "Full_Name",
            };

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateGlobalContactPersonalInfoCmd>(), CancellationToken.None))
                .Returns((UpdateGlobalContactPersonalInfoCmd q, CancellationToken t) => Task.FromResult(personalReadModel));

            //Act
            var ctr = new ContactDetailController(mediator.Object);
            var result = await ctr.UpdateGlobalContactPersonalInfo(new UpdateGlobalContactPersonalInfoCmd
            {
                Id = id,
                AlternativeEmail = "email@email.com",
                Email = "email@email.com",
                ExpirationDate = DateTime.UtcNow,
                DocumentNumber = "123",
                DocumentTypeId = 0,
                FullName = "Full_Name",
                IssuingCountryId = null,
                MobileNumber = "+1230481230481234",
                NationalityId = null,
                PreferredName = "Pref Name",
                SecondaryMobileNumber = "+3214-12349",
                Title = "123",
                WorkNumber = "+2213421341234"
            }
            , CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.FullName, "Full_Name");
        }
    }
}
