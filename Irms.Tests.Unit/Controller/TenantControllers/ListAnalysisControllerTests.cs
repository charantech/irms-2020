﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Application.Campaigns.Commands;
using Irms.WebApi.TenantControllers;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Application.Campaigns.ReadModels;
using Irms.Data.Read.Abstract;
using System.Collections.Generic;
using System.Linq;
using Irms.Data.Read.ListAnalysis.ReadModels;
using Irms.Domain;
using Irms.Data.Read.ListAnalysis.Queries;
using Irms.Application.ListAnalysis.Commands;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class ListAnalysisControllerTests
    {
        /// <summary>
        /// can get list analysis important fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetListAnalysisImportantFields()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid().ToString();

            var liFields = new ListAnalysisImportantFields
            {
                ReservedFields = new Data.Read.ListAnalysis.ReadModels.Field("FullName", true).Enumerate(),
                CustomsFields = new Data.Read.ListAnalysis.ReadModels.Field(id, false).Enumerate(),
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetListAnalysisImportantFieldsQuery>(), CancellationToken.None))
                    .Returns((GetListAnalysisImportantFieldsQuery q, CancellationToken t) => Task.FromResult(liFields));

            var sut = new ListAnalysisController(mediator.Object);

            //Act
            var result = await sut.GetListAnalysisImportantFields(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.CustomsFields.First().Value, id);
            Assert.AreEqual(result.CustomsFields.First().IsRequired, false);
            Assert.AreEqual(result.ReservedFields.First().Value, "FullName");
            Assert.AreEqual(result.ReservedFields.First().IsRequired, true);
        }

        /// <summary>
        /// can create list analysis important fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateListAnalysisImportantFields()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var result = MediatR.Unit.Value;

            mediator
                .Setup(x => x.Send(It.IsAny<CreateListAnalysisImportantFieldsCmd>(), CancellationToken.None))
                .Returns((CreateListAnalysisImportantFieldsCmd q, CancellationToken t) => Task.FromResult(result));

            var sut = new ListAnalysisController(mediator.Object);

            //Act
            var apiResult = await sut.CreateListAnalysisImportantFields(
                new CreateListAnalysisImportantFieldsCmd(
                    Guid.NewGuid(),
                    new Application.ListAnalysis.Commands.Field { Value = "FullName", IsRequired = true }.Enumerate(),
                    new Application.ListAnalysis.Commands.Field { Value = Guid.NewGuid().ToString(), IsRequired = true }.Enumerate()
                ),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, result);
        }

        /// <summary>
        /// can get validation count
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetValidationsCount()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var result = new ValidationsCount
            {
                Errors = 4,
                FieldWarnings = 5,
                GuestWarnings = 7,
            };

            var qry = new GetValidationsCountQuery(
                Guid.NewGuid(),
                "FullName".Enumerate(),
                Guid.NewGuid().Enumerate()
            );

            mediator
                .Setup(x => x.Send(It.IsAny<GetValidationsCountQuery>(), CancellationToken.None))
                    .Returns((GetValidationsCountQuery q, CancellationToken t) => Task.FromResult(result));

            var sut = new ListAnalysisController(mediator.Object);

            //Act
            var apiResult = await sut.GetValidationsCount(
                qry,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.Errors, result.Errors);
            Assert.AreEqual(apiResult.FieldWarnings, result.FieldWarnings);
            Assert.AreEqual(apiResult.GuestWarnings, result.GuestWarnings);
        }

        /// <summary>
        /// can create list analysis important fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteListAnalysis()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var result = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteListAnalysisCmd>(), CancellationToken.None))
                .Returns((DeleteListAnalysisCmd q, CancellationToken t) => Task.FromResult(result));

            var sut = new ListAnalysisController(mediator.Object);

            //Act
            var apiResult = await sut.DeleteListAnalysis(
                new DeleteListAnalysisCmd
                {
                    EventId = Guid.NewGuid(),
                    GuestListId = Guid.NewGuid()
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, result);
        }

        /// <summary>
        /// can get list analysis state
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetListAnalysisState()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var result = new ListAnalysisState(
                Domain.Entities.ListAnalysisStep.ImportantFields,
                3,
                10);

            var qry = new GetListAnalysisStateQuery
            {
                ListId = Guid.NewGuid(),
                EventId = Guid.NewGuid()
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetListAnalysisStateQuery>(), CancellationToken.None))
                    .Returns((GetListAnalysisStateQuery q, CancellationToken t) => Task.FromResult(result));

            var sut = new ListAnalysisController(mediator.Object);

            //Act
            var apiResult = await sut.GetListAnalysisState(
                qry,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.ListAnalysisStep, result.ListAnalysisStep);
            Assert.AreEqual(apiResult.ListAnalysisPageNo, result.ListAnalysisPageNo);
            Assert.AreEqual(apiResult.ListAnalysisPageSize, result.ListAnalysisPageSize);
        }

        /// <summary>
        /// can create list analysis important fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanSetListAnalysisState()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var result = MediatR.Unit.Value;

            mediator
                .Setup(x => x.Send(It.IsAny<CreateListAnalysisImportantFieldsCmd>(), CancellationToken.None))
                .Returns((CreateListAnalysisImportantFieldsCmd q, CancellationToken t) => Task.FromResult(result));

            var sut = new ListAnalysisController(mediator.Object);

            //Act
            var apiResult = await sut.SetListAnalysisState(
                new SetListAnalysisStateCmd
                {
                    ListAnalysisStep = Domain.Entities.ListAnalysisStep.ImportantFields,
                    ListAnalysisPageNo = 3,
                    ListAnalysisPageSize = 10
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, result);
        }
    }
}