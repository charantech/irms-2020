﻿using Irms.Domain.Entities.Templates;
using System;

namespace Irms.Tests.Unit.DataBuilder.Template
{
    public class TemplateSelectionBuilder
    {
        public TemplateSelectionBuilder()
        {
            Id = Guid.NewGuid();
            IsEnabled = true;
            Type = TemplateType.EventAssignment;
            SmsTemplate = Guid.NewGuid();
            EmailTemplate = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public bool IsEnabled { get; set; }
        public TemplateType Type { get; set; }
        public Guid? SmsTemplate { get; set; }
        public Guid? EmailTemplate { get; set; }

        public static implicit operator Domain.Entities.Templates.TemplateSelection(TemplateSelectionBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Templates.TemplateSelection Build()
        {
            return new Domain.Entities.Templates.TemplateSelection(
                Id,
                IsEnabled,
                Type,
                SmsTemplate,
                EmailTemplate);
        }
    }
}
