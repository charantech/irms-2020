﻿using Irms.Domain.Entities;
using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class ContactReachabilityBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public bool WhatsApp { get; set; }
        public bool Sms { get; set; }
        public bool Email { get; set; }
        public DateTime? WhatsAppLastReachableOn { get; set; }
        public DateTime? SmsLastReachableOn { get; set; }
        public DateTime? EmailLastReachableOn { get; set; }


        public static implicit operator ContactReachability(ContactReachabilityBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.ContactReachability Build()
        {
            return new ContactReachability
            {
                Id = Guid.NewGuid(),
                ContactId = Guid.NewGuid(),
                WhatsApp = true,
                Sms = false,
                Email = false,
            };
        }

        public Data.EntityClasses.ContactReachability BuildEntity()
        {
            return new Data.EntityClasses.ContactReachability
            {
                Id = Guid.NewGuid(),
                ContactId = Guid.NewGuid(),
                WhatsApp = true,
                Sms = false,
                Email = false,
            };
        }
    }
}
