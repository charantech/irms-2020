﻿using Irms.Application.Campaigns.Commands;
using Irms.Domain.Entities;
using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class CampaignEmailTemplateBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string Subject { get; set; }
        public string Preheader { get; set; }
        public string SenderEmail { get; set; }
        public string Body { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }

        public static implicit operator CampaignEmailTemplate(CampaignEmailTemplateBuilder instance)
        {
            return instance.Build();
        }

        public CampaignEmailTemplate Build()
        {
            return new CampaignEmailTemplate
            {
                Id = Guid.NewGuid(),
                Subject = "TestSubject",
                Preheader = "PreHeader",
                SenderEmail = "sender@email.com",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };
        }

        public Data.EntityClasses.CampaignEmailTemplate BuildEntity()
        {
            return new Data.EntityClasses.CampaignEmailTemplate
            {
                Id = Guid.NewGuid(),
                Subject = "TestSubject",
                Preheader = "PreHeader",
                SenderEmail = "sender@email.com",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };
        }

        public UpdateCampaignEmailResponseFormCmd BuildUpdateEmailResponse()
        {
            return new UpdateCampaignEmailResponseFormCmd
            {
                Id = Guid.NewGuid(),
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
            };
        }
    }
}
