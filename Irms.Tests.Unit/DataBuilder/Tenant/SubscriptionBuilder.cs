﻿using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class SubscriptionBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ProductId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime ExpiryDateTime { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public static implicit operator Domain.Entities.Subscription(SubscriptionBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Subscription Build()
        {
            return new Domain.Entities.Subscription
            {
                Id = Guid.NewGuid(),
                IsActive = false,
                ProductId = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                StartDateTime = DateTime.UtcNow,
                ExpiryDateTime = DateTime.UtcNow.AddDays(30),
            };
        }
    }
}
