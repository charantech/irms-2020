﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.CommandHandlers;
using Irms.Application.Contact.Commands;
using Irms.Application.Events.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class CreateGlobalContactListHandlerTest
    {
        [TestMethod]
        public async Task CanCreateGlobalContactListHandler()
        {
            var mediator = new Mock<IMediator>();
            var contactListRepository = new Mock<IContactListRepository<Domain.Entities.ContactList, Guid>>();
            var mapper = new Mock<IMapper>();

            mediator
                .Setup(x => x.Publish(It.IsAny<GlobalContactListCreated>(), CancellationToken.None))
                .Returns((GlobalContactListCreated q, CancellationToken t) => MediatR.Unit.Task);

            var expectedResult = new ContactListBuilder().Build();
            mapper
                .Setup(x => x.Map<Domain.Entities.ContactList>(It.IsAny<CreateGlobalContactListCmd>()))
                .Returns(expectedResult);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var prodHandler = new CreateGlobalContactListHandler(tenant, contactListRepository.Object, mediator.Object, mapper.Object);
            var cmd = new CreateGlobalContactListCmd();


            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
        }

    }
}
