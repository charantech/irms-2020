﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.CommandHandlers;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class DeleteContactListHandlerTest
    {
        /// <summary>
        /// checks if can delete contact list handler, checks for result and also verifies that mediator published event
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteContactListHandler()
        {
            var mediator = new Mock<IMediator>();
            var contactListRepository = new Mock<IContactListRepository<Domain.Entities.ContactList, Guid>>();
            var mapper = new Mock<IMapper>();

            var id = Guid.NewGuid();

            contactListRepository
                .Setup(x => x.IsContactListAssignToACampaign(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid g, CancellationToken t) => Task.FromResult(false));

            contactListRepository
               .Setup(x => x.Delete(It.IsAny<Guid>(), CancellationToken.None))
               .Returns((Guid g, CancellationToken t) => MediatR.Unit.Task);

            mediator
                .Setup(x => x.Publish(It.IsAny<ContactListRemoved>(), CancellationToken.None))
                .Returns((ContactListRemoved l, CancellationToken t) => MediatR.Unit.Task);

            var tenant = TenantBasicInfoBuilder.BuildDefault();
            var eventId = Guid.NewGuid();
            var prodHandler = new DeleteContactListHandler(tenant, contactListRepository.Object, mediator.Object, mapper.Object);
            var cmd = new DeleteContactListCmd
            {
                Id = id
            };

            //Act
            var guid = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(guid);
            Assert.AreEqual(id, guid);

            mediator.Verify(x => x.Publish(It.IsAny<ContactListRemoved>(), CancellationToken.None));
        }
    }
}
