﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Events;
using Irms.Application.RfiForms.CommandHandlers;
using Irms.Application.RfiForms.Commands;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.RfiForm
{
    [TestClass]
    public class UpsertRfiFormHandlerTest
    {
        [TestMethod]
        public async Task UpsertRfiformhandler_Create()
        {
            //Arrange
            var repo = new Mock<IRfiFormRepository<Domain.Entities.RfiForm, Guid>>();
            var mediator = new Mock<IMediator>();
            var mapper = new Mock<IMapper>();
            var config = new Mock<IConfiguration>();

            Domain.Entities.RfiForm eRfi = null;
            var rfi = new RfiFormBuilder().Build();

            repo
                .Setup(x => x.Create(It.IsAny<Domain.Entities.RfiForm>(), CancellationToken.None))
                .Returns((Domain.Entities.RfiForm q, CancellationToken t) => Task.FromResult(rfi.Id));

            repo
                .Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(eRfi));

            mediator
                .Setup(x => x.Publish(It.IsAny<RfiFormUpserted>(), CancellationToken.None))
                .Returns((RfiFormUpserted q, CancellationToken t) => MediatR.Unit.Task);

            mapper
                .Setup(x => x.Map<Domain.Entities.RfiForm>(It.IsAny<UpsertRfiFormCmd>()))
                .Returns(rfi);

            config
                .SetupGet(x => x[It.Is<string>(s => s == "AzureStorage:BaseUrl")])
                .Returns(string.Empty);

            var cmd = new UpsertRfiFormCmd
            {
                Id = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                WelcomeHtml = "<p>welcome...</p>",
                SubmitHtml = "<button>save</button>",
                ThanksHtml = "<p>thanks...</p>",
                FormTheme = "<p>form theme...</button>",
                FormSettings = "<p>form settings</p>",
                ThemeBackgroundImage = "abc.png",
                QuestionJson = "[{\"id\":\"b64581ad-f7b6-4fe0-b6a2-e317a2fe5fe5\",\"question\":\"{\\\"type\\\":\\\"singleLineText\\\",\\\"id\\\":\\\"b64581ad-f7b6-4fe0-b6a2-e317a2fe5fe5\\\",\\\"headlineArabic\\\":\\\"\\\",\\\"headline\\\":\\\"test\\\",\\\"isDescription\\\":false,\\\"description\\\":\\\"question\\\",\\\"descriptionArabic\\\":null,\\\"required\\\":false,\\\"multiline\\\":false,\\\"minLength\\\":3,\\\"maxLength\\\":20,\\\"pattern\\\":\\\"\\\",\\\"mapping\\\":true,\\\"mappingField\\\":\\\"dateOfBirth\\\",\\\"jumps\\\":null}\",\"mapped\":true,\"mappedField\":\"dateOfBirth\",\"sortorder\":1}]"
            };

            var procHandler = new UpsertRfiFormHandler(repo.Object, mediator.Object, mapper.Object, config.Object);

            //Act
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task UpsertRfiformhandler_Update()
        {
            //Arrange
            var repo = new Mock<IRfiFormRepository<Domain.Entities.RfiForm, Guid>>();
            var mediator = new Mock<IMediator>();
            var mapper = new Mock<IMapper>();
            var config = new Mock<IConfiguration>();

            var rfi = new RfiFormBuilder().Build();

            repo
                .Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(rfi));

            repo
                .Setup(x => x.Update(It.IsAny<Domain.Entities.RfiForm>(), CancellationToken.None))
                .Returns((Domain.Entities.RfiForm q, CancellationToken t) => Task.FromResult(rfi.Id));

            mediator
                .Setup(x => x.Publish(It.IsAny<RfiFormUpserted>(), CancellationToken.None))
                .Returns((RfiFormUpserted q, CancellationToken t) => MediatR.Unit.Task);

            mapper
                .Setup(x => x.Map<Domain.Entities.RfiForm>(It.IsAny<UpsertRfiFormCmd>()))
                .Returns(rfi);

            config
                .SetupGet(x => x[It.Is<string>(s => s == "AzureStorage:BaseUrl")])
                .Returns(string.Empty);

            var cmd = new UpsertRfiFormCmd
            {
                Id = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                WelcomeHtml = "{\"content\":\"english content\",\"contentArabic\":\"arabic content\",\"button\":\"Proceed\",\"buttonArabic\":\"تقدم\"}",
                SubmitHtml = "<button>save</button>",
                ThanksHtml = "{\"content\":\"english content\",\"contentArabic\":\"arabic content\",\"button\":\"Proceed\",\"buttonArabic\":\"تقدم\"}",
                FormTheme = "<p>form theme...</button>",
                FormSettings = "<p>form settings</p>",
                ThemeBackgroundImage = "abc.png",
                QuestionJson = "[{\"id\":\"b64581ad-f7b6-4fe0-b6a2-e317a2fe5fe5\",\"question\":\"{\\\"type\\\":\\\"singleLineText\\\",\\\"id\\\":\\\"b64581ad-f7b6-4fe0-b6a2-e317a2fe5fe5\\\",\\\"headlineArabic\\\":\\\"\\\",\\\"headline\\\":\\\"test\\\",\\\"isDescription\\\":false,\\\"description\\\":\\\"question\\\",\\\"descriptionArabic\\\":null,\\\"required\\\":false,\\\"multiline\\\":false,\\\"minLength\\\":3,\\\"maxLength\\\":20,\\\"pattern\\\":\\\"\\\",\\\"mapping\\\":true,\\\"mappingField\\\":\\\"dateOfBirth\\\",\\\"jumps\\\":null}\",\"mapped\":true,\"mappedField\":\"dateOfBirth\",\"sortorder\":1}]"
            };

            var procHandler = new UpsertRfiFormHandler(repo.Object, mediator.Object, mapper.Object, config.Object);

            //Act
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(rfi.Id, result);
        }
    }

}
