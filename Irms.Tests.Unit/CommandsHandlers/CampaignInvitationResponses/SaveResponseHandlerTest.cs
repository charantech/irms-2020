﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitationResponses.CommandHandlers;
using Irms.Application.CampaignInvitationResponses.Commands;
using Irms.Application.CampaignInvitationResponses.Events;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitationResponses
{
    [TestClass]
    public class SaveResponseHandlerTest
    {
        [TestMethod]
        public async Task SaveResponseHandler_Test()
        {
            //Arrange
            var repo = new Mock<ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid>>();
            var invitationRepo = new Mock<ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid>>();
            var nextRun = new Mock<ICampaignInvitationNextRunRepository<Domain.Entities.CampaignInvitationNextRun, Guid>>();

            var mediator = new Mock<IMediator>();

            invitationRepo
                .Setup(x => x.LoadAcceptedOrRejectedInvitations(It.IsAny<Guid>(), It.IsAny<UserAnswer>(), CancellationToken.None))
                .Returns((Guid q, UserAnswer a, CancellationToken t) => Task.FromResult((IEnumerable<CampaignInvitation>)new List<CampaignInvitation>
                {
                    new CampaignInvitation
                    {
                        InvitationType = InvitationType.Accepted,
                     IsInstant = true
                    }
                }));

            invitationRepo
                .Setup(x => x.UpdateRange(It.IsAny<IEnumerable<CampaignInvitation>>(), CancellationToken.None))
                .Returns((IEnumerable<CampaignInvitation> eEndSize, CancellationToken t) => Task.CompletedTask);

            nextRun
                .Setup(x => x.CreateRange(It.IsAny<IEnumerable<CampaignInvitationNextRun>>(), CancellationToken.None))
                .Returns((IEnumerable<CampaignInvitationNextRun> QueryHookExtensions, CancellationToken t) => MediatR.Unit.Task);

            var entity = new Domain.Entities.CampaignInvitationResponse
            {
                Id = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                ContactId = Guid.NewGuid(),
                CreatedOn = DateTime.UtcNow,
                TenantId = Guid.NewGuid()
            };

            repo.Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(entity));

            repo.Setup(x => x.Update(It.IsAny<Domain.Entities.CampaignInvitationResponse>(), CancellationToken.None))
                .Returns((Domain.Entities.CampaignInvitationResponse q, CancellationToken t) => MediatR.Unit.Task);

            mediator.Setup(x => x.Publish(It.IsAny<ResponseSaved>(), CancellationToken.None))
                .Returns((ResponseSaved q, CancellationToken t) => MediatR.Unit.Task);

            var jobWorker = new Mock<IHangfireJobsWorker>();

            jobWorker
                .Setup(x => x.SetupAcceptedSending(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<DateTime>()));


            jobWorker
                .Setup(x => x.SetupRejectedSending(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<DateTime>()));





            var prodHandler = new SaveResponseHandler(mediator.Object, repo.Object, invitationRepo.Object, nextRun.Object, jobWorker.Object);
            var cmd = new SaveResponseCmd
            {
                Id = entity.Id,
                Answer = Domain.Entities.UserAnswer.ACCEPTED,
                MediaType = Domain.Entities.MediaType.Email
            };

            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
            Assert.AreEqual(entity.Id, id);
        }
    }
}
