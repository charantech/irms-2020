﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CustomFields.CommandHandlers;
using Irms.Application.CustomFields.Commands;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CustomFieldTest
{
    [TestClass]
    public class AddCustomFieldHandlerTest
    {
        /// <summary>
        /// Tries to create custom field in database
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task AddCustomFieldHandlerTest_Create()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var repo = new Mock<ICustomFieldRepository<CustomField, Guid>>();
            var mapper = new Mock<IMapper>();

            var data = new Domain.Entities.CustomField
            {
                MaxValue = 4,
                MinValue = 3,
                FieldName = "adsf123",
                CustomFieldType = CustomFieldType.Text
            };

            var guid = Guid.NewGuid();

            mediator
                .Setup(x => x.Publish(It.IsAny<AddCustomFieldCmd>(), CancellationToken.None))
                .Returns((AddCustomFieldCmd q, CancellationToken t) => Task.CompletedTask);

            repo
                .Setup(x => x.HasCustomFieldWithThisName(It.IsAny<string>(), CancellationToken.None))
                .Returns((string q, CancellationToken t) => Task.FromResult(false));

            repo
                .Setup(x => x.Create(It.IsAny<Domain.Entities.CustomField>(), CancellationToken.None))
                .Returns((CustomField q, CancellationToken t) => Task.FromResult(guid));

            //Act
            var cmd = new AddCustomFieldCmd
            {
                CustomFieldType = CustomFieldType.Text,
                MaxValue = 4,
                MinValue = 3,
                Name = "adsf123"
            };
            var procHandler = new AddCustomFieldHandler(mediator.Object, mapper.Object, repo.Object);
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, guid);
        }

    }
}
