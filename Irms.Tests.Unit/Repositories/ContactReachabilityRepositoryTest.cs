﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Irms.Tests.Unit.ValidData;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class ContactReachabilityRepositoryTest
    {
        /// <summary>
        /// test case to test get contact reachability by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.ContactReachability, Domain.Entities.ContactReachability>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var cr = new EntitiesValidData().GuestReachabilityEntityValidData();
            cr.ContactId = cr.Id;
            cr.TenantId = tenant.Id;

            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.ContactReachability.AddRangeAsync(cr);
                await db.SaveChangesAsync();
            }

            //act
            Domain.Entities.ContactReachability result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactReachabilityRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetByGuestId(cr.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Id);
            Assert.AreEqual(result.Email, false);
            Assert.AreEqual(result.Sms, false);
            Assert.AreEqual(result.WhatsApp, false);
        }

        /// <summary>
        /// can update contact Reachability info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateContactReachability()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var reachabilityEntity = new ContactReachabilityBuilder().BuildEntity();
            reachabilityEntity.ContactId = id;
            reachabilityEntity.TenantId = tenant.Id;

            var crToUpdate = new ContactReachabilityBuilder().Build();
            crToUpdate.ContactId = id;
            crToUpdate.TenantId = tenant.Id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.EventCampaign>(It.IsAny<Domain.Entities.Campaign>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.ContactReachability.AddRangeAsync(reachabilityEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.ContactReachability result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactReachabilityRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Update(crToUpdate, CancellationToken.None);
                result = await db.ContactReachability.FirstOrDefaultAsync(x => x.ContactId == id, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ContactId, id);
            Assert.AreEqual(result.WhatsApp, true);
            Assert.AreEqual(result.Sms, false);
            Assert.AreEqual(result.Email, false);
        }
    }
}
