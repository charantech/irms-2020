﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Tenant = Irms.Domain.Entities.Tenant.Tenant;
using Irms.Domain.Entities.Tenant;
using Irms.Domain.Entities;

namespace Irms.Tests.Unit.Repositories
{
    /// <summary>
    /// subscription info unit tests
    /// </summary>
    [TestClass]
    public class SubscriptionRepositoryTest
    {

        //create tenant/ customer test
        [TestMethod]
        public async Task CanCreate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Subscription, Data.EntityClasses.TenantSubscription>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var subscriptionId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(subscriptionId);



            var product = new EntitiesValidData().ProductEntityValidData();
            var productId = product.Id;

            using (var db = new IrmsDataContext(options))
            {
                await db.Product.AddRangeAsync(product);
                await db.SaveChangesAsync();
            }

            var subscriptionToCreate = new SubscriptionBuilder().Build();
            subscriptionToCreate.ProductId = productId;

            //act
            Guid resultId;
            Data.EntityClasses.TenantSubscription result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new SubscriptionRepository(db, mapper);
                resultId = await sut.Create(subscriptionToCreate, CancellationToken.None);
                result = await db.TenantSubscription.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.IsActive, false);
            Assert.AreEqual(result.ProductId, productId);
        }
    }
}
