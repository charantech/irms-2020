﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Tenant = Irms.Domain.Entities.Tenant.Tenant;
using Irms.Domain.Entities.Tenant;
using Irms.Domain.Entities;

namespace Irms.Tests.Unit.Repositories
{
    /// <summary>
    /// user info unit tests
    /// </summary>
    [TestClass]
    public class UserInfoRepositoryTest
    {
        /// <summary>
        /// can get user by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.UserInfo, UserInfo>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var userInfo = new EntitiesValidData().UserInfoEntityValidData();
            var userId = userInfo.Id;

            using (var db = new IrmsDataContext(options))
            {
                await db.UserInfo.AddRangeAsync(userInfo);
                await db.SaveChangesAsync();
            }

            //act
            UserInfo result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new UserInfoRepository(db, mapper);
                result = await sut.GetById(userId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, userId);
            Assert.AreEqual(result.Email, "employee@mail.net");
            Assert.AreEqual(result.NationalId, "01234567890");
            Assert.AreEqual(result.MobileNo, "+380952045485");
            Assert.AreEqual(result.PassportNo, "DG5338531");
        }

        /// <summary>
        /// can get tenant admin by tenant id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantAdminByTenantId()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.UserInfo, UserInfo>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new EntitiesValidData().TenantValidData();
            var tenantId = tenant.Id;

            var userInfo = new EntitiesValidData().UserInfoEntityValidData();
            userInfo.TenantId = tenantId;
            userInfo.RoleId = (int)RoleType.TenantAdmin;

            using (var db = new IrmsDataContext(options))
            {
                await db.Tenant.AddRangeAsync(tenant);
                await db.UserInfo.AddRangeAsync(userInfo);
                await db.SaveChangesAsync();
            }

            //act
            UserInfo result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new UserInfoRepository(db, mapper);
                result = await sut.GetTenantAdmin(tenantId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.TenantId, tenantId);
            Assert.AreEqual(result.Email, "employee@mail.net");
            Assert.AreEqual(result.NationalId, "01234567890");
            Assert.AreEqual(result.MobileNo, "+380952045485");
            Assert.AreEqual(result.PassportNo, "DG5338531");
        }

        //create tenant/ customer test
        [TestMethod]
        public async Task CanCreate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserInfo, Data.EntityClasses.UserInfo>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var userInfoEntity = new EntitiesValidData().UserInfoEntityValidData();
            userInfoEntity.Id = userId;

            var userToCreate = new UserInfoBuilder().Build();

            //act
            Guid resultId;
            Data.EntityClasses.UserInfo result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new UserInfoRepository(db, mapper);
                resultId = await sut.Create(userToCreate, CancellationToken.None);
                result = await db.UserInfo.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Email, "employee@mail.net");
            Assert.AreEqual(result.NationalId, "01234567890");
            Assert.AreEqual(result.MobileNo, "+380952045485");
            Assert.AreEqual(result.PassportNo, "DG5338531");
        }

        //create tenant/ customer test
        [TestMethod]
        public async Task CanUpdate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid userId = Guid.NewGuid();
            var mapper = new Mock<IMapper>();

            var userInfoEntity = new EntitiesValidData().UserInfoEntityValidData();
            userInfoEntity.Id = userId;

            var userInfoToUpdate = new UserInfoBuilder().Build();
            userInfoToUpdate.Id = userId;

            mapper
                .Setup(x => x.Map<Data.EntityClasses.UserInfo>(It.IsAny<UserInfo>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.UserInfo.AddRangeAsync(
                    userInfoEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.UserInfo result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new UserInfoRepository(db, mapper.Object);
                await sut.Update(userInfoToUpdate, CancellationToken.None);
                result = await db.UserInfo.FirstOrDefaultAsync(x => x.Id == userId, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, userId);
            Assert.AreEqual(result.Email, "employee@mail.net");
            Assert.AreEqual(result.NationalId, "01234567890");
            Assert.AreEqual(result.MobileNo, "+380952045485");
            Assert.AreEqual(result.PassportNo, "DG5338531");
        }
    }
}
