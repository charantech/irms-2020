﻿using Hangfire;
using Irms.Application;
using Irms.Application.CampaignInvitations;
using Irms.Background.Abstract;
using Irms.Data;
using Irms.Data.Read.BackgroundService.Queries;
using Irms.Data.Read.BackgroundService.QueryHandlers;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Background.Jobs
{
    [AutomaticRetry(Attempts = 0)]
    public class RejectedJob
    {
        private readonly IMediator _mediator;
        private readonly IServiceProvider _serviceProvider;
        private readonly IBackgroundJobsHelperService _backgroundJobsHelper;
        public RejectedJob(
            IMediator mediator,
            IBackgroundJobsHelperService backgroundJobsHelper,
            IServiceProvider serviceProvider)
        {
            _mediator = mediator;
            _backgroundJobsHelper = backgroundJobsHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task SendInvitation(Guid campaignInvitationId, Guid contactId, CancellationToken token)
        {
            using (IServiceScope scope = _serviceProvider.CreateScope())
            using (IrmsDataBackgroundJobContext _context = scope.ServiceProvider.GetRequiredService<IrmsDataBackgroundJobContext>())
            {
                // Load data
                var campaignInvitation = await _context.CampaignInvitation
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Active && x.Id == campaignInvitationId, token);

                var contact = await _context.Contact
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == contactId);

                var prefferedMedia = await _context.CampaignPrefferedMedia
                    .FirstOrDefaultAsync(x => x.EventCampaignId == campaignInvitation.EventCampaignId && x.ContactId == contactId, token);

                // If need to send at least email or sms
                if (prefferedMedia.Email || prefferedMedia.Sms || prefferedMedia.WhatsApp)
                {
                    // Load template
                    var template = await _backgroundJobsHelper.GetCampaignTemplates(campaignInvitationId, token);

                    // Load info about invitation for user
                    var guest = (await _mediator.Send(new GetAcceptedRejectedDataForContactQuery(campaignInvitationId, contactId), token)).First();

                    if (prefferedMedia.Email && template.EmailBody.IsNotNullOrEmpty())
                    {
                        var emailTemplate = new EventGuest
                        {
                            EmailAcceptedLink = guest.EmailAcceptedLink,
                            EmailRejectedLink = guest.EmailRejectedLink,
                            Email = guest.Email,
                            FullName = guest.FullName,
                            Id = guest.Id,
                            Organization = guest.Organization,
                            Phone = guest.Phone,
                            Position = guest.Position,
                            PreferredName = guest.PreferredName,
                            SenderName = template.EmailSender
                        };

                        await _backgroundJobsHelper.SendEmail(emailTemplate.Enumerate().ToList(), template, campaignInvitationId, InvitationType.Rejected, token);
                    }

                    if (prefferedMedia.Sms && template.SmsBody.IsNotNullOrEmpty())
                    {
                        var smsTemplate = new EventGuest
                        {
                            SmsAcceptedLink = guest.SmsAcceptedLink,
                            SmsRejectedLink = guest.SmsRejectedLink,
                            Email = guest.Email,
                            FullName = guest.FullName,
                            Id = guest.Id,
                            Organization = guest.Organization,
                            Phone = guest.Phone,
                            Position = guest.Position,
                            PreferredName = guest.PreferredName,
                            SenderName = guest.SenderName
                        };

                        await _backgroundJobsHelper.SendSms(smsTemplate.Enumerate().ToList(), template, campaignInvitationId, InvitationType.Rejected, token);
                    }

                    //whatsapp
                    if (prefferedMedia.WhatsApp && template.WhatsappBody.IsNotNullOrEmpty())
                    {
                        var whatsappTemplate = new EventGuest
                        {
                            WhatsappAcceptedLink = guest.WhatsappRejectedLink,
                            WhatsappRejectedLink = guest.WhatsappRejectedLink,
                            Email = guest.Email,
                            FullName = guest.FullName,
                            Id = guest.Id,
                            Organization = guest.Organization,
                            Phone = guest.Phone,
                            Position = guest.Position,
                            PreferredName = guest.PreferredName,
                            SenderName = guest.SenderName
                        };

                        await _backgroundJobsHelper.SendWhatsappMessage(whatsappTemplate.Enumerate().ToList(), template, campaignInvitationId, InvitationType.Accepted, token);
                    }
                }
            }
        }
    }
}
