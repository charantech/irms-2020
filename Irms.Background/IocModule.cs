﻿using Autofac;
using Irms.Application.Abstract;
using Irms.Background.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Background
{
    public class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<HangfireJobsWorker>().As<IHangfireJobsWorker>();
            builder.RegisterType<BackgroundJobsHelperService>().As<IBackgroundJobsHelperService>();
            //builder.RegisterType<UnifonicWebhookService>().As<IUnifonicWebhookService>();
            
        }
    }
}
