﻿using Irms.Application.CampaignInvitations;
using Irms.Background.ReadModels;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Domain.Entities.ContactListRFI;

namespace Irms.Background.Abstract
{
    public interface IBackgroundJobsHelperService
    {
        Task<CampaignTemplates> GetCampaignTemplates(Guid invitationId, CancellationToken token);
        Task<IEnumerable<CampaignTemplates>> GetCampaignTemplates(IEnumerable<Guid> invitationIds, CancellationToken token);
        Task ChangeInvitationStatus(Guid campaignInvitationId, InvitationStatus status, CancellationToken token);
        IEnumerable<CampaignInvitationMessageLog> RsvpPendingGuestsToUpdate(IEnumerable<Application.BackgroundService.ReadModels.GuestsListItem> guests, Guid tenantId, Guid campaignInvitationId);
        Task ChangeRsvpPendingGuestsStatus(IEnumerable<CampaignInvitationMessageLog> guests, CancellationToken token);
        Task<bool> SendContactListRfiEmail(PrefferedMediaReadModel prefference, string subject, string body, Guid tenantId, CancellationToken token);
        Task<bool> SendContactListRfiSms(PrefferedMediaReadModel prefference, string body, Guid tenantId, CancellationToken token);
        Task<bool> SendSms(List<EventGuest> egs, CampaignTemplates templates, Guid invitationId, InvitationType type, CancellationToken token);
        Task<bool> SendEmail(List<EventGuest> ege, CampaignTemplates templates, Guid invitationId, InvitationType type, CancellationToken token);
        IEnumerable<CampaignInvitationNextRun> AcceptedRejectedGuestsToUpdate(IEnumerable<Data.Read.BackgroundService.ReadModels.AcceptedRejectedContactInvitationListItem> guests, Guid tenantId, Guid campaignInvitationId);
        Task ChangeAcceptedRejectedGuestsStatus(IEnumerable<CampaignInvitationNextRun> guests, CancellationToken token);
        Task<ContactListRFI> FindContactListRfi(Guid id, CancellationToken token);
        Task UpdateContactRfiFormStatus(Guid id, ContactListRfiStatus status, CancellationToken token);
        Task<IEnumerable<PrefferedMediaReadModel>> FetchPrefferedMediasFromContactListRfiForm(Guid contactListId, CancellationToken token);
        Task<IEnumerable<ContactListRFIAnswer>> CreateContactListRfiAnswers(IEnumerable<PrefferedMediaReadModel> guests, ContactListRFI rfi, CancellationToken token);
        Task<bool> SendWhatsappMessage(List<EventGuest> egw, CampaignTemplates templates, Guid invitationId, InvitationType type, CancellationToken token);
    }
}
