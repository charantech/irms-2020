﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Background.ReadModels
{
    public class AcceptedRejectedContactInvitationListItem
    {
        public Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string PreferredName { get; set; }
        public string Email { get; set; }
        public bool EmailPreferred { get; set; }
        public string Phone { get; set; }
        public bool SmsPreferred { get; set; }
        public string FullName { get; set; }
        public string AcceptedLink { get; set; }
        public string RejectedLink { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
    }
}
