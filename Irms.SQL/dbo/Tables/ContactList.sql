﻿CREATE TABLE [dbo].[ContactList] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [Name]         NVARCHAR (100)   NOT NULL,
    [IsGlobal]     BIT              NOT NULL,
    [IsGuest]      BIT              CONSTRAINT [DF_ContactList_IsGuest] DEFAULT ((0)) NOT NULL,
    [EventId]      UNIQUEIDENTIFIER NULL,
    [CreatedById]  UNIQUEIDENTIFIER NULL,
    [CreatedOn]    DATETIME         NOT NULL,
    [ModifiedById] UNIQUEIDENTIFIER NULL,
    [ModifiedOn]   DATETIME         NULL,
    CONSTRAINT [PK_ContactList] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ContactList_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);






