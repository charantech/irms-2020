﻿CREATE TABLE [dbo].[TemplateSelection] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [TypeId]          INT              NOT NULL,
    [EmailTemplateId] UNIQUEIDENTIFIER NULL,
    [SmsTemplateId]   UNIQUEIDENTIFIER NULL,
    [IsEnabled]       BIT              NOT NULL,
    CONSTRAINT [PK_TemplateSelection] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_TemplateSelection_Template_Email] FOREIGN KEY ([EmailTemplateId], [TenantId]) REFERENCES [dbo].[Template] ([Id], [TenantId]),
    CONSTRAINT [FK_TemplateSelection_Template_Sms] FOREIGN KEY ([SmsTemplateId], [TenantId]) REFERENCES [dbo].[Template] ([Id], [TenantId]),
    CONSTRAINT [FK_TemplateSelection_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);

