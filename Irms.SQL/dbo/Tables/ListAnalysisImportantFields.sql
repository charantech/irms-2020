﻿CREATE TABLE [dbo].[ListAnalysisImportantFields] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [GuestListId] UNIQUEIDENTIFIER NOT NULL,
    [FieldType]   INT              NOT NULL,
    [Field]       NVARCHAR (50)    NOT NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [CreatedById] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_ListAnalysisImportantFields] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ListAnalysisImportantFields_ContactList] FOREIGN KEY ([GuestListId], [TenantId]) REFERENCES [dbo].[ContactList] ([Id], [TenantId])
);

