﻿create table CampaignInvitationResponse
(
	Id uniqueidentifier NOT NULL,
	TenantId uniqueidentifier NOT NULL,
	CampaignInvitationId uniqueidentifier NOT NULL,
	ContactId uniqueidentifier NOT NULL,
	ResponseMediaType int NULL,
	Answer int NULL,
	ResponseDate datetime NULL,
	CreatedOn datetime NOT NULL,
	CONSTRAINT PK_CampaignInvitationResponse PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
	CONSTRAINT FK_CampaignInvitationResponse_Invitation FOREIGN KEY (CampaignInvitationId, TenantId) REFERENCES campaigninvitation (id, tenantId),
	CONSTRAINT FK_CampaignInvitationResponse_Contact FOREIGN KEY (contactId, TenantId) REFERENCES contact (id, tenantId)
)