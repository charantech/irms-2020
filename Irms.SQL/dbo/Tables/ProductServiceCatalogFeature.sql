﻿CREATE TABLE [dbo].[ProductServiceCatalogFeature] (
    [Id]                         UNIQUEIDENTIFIER NOT NULL,
    [ProductId]                  UNIQUEIDENTIFIER NOT NULL,
    [ServiceCatalogId]           UNIQUEIDENTIFIER NOT NULL,
    [ServiceCatalogFeatureId]    UNIQUEIDENTIFIER NOT NULL,
    [ServiceCatalogFeatureCount] INT              NULL,
    CONSTRAINT [PK_ProductServiceCatalogFeature] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ProductServiceCatalogFeature_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id]),
    CONSTRAINT [FK_ProductServiceCatalogFeature_ServiceCatalog] FOREIGN KEY ([ServiceCatalogId]) REFERENCES [dbo].[ServiceCatalog] ([Id]),
    CONSTRAINT [FK_ProductServiceCatalogFeature_ServiceCatalogFeature] FOREIGN KEY ([ServiceCatalogFeatureId]) REFERENCES [dbo].[ServiceCatalogFeature] ([Id])
);

