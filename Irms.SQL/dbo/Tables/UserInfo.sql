﻿CREATE TABLE [dbo].[UserInfo] (
    [Id]         UNIQUEIDENTIFIER CONSTRAINT [Employee_Id_default] DEFAULT (newid()) NOT NULL,
    [TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [RoleId]     INT              NOT NULL,
    [FirstName]  NVARCHAR (50)    NOT NULL,
    [LastName]   NVARCHAR (50)    NOT NULL,
    [FullName]   AS               ((isnull([FirstName],'')+' ')+isnull([LastName],'')),
    [GenderId]   INT              NULL,
    [BirthDate]  DATETIME         NULL,
    [MobileNo]   NVARCHAR (50)    NOT NULL,
    [Email]      NVARCHAR (50)    NOT NULL,
    [NationalId] NVARCHAR (50)    NULL,
    [PassportNo] NVARCHAR (50)    NULL,
    [SearchText] NVARCHAR (1000)  NULL,
    [IsActive]   BIT              NOT NULL,
    [UserId]     UNIQUEIDENTIFIER NULL,
    [CreatedOn]  DATETIME         NOT NULL,
    [UpdatedOn]  DATETIME         NULL,
    [CreatedBy]  UNIQUEIDENTIFIER NULL,
    [UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_Employee_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_Employee_AspNetUsers] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Employee_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);



