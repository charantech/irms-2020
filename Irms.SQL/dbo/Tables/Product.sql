﻿CREATE TABLE [dbo].[Product] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [Title]              NVARCHAR (100)   NOT NULL,
    [Price]              DECIMAL (18, 2)  NOT NULL,
    [DiscountPercentage] DECIMAL (5, 2)   DEFAULT ((0)) NULL,
    [DiscountAmount]     AS               (([Price]*[DiscountPercentage])/(100)) PERSISTED,
    [FinalPrice]         AS               ([Price]-([Price]*[DiscountPercentage])/(100)) PERSISTED,
    [CurrencyId]         TINYINT          NOT NULL,
    [LicensePeriodId]    TINYINT          NOT NULL,
    [LicensePeriodDays]  INT              NOT NULL,
    [IsShowOnWeb]        BIT              CONSTRAINT [DF_Product_ShowOnWeb] DEFAULT ((0)) NOT NULL,
    [IsDeleted]          BIT              DEFAULT ((0)) NULL,
    [CreatedById]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]          DATETIME         DEFAULT (getdate()) NULL,
    [ModifiedById]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME         DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Product_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_Product_LicensePeriod] FOREIGN KEY ([LicensePeriodId]) REFERENCES [dbo].[LicensePeriod] ([Id])
);




GO

CREATE TRIGGER ProductModifiedDate  
ON [dbo].[Product] 
AFTER UPDATE   
AS 
BEGIN

  IF @@rowcount = 0
    RETURN;

  IF UPDATE(ModifiedOn)
    RETURN;

  ELSE
     UPDATE [dbo].[Product] SET ModifiedOn = CURRENT_TIMESTAMP WHERE Id in (SELECT DISTINCT Id FROM Inserted);

END 
GO  
