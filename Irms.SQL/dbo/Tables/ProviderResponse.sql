﻿CREATE TABLE [dbo].[ProviderResponse] (
    [Id]                             UNIQUEIDENTIFIER NOT NULL,
    [TenantId]                       UNIQUEIDENTIFIER NOT NULL,
    [ContactId]                      UNIQUEIDENTIFIER NOT NULL,
    [CampaignInvitationMessageLogId] UNIQUEIDENTIFIER NOT NULL,
    [MessageStatus]                  INT              NOT NULL,
    [ProviderType]                   INT              NOT NULL,
    [ResponseDate]                   DATETIME         NOT NULL,
    [ResponseJson]                   NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ProviderResponse] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ProviderResponse_CampaignInvitationMessageLog] FOREIGN KEY ([CampaignInvitationMessageLogId], [TenantId]) REFERENCES [dbo].[CampaignInvitationMessageLog] ([Id], [TenantId]),
    CONSTRAINT [FK_ProviderResponse_Contact] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId])
);



