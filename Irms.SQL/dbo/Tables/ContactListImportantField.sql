﻿CREATE TABLE [dbo].[ContactListImportantField]
(
    Id UNIQUEIDENTIFIER NOT NULL,
    TenantId UNIQUEIDENTIFIER NOT NULL,
    ContactListId UNIQUEIDENTIFIER NOT NULL,
    FieldName NVARCHAR(50) NOT NULL,
    CustomFieldId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_ContactListImportantField PRIMARY KEY CLUSTERED (Id ASC, TenantId ASC),
    CONSTRAINT FK_ContactListImportantField_Tenant FOREIGN KEY (TenantId) REFERENCES [dbo].[Tenant] (Id),
    CONSTRAINT [FK_ContactListImportantField_ContactList] FOREIGN KEY (ContactListId, [TenantId]) REFERENCES [dbo].[ContactList] ([Id], [Tenantid]),
    CONSTRAINT [FK_ContactListImportantField_CustomField] FOREIGN KEY (CustomFieldId, [TenantId]) REFERENCES [dbo].CustomField ([Id], [Tenantid])
)
