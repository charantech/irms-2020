﻿CREATE TABLE [dbo].[CustomField] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [FieldName]       NVARCHAR (100)   NOT NULL,
    [CustomFieldType] INT              NOT NULL,
    [MinValue]        BIGINT           NULL,
    [MaxValue]        BIGINT           NULL,
    [CreatedOn]       DATETIME         NOT NULL,
    [CreatedById]     UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]      DATETIME         NULL,
    [ModifiedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CustomField] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CustomField_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);

