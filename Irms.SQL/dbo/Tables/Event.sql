﻿CREATE TABLE [dbo].[Event] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [TenantId]           UNIQUEIDENTIFIER NOT NULL,
    [Name]               NVARCHAR (200)   NOT NULL,
    [ImagePath]          NVARCHAR (200)   NULL,
    [EventTypeId]        SMALLINT         NULL,
    [IsLimitedAttendees] BIT              NOT NULL,
    [MaximumAttendees]   INT              NULL,
    [OverflowAttendees]  INT              NULL,
    [StartDateTime]      DATETIME         NOT NULL,
    [EndDateTime]        DATETIME         NOT NULL,
    [TimeZoneUtcOffset]  NVARCHAR (10)    NOT NULL,
    [TimeZoneName]       NVARCHAR (100)   NOT NULL,
    [IsActive]           BIT              CONSTRAINT [DF_Event_IsActive] DEFAULT ((0)) NOT NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_Event_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedById]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_Event_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedById]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME         NULL,
    CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_Event_EventType] FOREIGN KEY ([EventTypeId]) REFERENCES [dbo].[EventType] ([Id])
);



