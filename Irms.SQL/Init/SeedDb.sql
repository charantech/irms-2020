BEGIN TRAN Seed;

-- TODO: Tenant Admin
DECLARE @tenantId UNIQUEIDENTIFIER
SET @tenantId = 'FE211A87-C4F0-40AB-AD9B-9C6EF63579DA'

DECLARE @departmentId UNIQUEIDENTIFIER
SET @departmentId = '4CFCFF15-312A-45AF-8462-949DF52D79B3'

-- Tenant
INSERT INTO dbo.Tenant (ID, CreateDate, Description, ExpiryDate, IsActive, IsDeleted, Name, UpdateDate, AdminId, IsDefault) VALUES (@tenantId, '2017-12-12 10:59:18.117', 'The default tenant', '2020-12-12 10:59:23.097', 1, 0, 'Default', '2017-12-12 10:59:44.117', null, 1);

-- Department
INSERT INTO dbo.Department (Id, TenantId, ClientURL, IsActive, IsDefault, IsDeleted, Name, EmailFrom, SendGridApiKey,TwilioAccountSid,TwilioAuthToken ,TwilioNotificationServiceId ,LogoId, HeaderId, FooterId, UpdateDate, AdminId) VALUES (@departmentId, @tenantId, 'default.takamul.com', 1, 1, 0, 'default','defaultDepartment@takamul.com', null,null,null,null, null, null, null, '2017-12-12 10:59:44.117', null);

-- Tenant Modules
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('6914D40A-520A-4E94-8D02-146510417DCD', @tenantId, 0);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('8FA237E7-4160-4601-96E3-2E83E51ACD35', @tenantId, 1);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('D7DBC327-915E-4D08-A029-6EE21DC85CC6', @tenantId, 2);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('72455EDC-665D-450D-85CB-6AA49C5328DC', @tenantId, 3);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('C052EE8A-E84B-479F-BF9A-8BFB58AF8A0D', @tenantId, 4);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('FEAF4727-CE5E-4341-9D9B-7711E7DD6AE9', @tenantId, 5);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('2893D291-75FB-4D45-93D8-8361B5F62CB4', @tenantId, 6);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('2E2E0235-C80F-4DB1-B82C-56D9A0A86AE3', @tenantId, 7);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('70E741E9-9C38-4DBB-82B0-EB54322341C2', @tenantId, 8);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('6F8C6B2B-79DE-463F-98E0-E1E19BAFC7AA', @tenantId, 9);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('BEA6F1CC-D283-40C4-BB6D-F864A0C23F17', @tenantId, 10);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('6DEC8D20-AAE2-4F13-B705-324FA7B7B318', @tenantId, 11);
INSERT INTO dbo.TenantToSystemModule (ID, TenantID, SystemModuleID) VALUES ('68299D01-4E04-46C2-9B31-93F5EA8A2EB9', @tenantId, 12);

-- Roles
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('2D3B90AA-7D94-4EF1-E53D-08D54152B6C2', '42e7abb1-ffb0-46a4-81e0-41ea83b23feb', 'SuperAdmin', 'SUPERADMIN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('CE27BDEC-82C6-496D-B0E1-8C7CFE33DC20', '6F6F6D67-14FF-4989-ADE1-8F23AEB036D0', 'TenantAdmin', 'TENANTADMIN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('89C7BED7-D2FC-41A1-8EA4-21B738CC4D1C', '44088379-78C2-4394-B8D3-D2438A428A76', 'EventAdmin', 'EVENTADMIN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('EA4A0EC2-B5B3-4102-8491-FF5209EF3EBE', '134FE4CC-3628-4607-8255-1443C4EF68D9', 'AreaAdmin', 'AREAADMIN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('708025D3-0513-47B7-9B5C-BF30E55350DB', '909469DA-A921-4E9B-A71C-A569D7587719', 'ContentAdmin', 'CONTENTADMIN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('ED8EE32B-6668-43BF-AB4F-37D3DA9B507B', '1FC1F540-EDAD-4BB3-97AF-4501B4415F19', 'ReadOnlyAdmin', 'READONLYADMIN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('0FE7BF19-FB40-4454-ADA3-1AED047539E6', 'ADA7A721-E930-4CF9-8F20-883BD0833549', 'Interviewer', 'INTERVIEWER');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('0D71FC88-2A74-4B33-91C6-46F4789F1282', '2285D3BC-DC2C-45B2-A142-AF069E04AC8C', 'Physician', 'PHYSICIAN');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('864314B8-FE55-4662-9866-E59FCA0B4AE1', '1B71EF95-F477-4991-A841-7BEE40C188EB', 'Volunteer', 'VOLUNTEER');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('C1C325A0-B5DF-4340-8927-FF4F5453AEF4', '63879EAC-09B6-4033-B004-9151B2AE94B3', 'Trainer', 'TRAINER');
INSERT INTO dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES ('17A3AAC0-AC8C-478C-835A-D455E7032C34', '9CBACD9D-1A56-437E-8244-8880EE11251E', 'DepartmentAdmin', 'DEPARTMENTADMIN');

--Users
INSERT INTO dbo.AspNetUsers (Id, AccessFailedCount, ConcurrencyStamp, Email, EmailConfirmed, IsSuperAdmin, LockoutEnabled, LockoutEnd, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UserName, TenantId, DepartmentId, IsEnabled) VALUES ('25E3ECA3-FEEC-4A6C-65B9-08D54153E5E7', 0, '5dcd7a22-d907-4145-a19f-3da61d9a8b02', 'sadmin@example.com', 1, 1, 1, null, 'SADMIN@EXAMPLE.COM', 'SADMIN@EXAMPLE.COM', 'AQAAAAEAACcQAAAAEBNqrOdwg0Qy3KcbJY92UmYRGLBU4OzsX1kRvh4fzRzuRQoiOckQWASN0xPoHiZf0Q==', '+380501231212', 0, '029a19a9-9b40-450a-ae06-fc052729eea2', 0, 'sadmin@example.com', @tenantId, @departmentId, 1);
INSERT INTO dbo.AspNetUsers (Id, AccessFailedCount, ConcurrencyStamp, Email, EmailConfirmed, IsSuperAdmin, LockoutEnabled, LockoutEnd, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UserName, TenantId, DepartmentId, IsEnabled) VALUES ('6E04B582-3A73-4976-B065-90E369C9E02D', 0, '7ac94154-f621-417b-a743-cd06e03fe44a', 'phy1@gmail.com', 1, 0, 1, null, 'PHY1@GMAIL.COM', 'PHY1@GMAIL.COM', 'AQAAAAEAACcQAAAAEGFnp5u3LUHkLGsPUr2fQt+/HYIwtfqohfowjRg78DiqSD29dZNTWdj2typ/huV8tw==', '+380501234567', 1, 'd9bfce45-b81b-485c-95e1-e32879e42235', 0, 'phy1@gmail.com', @tenantId, @departmentId, 1);

-- User Roles
INSERT INTO dbo.AspNetUserRoles (UserId, RoleId) VALUES ('25E3ECA3-FEEC-4A6C-65B9-08D54153E5E7', '2D3B90AA-7D94-4EF1-E53D-08D54152B6C2'); -- sadmin = super admin
INSERT INTO dbo.AspNetUserRoles (UserId, RoleId) VALUES ('6E04B582-3A73-4976-B065-90E369C9E02D', '0D71FC88-2A74-4B33-91C6-46F4789F1282'); -- phy1 = physician

-- Employee
INSERT INTO dbo.Employee (Id, TenantId, DepartmentId, RoleId, FullName, GenderId, BirthDate, PhoneNo, Email, MessagingHandle, IsActive, UserId, AvatarId, IsDeleted) VALUES ('3AE6D177-1897-4C2C-97F9-FB0336E642A4', @tenantId, @departmentId, 9, 'Admin Admin  2344', 0, '2016-01-26 22:00:00.000', '+380501234517', 'sadmin@example.com', '123', 1, '25E3ECA3-FEEC-4A6C-65B9-08D54153E5E7', null, 0);
INSERT INTO dbo.Employee (Id, TenantId, DepartmentId, RoleId, FullName, GenderId, BirthDate, PhoneNo, Email, MessagingHandle, IsActive, UserId, AvatarId, IsDeleted) VALUES ('17184661-1E05-41D6-A5C9-01794C772626', @tenantId, @departmentId, 3, 'Physician No 1', 0, '1982-01-10 11:13:00.273', '+380501234567', 'phy1@gmail.com', '', 1, '6E04B582-3A73-4976-B065-90E369C9E02D', null, 0 );

-- Languages
INSERT INTO dbo.Language (Id, TenantId, Culture, Name) VALUES ('C454253B-4197-4EEB-BC3E-96F2915B04AD', @tenantId, 'ar-SA', 'Arabic');
INSERT INTO dbo.Language (Id, TenantId, Culture, Name) VALUES ('2C99A21A-F1D4-4EA8-8BA9-C8BA90C64E2A', @tenantId, 'en-US', 'English');

-- City
INSERT INTO dbo.City (Id, TenantId, Name) VALUES ('5DEA1A8F-B20E-41A6-9BCB-82FE6E9F31C2', @tenantId, 'Riyadh');
INSERT INTO dbo.City (Id, TenantId, Name) VALUES ('44D6DE04-0814-42CB-BFFD-C0588E3CBC44', @tenantId, 'Ney York');

INSERT INTO dbo.CityTranslation (Id, TenantId, CityId, LanguageId, Translation) VALUES ('C03C351B-1C47-4A10-8F38-718B51E1C16B', @tenantId, '44D6DE04-0814-42CB-BFFD-C0588E3CBC44', '2C99A21A-F1D4-4EA8-8BA9-C8BA90C64E2A', N'New York');
INSERT INTO dbo.CityTranslation (Id, TenantId, CityId, LanguageId, Translation) VALUES ('87C76949-DEA5-452A-8645-81C5ED152E65', @tenantId, '5DEA1A8F-B20E-41A6-9BCB-82FE6E9F31C2', '2C99A21A-F1D4-4EA8-8BA9-C8BA90C64E2A', N'Riyadh');
INSERT INTO dbo.CityTranslation (Id, TenantId, CityId, LanguageId, Translation) VALUES ('0BA8DEB5-985F-4346-91D8-93338B22DED9', @tenantId, '44D6DE04-0814-42CB-BFFD-C0588E3CBC44', 'C454253B-4197-4EEB-BC3E-96F2915B04AD', N'نيويورك');
INSERT INTO dbo.CityTranslation (Id, TenantId, CityId, LanguageId, Translation) VALUES ('B5DF1201-2D41-430D-9BFC-CB66DB265E72', @tenantId, '5DEA1A8F-B20E-41A6-9BCB-82FE6E9F31C2', 'C454253B-4197-4EEB-BC3E-96F2915B04AD', N'الرياض');

-- Default registration types
INSERT INTO dbo.RegistrationType (Id, TenantId, Name, Description, IsActive, IsDeleted) VALUES (NEWID(), @tenantId, 'Open Registration', 'On-site or Online registration', 1, 0);
INSERT INTO dbo.RegistrationType (Id, TenantId, Name, Description, IsActive, IsDeleted) VALUES (NEWID(), @tenantId, 'Closed registration', 'Admin will upload a guest list and request for RSVP', 1, 0);

-- HomepageSection
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 0, 'Home', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 1, 'Blog', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 2, 'JoinUs', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 3, 'About', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 4, 'Events', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 5, 'Training', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 6, 'Gallery', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 7, 'Contact', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');
INSERT INTO dbo.HomepageSection (Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate) VALUES (NEWID(),  @tenantId, @departmentId, 8, 'Footer', '', 1, '3AE6D177-1897-4C2C-97F9-FB0336E642A4', '2018-03-08 18:18:39.677');


COMMIT TRAN Seed;