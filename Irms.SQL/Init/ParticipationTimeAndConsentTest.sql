DECLARE @tenantId UNIQUEIDENTIFIER
SET @tenantId = 'FE211A87-C4F0-40AB-AD9B-9C6EF63579DA'

DECLARE @departmentId UNIQUEIDENTIFIER
SET @departmentId = '4CFCFF15-312A-45AF-8462-949DF52D79B3'


-- Add Consent text into homepage sections
INSERT INTO HomepageSection
(Id, TenantId, DepartmentId, TypeId, Title, Content, IsVisible, AuthorId, LastUpdate)
VALUES
  (
    NEWID(),
    'FE211A87-C4F0-40AB-AD9B-9C6EF63579DA', /*Default*/
    '4CFCFF15-312A-45AF-8462-949DF52D79B3', /*Default*/
    9,
    'MedicalConsent',
    '',
    1,
    '3AE6D177-1897-4C2C-97F9-FB0336E642A4',
    '2018-03-08 18:18:39.677'
  );

-- Migrate participation time of existing volunteers

/* Evening: 2 -> 3 */
UPDATE Volunteer
SET ParticipationTimeId = 3 /* New Evening value */
WHERE ParticipationTimeId = 2 /* Old Evening Value */