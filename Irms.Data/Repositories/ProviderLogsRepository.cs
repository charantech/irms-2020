﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;

namespace Irms.Data.Repositories
{
    public class ProviderLogsRepository : IProviderLogsRepository<ProviderLogs, Guid>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;

        public ProviderLogsRepository(IrmsDataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Guid> Create(Domain.Entities.ProviderLogs entity, CancellationToken token)
        {
            var data = _mapper.Map<EntityClasses.ProviderLogs>(entity);
            data.ProviderType = (int)entity.ProviderType;
            await _context.ProviderLogs.AddAsync(data, token);
            await _context.SaveChangesAsync(token);

            return data.Id;
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task<Domain.Entities.ProviderLogs> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task Update(Domain.Entities.ProviderLogs entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }
    }
}