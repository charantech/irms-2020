﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using System.Collections.Generic;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Irms.Application;
using Irms.Domain;
using Irms.Application.CampaignInvitations;

namespace Irms.Data.Repositories
{
    public class BackgroundServiceRepository : IBackgroundServiceRepository<CampaignInvitation, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private static string DefaultResponse = @"Sorry... {{user_input}} is an invalid response to the invitation. Please reply back with the corresponding RSVP code. (You can find the RSVP code by scrolling to the top of the invitation message)";
        public BackgroundServiceRepository(IrmsDataContext context, IMapper mapper)
        {
            _dataContext = context;
            _mapper = mapper;
        }

        public async Task ChangeGuestsStatus(IEnumerable<CampaignInvitationMessageLog> guests, CancellationToken token)
        {
            var inv = await _dataContext.CampaignInvitationMessageLog
                .Where(x => guests.Select(y => y.ContactId).Contains(x.ContactId))
                .ToListAsync(token);

            var (toAdd, _, toUpd) = inv.Compare(guests, (o, n) => o.ContactId == n.ContactId
            && o.CampaignInvitationId == n.CampaignInvitationId
            && o.MessageStatus == n.MessageStatus
            && o.InvitationTemplateTypeId == n.InvitationTemplateTypeId);

            //add
            //var logs = _mapper.Map<List<EntityClasses.CampaignInvitationMessageLog>>(guests);
            await _dataContext.CampaignInvitationMessageLog.AddRangeAsync(
                toAdd.Select(x => _mapper.Map<EntityClasses.CampaignInvitationMessageLog>(x)),
                token);

            //update
            foreach (var (o, n) in toUpd)
            {
                o.TimeStamp = DateTime.UtcNow;
            }

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task ChangeInvitationStatus(Guid campaignInvitationId, InvitationStatus status, CancellationToken token)
        {
            var inv = await _dataContext.CampaignInvitation
                .FirstOrDefaultAsync(x => x.Id == campaignInvitationId, token);

            inv.Status = (int)status;
            inv.ModifiedOn = DateTime.UtcNow;

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task<CampaignInvitationMessageLog> GetMessageById(string messageSid, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationMessageLog.FirstOrDefaultAsync(x => x.MessageId == messageSid, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Message log Id is incorrect");
            }
            var result = _mapper.Map<EntityClasses.CampaignInvitationMessageLog, CampaignInvitationMessageLog>(data);
            return result;
        }

        public async Task LogMessages(List<CampaignInvitationMessageLog> logs, CancellationToken token)
        {
            var data = _mapper.Map<List<EntityClasses.CampaignInvitationMessageLog>>(logs);
            await _dataContext.CampaignInvitationMessageLog.AddRangeAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
        }

        #region webhooks
        public async Task<IEnumerable<CampaignInvitationMessageLog>> GetMessagesByEmails(IEnumerable<string> messageIds, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationMessageLog
                .Where(x => messageIds.Contains(x.MessageId))
                .ToListAsync(token);

            if (data == null)
            {
                throw new IncorrectRequestException("Message log Id is incorrect");
            }
            var result = _mapper.Map<List<CampaignInvitationMessageLog>>(data);
            return result;
        }
        public async Task LogProviderResponse(ProviderResponse response, CancellationToken token)
        {
            var data = _mapper.Map<EntityClasses.ProviderResponse>(response);
            await _dataContext.ProviderResponse.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task<InstantBotResponse> GetInvitationResponse(string from, string body, CancellationToken token)
        {
            string code = body.Trim().ToUpper();
            var campaign = await _dataContext.EventCampaign
                .FirstOrDefaultAsync(x => x.AcceptCode == code
                || x.RejectCode == code, token);

            var contact = await _dataContext.Contact
                .FirstOrDefaultAsync(x => x.MobileNumber == from.Replace("whatsapp:", string.Empty), token);

            if (contact == null)
            {
                throw new IncorrectRequestException("Invalid response from Whatsapp BOT");
            }

            if (campaign == null)
            {
                return new InstantBotResponse
                {
                    HasInvalidCode = true,
                    Body = DefaultResponse.Replace("{{user_input}}", body),
                    TenantId = contact.TenantId,
                    To = contact.MobileNumber,
                    ContactId = contact.Id
                };
            }

            var log = await _dataContext.CampaignInvitationMessageLog
            .Include(x => x.CampaignInvitation)
            .Where(x => x.Contact.MobileNumber == from.Replace("whatsapp:", string.Empty)
            && x.InvitationTemplateTypeId == (int)InvitationTemplateType.WhatsApp
            && (x.CampaignInvitation.EventCampaign.AcceptCode == body
            || x.CampaignInvitation.EventCampaign.RejectCode == body))
            .OrderByDescending(x => x.TimeStamp)
            .FirstOrDefaultAsync(token);

            var template = await _dataContext.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == log.CampaignInvitationId, token);

            var resp = await _dataContext.CampaignInvitationResponse
                .FirstOrDefaultAsync(x => x.Contact.MobileNumber == from.Replace("whatsapp:", string.Empty)
                && x.CampaignInvitationId == log.CampaignInvitationId, token);

            var mapResponse = _mapper.Map<CampaignInvitationResponse>(resp);
            mapResponse.Answer = (UserAnswer?)resp.Answer;
            mapResponse.ResponseMediaType = (MediaType?)resp.ResponseMediaType;

            UserAnswer answer = code == campaign.AcceptCode ? UserAnswer.ACCEPTED : UserAnswer.REJECTED;
            return new InstantBotResponse
            {
                HasInvalidCode = false,
                Body = code == campaign.AcceptCode ? template.AcceptHtml : template.RejectHtml,
                Answer = answer,
                TenantId = contact.TenantId,
                To = contact.MobileNumber,
                ContactId = contact.Id,
                InvitationResponse = mapResponse,
                CampaignInvitationId = log.CampaignInvitationId
            };

        }

        public async Task UpdateInvitationResponse(CampaignInvitationResponse entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationResponse
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Campaign invitation response id is incorrect");
            }

            data.Answer = (int?)entity.Answer;
            data.ResponseMediaType = (int?)entity.ResponseMediaType;
            data.ResponseDate = DateTime.UtcNow;
            await _dataContext.SaveChangesAsync(token);
        }
        #endregion

        #region not implemented methods
        public Task Update(Domain.Entities.CampaignInvitation entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task<Guid> Create(CampaignInvitation entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task<CampaignInvitation> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
