﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.ReadModels;
using Irms.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Irms.Data.Repositories
{
    public class ContactRepository : IContactRepository<Domain.Entities.Contact, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;
        private readonly IDatabaseQueryExecutor _queryExecutor;

        private const string ContactsToGuestsQuery = @"
            UPDATE contact SET isGuest = 1
            FROM contact c
            INNER JOIN ContactListToContacts cltc on cltc.ContactId = c.Id
            INNER JOIN ContactList cl on cl.Id = cltc.ContactListId
            INNER JOIN EventCampaign ec on ec.GuestListId = cl.Id
            WHERE
            ec.Id = '{0}'";

        private const string ContactListsQuery = @"
            UPDATE ContactList SET isGuest = 1, isGlobal = 1
            FROM ContactList cl
            INNER JOIN EventCampaign ec on ec.GuestListId = cl.Id
            WHERE
            ec.Id = '{0}'";

        public async Task CheckPrefferedMedias(Guid id, CancellationToken token)
        {
            var result = new bool[3];

            var eventCampaign = await _dataContext.EventCampaign
                .FirstOrDefaultAsync(x => x.Id == id, token);

            var contacts = await _dataContext.ContactListToContacts
                .AsNoTracking()
                .Include(x => x.Contact)
                .Include(x => x.ContactList)
                .Where(x => x.ContactList.Id == eventCampaign.GuestListId)
                .Select(x => x.ContactId)
                .ToListAsync(token);

            var medias = await _dataContext.CampaignPrefferedMedia
                .AsNoTracking()
                .Where(x => contacts.Contains(x.ContactId) && x.EventCampaignId == eventCampaign.Id)
                .ToListAsync(token);

            if (!medias.Any())
            {
                throw new IncorrectRequestException("Please select the mediums through which you prefer to reach your guests");
            }

            if (!medias.All(x => x.Email || x.Sms || x.WhatsApp))
            {
                throw new IncorrectRequestException("Please select the mediums through which you prefer to reach your guests");

            }
        }

        public async Task<IEnumerable<ContactPrefferedMedia>> GetContactsByCampaignId(Guid id, Guid campaignId, CancellationToken token)
        {
            var contacts = await _dataContext.ContactListToContacts
                .AsNoTracking()
                .Include(x => x.Contact)
                .Include(x => x.ContactList)
                .Where(x => x.ContactList.Id == id)
                .Select(x => x.ContactId)
                .ToListAsync(token);

            var prefferedMedias = await _dataContext.CampaignPrefferedMedia
                .AsNoTracking()
                .Where(x => contacts.Contains(x.ContactId) && x.EventCampaignId == campaignId)
                .Select(x => new ContactPrefferedMedia
                {
                    Id = x.Id,
                    Email = x.Email,
                    Sms = x.Sms,
                    WhatsApp = x.WhatsApp,
                    ContactId = x.ContactId
                })
                .ToListAsync();

            return prefferedMedias;
        }

        public ContactRepository(IrmsDataContext dataContext,
            IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant,
            IDatabaseQueryExecutor queryExecutor)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
            _queryExecutor = queryExecutor;
        }

        /// <summary>
        /// create contact
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(Domain.Entities.Contact entity, CancellationToken token)
        {
            var data = _mapper.Map<Domain.Entities.Contact, EntityClasses.Contact>(entity);
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;

            // save to db
            await _dataContext.Contact.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        public async Task CreateRange(IEnumerable<Domain.Entities.Contact> entities, CancellationToken token)
        {
            var data = _mapper.Map<IEnumerable<EntityClasses.Contact>>(entities).ToList();
            data.ForEach(x =>
            {
                x.CreatedById = _user.Id;
                x.CreatedOn = DateTime.UtcNow;
            });

            _dataContext.Contact.AddRange(data);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task UpdateRange(IEnumerable<Domain.Entities.Contact> entities, CancellationToken token)
        {
            var data = _mapper.Map<IEnumerable<EntityClasses.Contact>>(entities).ToList();
            data.ForEach(x =>
            {
                x.ModifiedById = _user.Id;
                x.ModifiedOn = DateTime.UtcNow;
            });

            _dataContext.Contact.UpdateRange(data);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// update contacts with custom fields
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdateContactWithCustomFields(Domain.Entities.Contact entity, CancellationToken token)
        {
            var data = _mapper.Map<EntityClasses.Contact>(entity);
            data.ModifiedById = _user.Id;
            data.ModifiedOn = DateTime.UtcNow;

            _dataContext.Contact.Update(data);

            //update custom fields
            var (toAdd, _, toUpd) = _dataContext.ContactCustomField.Compare(entity.CustomFields, (o, n) =>
            {
                return o.ContactId == n.ContactId
                && o.CustomFieldId == n.CustomFieldId
                && o.TenantId == _tenant.Id;
            });

            //add
            toAdd.Where(x => !string.IsNullOrEmpty(x.Value)).ToList()
                .ForEach(x =>
                {
                    var d = _mapper.Map<EntityClasses.ContactCustomField>(x);
                    d.Id = Guid.NewGuid();
                    d.TenantId = _tenant.Id;
                    d.CreatedById = _user.Id;
                    d.CreatedOn = DateTime.UtcNow;

                    _dataContext.ContactCustomField.Add(d);
                });

            //update
            foreach (var up in toUpd)
            {
                up.o.Value = up.n.Value;
                up.o.ModifiedById = _user.Id;
                up.o.ModifiedOn = DateTime.UtcNow;
            }

            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// delete contact
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// delete contacts by ids
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Delete(List<Guid> ids, CancellationToken token)
        {
            var contacts = await _dataContext.Contact
                .Where(x => ids.Contains(x.Id))
                .ToListAsync(token);

            foreach (var contact in contacts)
            {
                contact.Active = false;
                contact.Deleted = true;
            }

            await _dataContext.SaveChangesAsync();
        }

        /// <summary>
        /// get contact by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Domain.Entities.Contact> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Contact.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id, token);
            var result = _mapper.Map<EntityClasses.Contact, Domain.Entities.Contact>(data);
            return result;
        }

        /// <summary>
        /// Fetches contacts by ids
        /// </summary>
        /// <param name="ids">contact ids</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Domain.Entities.Contact>> GetByIds(IEnumerable<Guid> ids, CancellationToken token)
        {
            var data = await _dataContext.Contact
                .AsNoTracking()
                .Where(x => ids.Contains(x.Id))
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<Domain.Entities.Contact>>(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Domain.Entities.Contact> GetContactWithCustomFieldsById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Contact
                .Include(x => x.CustomFields)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id, token);

            return _mapper.Map<Domain.Entities.Contact>(data);
        }

        /// <summary>
        /// update contact
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(Domain.Entities.Contact entity, CancellationToken token)
        {
            var data = await _dataContext.Contact
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Contact id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task UpdateContactsToGuests(Guid campaignId, CancellationToken token)
        {
            var contactsQuery = ContactsToGuestsQuery.Replace("{0}", campaignId.ToString());
            var contactListsQuery = ContactListsQuery.Replace("{0}", campaignId.ToString());

            await _queryExecutor.ExecuteSqlRawAsync(contactsQuery, token);
            await _queryExecutor.ExecuteSqlRawAsync(contactListsQuery, token);
        }

        public async Task<IEnumerable<string>> GetExistingEmails(IEnumerable<string> emails, CancellationToken token)
        {
            var results = await _dataContext.Contact
                .Where(x => emails.Contains(x.Email))
                .Select(x => x.Email)
                .ToListAsync(token);

            return results;
        }
        public async Task<IEnumerable<string>> GetExistingPhoneNumbers(IEnumerable<string> phoneNumbers, CancellationToken token)
        {
            var results = await _dataContext.Contact
                .Where(x => phoneNumbers.Contains(x.MobileNumber))
                .Select(x => x.MobileNumber)
                .ToListAsync(token);

            return results;
        }
    }
}
