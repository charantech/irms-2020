﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class ServiceCatalogRepository : IServiceRepository<ServiceCatalog, Guid>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        public ServiceCatalogRepository(IrmsTenantDataContext context, ICurrentUser user, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _user = user;
        }
        /// <summary>
        /// this method can create new <see cref="EntityClasses.ServiceCatalog"/> in DB
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(ServiceCatalog entity, CancellationToken token)
        {
            var sc = _mapper.Map<EntityClasses.ServiceCatalog>(entity);
            if(sc == null && entity != null)
            {
                throw new AutoMapperMappingException();
            }

            if(sc == null)
            {
                throw new IncorrectRequestException("Not enough data for creating service");
            }
            //sc.CreatedById = _user.Id;
            sc.CreatedOn = DateTime.UtcNow;

            await _context.ServiceCatalog.AddAsync(sc);
            await _context.SaveChangesAsync(token);

            return sc.Id;
        }
        /// <summary>
        /// method can create new <see cref="EntityClasses.ServiceCatalogFeature"/> in DB for <see cref="EntityClasses.ServiceCatalog"/>
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> CreateServiceFeature(ServiceCatalog entity, CancellationToken token)
        {
            var scEntity = await _context.ServiceCatalog.FirstOrDefaultAsync(p => p.Id == entity.Id);
            if(scEntity == null)
            {
                throw new IncorrectRequestException($"ServiceCatalog not found by Id: {entity.Id}");
            }
            var scFeatures = _mapper.Map<List<EntityClasses.ServiceCatalogFeature>>(entity.ServiceCatalogFeatures);
            scEntity.ServiceCatalogFeature.Union(scFeatures);

            await _context.SaveChangesAsync();
            return scFeatures[0].Id;
        }
        /// <summary>
        /// this method can mark <see cref="EntityClasses.ServiceCatalog"/> and all his <see cref="EntityClasses.ServiceCatalogFeature"/> as inactive in DB 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Delete(Guid id, CancellationToken token)
        {
            var scEntity = await _context.ServiceCatalog.Include(s => s.ServiceCatalogFeature).FirstOrDefaultAsync(s => s.Id == id);
            if (scEntity == null)
            {
                throw new IncorrectRequestException($"ServiceCatalog not found by Id: {id}");
            }
            scEntity.IsActive = false;
            foreach (var item in scEntity.ServiceCatalogFeature)
            {
                item.IsActive = false;
            }
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// get <see cref="ServiceCatalog"/> by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ServiceCatalog> GetById(Guid id, CancellationToken token)
        {
            var scEntity = await _context.ServiceCatalog
                .FirstAsync(s => s.Id == id);
            if (scEntity == null)
            {
                throw new IncorrectRequestException($"ServiceCatalog not found by Id: {id}");
            }
            var sc = _mapper.Map<ServiceCatalog>(scEntity);
            if (sc == null && scEntity != null)
            {
                throw new AutoMapperMappingException();
            }
            return sc;
        }

        public Task Update(ServiceCatalog entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }
    }
}
