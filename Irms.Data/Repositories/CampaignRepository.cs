﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Campaign = Irms.Domain.Entities.Campaign;
using Irms.Application.Abstract.Repositories;
using Irms.Data.EntityClasses;
using Irms.Application.Abstract;
using Irms.Application;
using Irms.Application.Campaigns.Commands;
using System.Linq;
using Irms.Application.Campaigns.ReadModels;
using Z.EntityFramework.Plus;
using Irms.Domain.Entities;

namespace Irms.Data.Repositories
{
    public class CampaignRepository : ICampaignRepository<Campaign, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public CampaignRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        /// <summary>
        /// get tenant only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Campaign> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.EventCampaign.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Campaign Id is incorrect");
            }
            var result = _mapper.Map<EventCampaign, Campaign>(data);
            return result;
        }

        public async Task<bool[]> GetExistingMedias(Guid id, CancellationToken token)
        {
            var result = new bool[3];

            var invitations = await _dataContext.CampaignInvitation
                .Include(x => x.CampaignSmsTemplate)
                .Include(x => x.CampaignEmailTemplate)
                .Include(x => x.CampaignWhatsappTemplate)
                .Where(x => x.EventCampaignId == id)
                .ToListAsync(token);

            if (invitations.SelectMany(x => x.CampaignEmailTemplate).Count() > 0)
                result[0] = true;

            if (invitations.SelectMany(x => x.CampaignSmsTemplate).Count() > 0)
                result[1] = true;

            if (invitations.SelectMany(x => x.CampaignWhatsappTemplate).Count() > 0)
                result[2] = true;
            return result;
        }

        /// <summary>
        /// create event
        /// </summary>
        /// <param name="entity">event domain entity</param>
        /// <param name="token"> cancellation token</param>
        /// <returns></returns>
        public async Task<Guid> Create(Campaign entity, CancellationToken token)
        {
            var data = _mapper.Map<Campaign, EventCampaign>(entity);
            data.CampaignType = (int)entity.CampaignType;
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;

            await _dataContext.EventCampaign.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        /// <summary>
        /// update campaign basic info
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(Campaign entity, CancellationToken token)
        {
            var data = await _dataContext.EventCampaign.FirstOrDefaultAsync(x => x.Id == entity.Id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Campaign Id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.CampaignStatus = (byte)(int)entity.CampaignStatus;
            data.ModifiedById = _user.Id;

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task Delete(Guid id, CancellationToken token)
        {
            var data = await _dataContext.EventCampaign.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Campaign Id is incorrect");
            }

            data.Deleted = true;
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;

            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// update preferred media
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdatePreferredMedia(UpdatePreferredMediaCmd request, CancellationToken token)
        {
            var dbList = await _dataContext.CampaignPrefferedMedia
                .Where(x => x.EventCampaignId == request.CampaignId)
                .ToListAsync(token);

            var dbListIds = dbList.Select(x => x.Id).ToList();

            var (toAdd, toDel, toUpd) = dbListIds.Compare(request.GuestPreferredMediaList.Select(x => x.Id), (x, y) => x == y);

            var toUpdGuids = toUpd.Select(x => x.o);

            toAdd.ToList()
                .ForEach(x =>
                {
                    var item = request.GuestPreferredMediaList.FirstOrDefault(y => y.Id == x);
                    var d = _mapper.Map<CampaignPrefferedMedia>(item);

                    d.Id = Guid.NewGuid();
                    d.EventCampaignId = request.CampaignId;
                    d.TenantId = _tenant.Id;
                    d.CreatedById = _user.Id;
                    d.CreatedOn = DateTime.UtcNow;

                    _dataContext.CampaignPrefferedMedia.Add(d);
                });

            dbList.Where(x => toDel.Contains(x.Id))
                .ToList()
                .ForEach(x =>
                {
                    _dataContext.CampaignPrefferedMedia.Remove(x);
                });

            dbList.Where(x => toUpdGuids.Contains(x.Id))
                .ToList()
                .ForEach(x =>
                {
                    var item = request.GuestPreferredMediaList.FirstOrDefault(y => y.Id == x.Id);
                    _mapper.Map(item, x);
                    x.ModifiedById = _user.Id;
                    x.ModifiedOn = DateTime.UtcNow;
                });

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task<(string name, string email)> GetTenantAdmin(CancellationToken token)
        {
            var data = await _dataContext.UserInfo.FirstOrDefaultAsync(x =>
            x.RoleId == (int)RoleType.TenantAdmin
            && x.TenantId == _tenant.Id,
            token);

            if (data == null)
            {
                throw new IncorrectRequestException("Tenant admin not found.");
            }

            return (data.FullName, data.Email);
        }

        public async Task<bool> CheckBOTCodeUniqueness(string code, CancellationToken token)
        {
            var data = await _dataContext.EventCampaign.FirstOrDefaultAsync(x => x.AcceptCode == code
            || x.RejectCode == code, token);
            if (data == null)
            {
                return true;
            }

            return false;
        }
    }
}
