﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class ContactListRepository : IContactListRepository<ContactList, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;

        public ContactListRepository(IrmsDataContext dataContext, IMapper mapper, ICurrentUser user)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
        }

        /// <summary>
        /// create contact list
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(ContactList entity, CancellationToken token)
        {
            var data = _mapper.Map<ContactList, EntityClasses.ContactList>(entity);
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;

            // save to db
            await _dataContext.ContactList.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        public async Task<bool> IsNameAlreadyExists(string name, CancellationToken token)
        {
            var result = await _dataContext.ContactList.AnyAsync(x => x.Name == name);
            return result;
        }

        /// <summary>
        /// delete contact list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Delete(Guid id, CancellationToken token)
        {
            var contactList = await _dataContext.ContactList.FirstOrDefaultAsync(x => x.Id == id);

            var contactListToContacts = await _dataContext.ContactListToContacts
                .Where(x => x.ContactListId == id)
                .ToListAsync();

            _dataContext.ContactList.Remove(contactList);
            _dataContext.ContactListToContacts.RemoveRange(contactListToContacts);
            await _dataContext.SaveChangesAsync();
        }

        /// <summary>
        /// get contact list by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ContactList> GetById(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.ContactList
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            var result = _mapper.Map<ContactList>(entity);

            return result;
        }


        /// <summary>
        /// is contact lsit assigned to a campaign?
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> IsContactListAssignToACampaign(Guid id, CancellationToken token)
        {
            var result = await _dataContext.EventCampaign.FirstOrDefaultAsync(x => x.GuestListId == id, token);
            return result != null;
        }

        /// <summary>
        /// Checks if contact list is empty
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> IsContactListEmpty(Guid id, CancellationToken token)
        {
            var isNotEmpty = await _dataContext.ContactListToContacts
                .AnyAsync(x => x.ContactListId == id, token);

            return !isNotEmpty;
        }

        /// <summary>
        /// update contact list
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(ContactList entity, CancellationToken token)
        {
            var updated = _mapper.Map<EntityClasses.ContactList>(entity);
            updated.ModifiedById = _user.Id;
            updated.ModifiedOn = DateTime.UtcNow;

            _dataContext.ContactList.Update(updated);
            await _dataContext.SaveChangesAsync(token);
        }

        //public async Task<IEnumerable<ContactListImportantField>> FetchImportantFields(Guid id, CancellationToken token)
        //{
        //    var data = await _dataContext.ContactListImportantField
        //        .Where(x => x.ContactListId == id)
        //        .ToListAsync(token);

        //    return _mapper.Map<IEnumerable<ContactListImportantField>>(data);
        //}

        //public async Task<ContactListRFI> GetContactListRfiForm(Guid id, CancellationToken token)
        //{
        //    var data = await _dataContext.ContactListRFI
        //        .Where(x => x.RfiFormId == id)
        //        .ToListAsync(token);

        //    return _mapper.Map<ContactListRFI>(data);
        //}

        public async Task<IEnumerable<ContactListImportantField>> GetContactListImportantFields(Guid listId, CancellationToken token)
        {
            var data = await _dataContext.ContactListImportantField
                .AsNoTracking()
                .Where(x => x.ContactListId == listId)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<ContactListImportantField>>(data);
        }

        public async Task AddImportantFields(IEnumerable<ContactListImportantField> fields, CancellationToken token)
        {
            var data = _mapper.Map<IEnumerable<EntityClasses.ContactListImportantField>>(fields);
            _dataContext.AddRange(data);
            await _dataContext.SaveChangesAsync(token);
        }
    }
}
