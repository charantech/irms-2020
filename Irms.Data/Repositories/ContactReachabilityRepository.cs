﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ContactReachability = Irms.Domain.Entities.ContactReachability;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract;
using Irms.Application;
using Irms.Domain;

namespace Irms.Data.Repositories
{
    public class ContactReachabilityRepository : IContactReachabilityRepository<ContactReachability, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public ContactReachabilityRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        public Task<Guid> Create(ContactReachability entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }
        public Task<ContactReachability> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }
        public async Task<ContactReachability> GetByGuestId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.ContactReachability.FirstOrDefaultAsync(x => x.ContactId == id, token);
            if (data == null)
            {
                return new ContactReachability();
            }
            var result = _mapper.Map<EntityClasses.ContactReachability, ContactReachability>(data);
            return result;
        }

        /// <summary>
        /// update reachability
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(ContactReachability entity, CancellationToken token)
        {
            var data = await _dataContext.ContactReachability.FirstOrDefaultAsync(x => x.ContactId == entity.ContactId, token);
            if (data == null)
            {
                //create
                var cr = _mapper.Map<ContactReachability, EntityClasses.ContactReachability>(entity);
                cr.Id = Guid.NewGuid();
                cr.TenantId = _tenant.Id;
                cr.CreatedById = _user.Id;
                cr.CreatedOn = DateTime.UtcNow;

                await _dataContext.ContactReachability.AddAsync(cr, token);
            }
            else
            {
                //update
                _mapper.Map(entity, data);
                data.ModifiedOn = DateTime.UtcNow;
                data.ModifiedById = _user.Id;
            }

            await _dataContext.SaveChangesAsync(token);
        }
    }
}
