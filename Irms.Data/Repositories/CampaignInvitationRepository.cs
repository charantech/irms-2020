﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CampaignInvitation = Irms.Domain.Entities.CampaignInvitation;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract;
using Irms.Application;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.Application.Campaigns.Commands;
using System.Linq;
using System.Collections.Generic;
using AutoMapper.QueryableExtensions;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.Campaigns.ReadModels;

namespace Irms.Data.Repositories
{
    public class CampaignInvitationRepository : ICampaignInvitationRepository<CampaignInvitation, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public CampaignInvitationRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        /// <summary>
        /// get invitations by campaign id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IEnumerable<(CampaignInvitation Invitation, bool Sms, bool Email, bool Whatsapp)>> GetByCampaignId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitation
                .AsNoTracking()
                .Include(x => x.CampaignSmsTemplate)
                .Include(x => x.CampaignEmailTemplate)
                .Include(x => x.CampaignWhatsappTemplate)
                .Where(x => x.EventCampaignId == id)
                .ToListAsync(token);

            var res = data.Select(x =>
            {
                var mapped = _mapper.Map<CampaignInvitation>(x);
                mapped.IntervalType = (IntervalType?)x.IntervalType;
                return (mapped, x.CampaignSmsTemplate.Count() > 0, x.CampaignEmailTemplate.Count() > 0, x.CampaignWhatsappTemplate.Count() > 0);
            });
            return res;
        }

        /// <summary>
        /// get invitation by id...
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignInvitation> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitation.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id, token);
            var result = _mapper.Map<EntityClasses.CampaignInvitation, CampaignInvitation>(data);
            if (result == null)
                return result;

            result.IntervalType = (IntervalType?)data.IntervalType;
            return result;
        }

        /// <summary>
        /// create invitation...
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(CampaignInvitation entity, CancellationToken token)
        {
            var data = _mapper.Map<CampaignInvitation, EntityClasses.CampaignInvitation>(entity);
            data.IntervalType = (int?)entity.IntervalType;
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;
            await _dataContext.CampaignInvitation.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
            return entity.Id;
        }

        /// <summary>
        /// updates an entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(CampaignInvitation entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitation
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Campaign invitation id is incorrect");
            }

            _mapper.Map(entity, data);
            data.IntervalType = (int?)entity.IntervalType;
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// deletes an invitation
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Delete(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.CampaignInvitation
                .FirstOrDefaultAsync(x => x.Id == id);

            _dataContext.CampaignInvitation.Remove(entity);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// deletes an email tempalte
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeleteEmailTemplate(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.CampaignEmailTemplate
                .FirstOrDefaultAsync(x => x.Id == id);

            _dataContext.CampaignEmailTemplate.Remove(entity);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// deletes an sms tempalte
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeleteSmsTemplate(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.CampaignSmstemplate
                .FirstOrDefaultAsync(x => x.Id == id);

            _dataContext.CampaignSmstemplate.Remove(entity);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task DeleteWhatsappTemplate(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.Id == id);

            _dataContext.CampaignWhatsappTemplate.Remove(entity);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// del rfi form
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeleteRfiForm(Guid id, CancellationToken token)
        {
            var form = await _dataContext.RfiForm
                .FirstOrDefaultAsync(x => x.Id == id);

            var questions = await _dataContext.RfiFormQuestion
                .Where(x => x.RfiFormId == form.Id)
                .ToListAsync(token);

            _dataContext.RfiFormQuestion.RemoveRange(questions);
            _dataContext.RfiForm.Remove(form);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// updates range of entities
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdateRange(IEnumerable<CampaignInvitation> entities, CancellationToken token)
        {
            //var ids = 
            var ids = entities.Select(x => x.Id);
            var data = _dataContext.CampaignInvitation
                .Where(x => ids.Contains(x.Id))
                .ToList();

            data.ForEach(item =>
            {
                var updated = entities.FirstOrDefault(x => x.Id == item.Id);

                _mapper.Map(updated, item);
                item.ModifiedOn = DateTime.UtcNow;
                item.ModifiedById = _user.Id;
                item.Active = true;
            });

            //_dataContext.UpdateRange(data);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// get email template by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignEmailTemplate> GetCampaignEmailTemplateById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignEmailTemplate.FirstOrDefaultAsync(x => x.Id == id, token);
            var result = _mapper.Map<EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>(data);
            return result;
        }

        /// <summary>
        /// get sms template by invitation id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignEmailTemplate> GetCampaignEmailTemplateByInvitationId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignEmailTemplate.FirstOrDefaultAsync(x => x.CampaignInvitationId == id, token);
            var result = _mapper.Map<EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>(data);
            return result;
        }

        /// <summary>
        /// create email template
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> CreateCampaignEmailTemplate(CampaignEmailTemplate entity, CancellationToken token)
        {
            var data = _mapper.Map<CampaignEmailTemplate, EntityClasses.CampaignEmailTemplate>(entity);
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;
            await _dataContext.CampaignEmailTemplate.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
            return entity.Id;
        }

        public async Task<Guid> CreateCampaignWhatsappTemplate(CampaignWhatsappTemplate entity, CancellationToken token)
        {
            var data = _mapper.Map<CampaignWhatsappTemplate, EntityClasses.CampaignWhatsappTemplate>(entity);
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;
            await _dataContext.CampaignWhatsappTemplate.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
            return entity.Id;
        }

        public async Task<Guid> UpdateCampaignWhatsappTemplate(CampaignWhatsappTemplate entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == entity.CampaignInvitationId);

            if (data == null)
            {
                throw new IncorrectRequestException("Whatsapp template id is incorrect");
            }

            data.Body = entity.Body;
            data.SenderName = entity.SenderName;
            data.TemplateId = entity.TemplateId;

            //_mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        public async Task<Guid> CopyCampaignWhatsappTemplate(CampaignWhatsappTemplate entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == entity.CampaignInvitationId);

            if (data == null)
            {
                throw new IncorrectRequestException("Whatsapp template id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        public async Task<Guid> UpdateCampaignWhatsappBOTTemplate(CampaignWhatsappTemplate entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == entity.CampaignInvitationId);

            if (data == null)
            {
                throw new IncorrectRequestException("Whatsapp template id is incorrect");
            }

            data.AcceptHtml = entity.AcceptHtml;
            data.RejectHtml = entity.RejectHtml;
            data.TemplateId = entity.TemplateId;
            data.SenderName = entity.SenderName;
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        public async Task CreateCampaignWhatsappResponseForm(CampaignWhatsappTemplate entity, CancellationToken token)
        {
            var data = _mapper.Map<EntityClasses.CampaignWhatsappTemplate>(entity);
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;
            _dataContext.CampaignWhatsappTemplate.Add(data);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task UpdateCampaignWhatsappResponseForm(CampaignWhatsappTemplate entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == entity.CampaignInvitationId);

            if (data == null)
            {
                throw new IncorrectRequestException("Email template id is incorrect");
            }

            data.Rsvphtml = entity.Rsvphtml;
            data.WelcomeHtml = entity.WelcomeHtml;
            data.AcceptHtml = entity.AcceptHtml;
            data.RejectHtml = entity.RejectHtml;
            data.ThemeJson = entity.ThemeJson;
            data.BackgroundImagePath = entity.BackgroundImagePath;
            data.ProceedButtonText = entity.ProceedButtonText;
            data.AcceptButtonText = entity.AcceptButtonText;
            data.RejectButtonText = entity.RejectButtonText;

            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// update email template
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdateCampaignEmailTemplate(CampaignEmailTemplate entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignEmailTemplate
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Email template id is incorrect");
            }

            data.BackgroundImagePath = entity.BackgroundImagePath;
            data.Body = entity.Body;
            data.PlainBody = entity.PlainBody;
            data.SenderEmail = entity.SenderEmail;
            data.Preheader = entity.Preheader;
            data.Subject = entity.Subject;

            //_mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// get sms template by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignSmsTemplate> GetCampaignSmsTemplateById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignSmstemplate.FirstOrDefaultAsync(x => x.Id == id, token);
            var result = _mapper.Map<EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>(data);
            return result;
        }

        /// <summary>
        /// get sms template by invitation id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignSmsTemplate> GetCampaignSmsTemplateByInvitationId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignSmstemplate.FirstOrDefaultAsync(x => x.CampaignInvitationId == id, token);
            var result = _mapper.Map<EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>(data);
            return result;
        }

        /// <summary>
        /// create sms template
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> CreateCampaignSmsTemplate(CampaignSmsTemplate entity, CancellationToken token)
        {
            var data = _mapper.Map<CampaignSmsTemplate, EntityClasses.CampaignSmstemplate>(entity);
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;
            await _dataContext.CampaignSmstemplate.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
            return entity.Id;
        }

        /// <summary>
        /// update sms template
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdateCampaignSmsTemplate(CampaignSmsTemplate entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignSmstemplate.FirstOrDefaultAsync(x => x.Id == entity.Id, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Sms template id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// update campaign email response form
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdateCampaignEmailResponseForm(UpdateCampaignEmailResponseFormCmd entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignEmailTemplate
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Email template id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// load invitation group of items
        /// </summary>
        /// <param name="eventCampaignId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CampaignInvitation>> LoadInvitationGroup(Guid eventCampaignId, InvitationType type, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitation
                .Where(x => x.EventCampaignId == eventCampaignId && x.InvitationTypeId == (int)type)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<CampaignInvitation>>(data);
        }

        public async Task<IEnumerable<CampaignInvitation>> LoadInvitationTemplateGroup(Guid eventCampaignId, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitation
                .Include(x => x.CampaignEmailTemplate)
                .Include(x => x.CampaignSmsTemplate)
                .Include(x => x.CampaignWhatsappTemplate)
                .Where(x => x.EventCampaignId == eventCampaignId)
                .ToListAsync(token);

            var model = data.Select(x =>
            {
                var inv = _mapper.Map<CampaignInvitation>(x);
                inv.IntervalType = (IntervalType?)x.IntervalType;
                inv.EmailTemplate = _mapper.Map<CampaignEmailTemplate>(x.CampaignEmailTemplate.FirstOrDefault());
                inv.SmsTemplate = _mapper.Map<CampaignSmsTemplate>(x.CampaignSmsTemplate.FirstOrDefault());
                inv.WhatsappTemplate = _mapper.Map<CampaignWhatsappTemplate>(x.CampaignWhatsappTemplate.FirstOrDefault());
                return inv;
            });

            return model;
        }

        public async Task<IEnumerable<CampaignInvitation>> LoadAcceptedOrRejectedInvitations(Guid invitationId, UserAnswer answer, CancellationToken token)
        {
            var invitation = await _dataContext.CampaignInvitation
                .Include(x => x.EventCampaign)
                .FirstOrDefaultAsync(x => x.Id == invitationId, token);

            var invitationTypeId = (int)answer + 1;

            var invitations = await _dataContext.CampaignInvitation
                .Where(x => x.EventCampaignId == invitation.EventCampaignId && x.InvitationTypeId == invitationTypeId)
                .ToListAsync(token);

            var mapped = _mapper.Map<IEnumerable<CampaignInvitation>>(invitations)
                .ToList();

            for (int i = 0; i < mapped.Count; ++i)
            {
                mapped[i].IntervalType = (IntervalType?)invitations[i].IntervalType;
            }

            return mapped;
        }

        public async Task<IEnumerable<WhatsappApprovedTemplate>> LoadWhatsappApprovedTemplates(InvitationType? invitationType, CancellationToken token)
        {
            List<EntityClasses.WhatsappApprovedTemplate> templates;
            if (!invitationType.HasValue)
            {
                templates = await _dataContext.WhatsappApprovedTemplate
                    .Where(x => x.TenantId == _tenant.Id)
                    .ToListAsync(token);
            }
            else
            {
                templates = await _dataContext.WhatsappApprovedTemplate
                    .Where(x => x.TenantId == _tenant.Id
                        && x.InvitationTypeId == (int)invitationType)
                    .ToListAsync(token);
            }

            return _mapper.Map<IEnumerable<WhatsappApprovedTemplate>>(templates);
        }

        public async Task<CampaignWhatsappTemplate> GetCampaignWhatsappTemplateById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignWhatsappTemplate.FirstOrDefaultAsync(x => x.Id == id, token);
            var result = _mapper.Map<EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>(data);
            return result;
        }

        public async Task<CampaignWhatsappTemplate> GetCampaignWhatsappTemplateByInvitationId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignWhatsappTemplate.FirstOrDefaultAsync(x => x.CampaignInvitationId == id, token);
            var result = _mapper.Map<EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>(data);
            return result;
        }

        public async Task<RfiForm> GetCampaignFormByInvitationId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.RfiForm.FirstOrDefaultAsync(x => x.CampaignInvitationId == id, token);
            var result = _mapper.Map<EntityClasses.RfiForm, RfiForm>(data);
            return result;
        }

        public async Task<ListAnalysisCampaignResponse> GetByGuestListId(Guid guestListId, Guid eventId, CancellationToken token)
        {
            var data = await _dataContext.EventCampaign
                .Include(x => x.CampaignInvitations)
                .FirstOrDefaultAsync(x => x.GuestListId == guestListId
                && x.EventId == eventId
                && x.CampaignType == (int)CampaignType.ListAnalysis, token);

            if (data == null)
            {
                return null;
            }

            var inv = data.CampaignInvitations.FirstOrDefault();

            var result = new ListAnalysisCampaignResponse
            {
                CampaignId = data.Id,
                CampaignInvitationId = inv.Id,
            };

            var form = await _dataContext.RfiForm
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == inv.Id, token);
            if (form != null)
            {
                result.RfiFormId = form.Id;
            }

            return result;
        }

    }
}
