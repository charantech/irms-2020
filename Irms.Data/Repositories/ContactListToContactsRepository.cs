﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Data.EntityClasses;
using Irms.Infrastructure.Services.ExcelExport;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class ContactListToContactsRepository : IContactListToContactsRepository<Domain.Entities.ContactListToContact, Guid>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;

        public ContactListToContactsRepository(IrmsDataContext dataContext, IMapper mapper, ICurrentUser user, TenantBasicInfo tenant)
        {
            _tenant = tenant;
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
        }

        /// <summary>
        /// create relationship between contact and contact list
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(Domain.Entities.ContactListToContact entity, CancellationToken token)
        {
            var data = _mapper.Map<Domain.Entities.ContactListToContact, EntityClasses.ContactListToContacts>(entity);
            data.CreatedById = _user.Id;

            // save to db
            await _dataContext.ContactListToContacts.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        /// <summary>
        /// create relationship between contacts and contact list
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task CreateRange(List<Domain.Entities.ContactListToContact> entity, CancellationToken token)
        {
            var data = _mapper.Map<List<Domain.Entities.ContactListToContact>, List<EntityClasses.ContactListToContacts>>(entity);
            data.ForEach(x => x.CreatedById = _user.Id);

            // save to db
            await _dataContext.ContactListToContacts.AddRangeAsync(data, token);
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// Delete range by ids
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeleteRangeByIds(IEnumerable<Guid> ids, CancellationToken token)
        {
            var entities = await _dataContext.ContactListToContacts.Where(x => ids.Contains(x.Id))
                .ToListAsync();

             _dataContext.ContactListToContacts.RemoveRange(entities);
            await _dataContext.SaveChangesAsync(token);
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task<Domain.Entities.ContactListToContact> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task Update(Domain.Entities.ContactListToContact entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Delete contacts from contact list. Carefull: Removes only M2M binding, not contacts!
        /// </summary>
        /// <param name="contactIds"></param>
        /// <param name="contactListId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeleteRangeFromContactList(IEnumerable<Guid> contactIds, Guid contactListId, CancellationToken token)
        {
            contactIds = contactIds.Distinct();
            var contacts = await _dataContext.ContactListToContacts
                .Where(x => x.TenantId == _tenant.Id
                         && contactIds.Contains(x.ContactId)
                         && x.ContactListId == contactListId)
                .ToListAsync();

            _dataContext.ContactListToContacts.RemoveRange(contacts);
            await _dataContext.SaveChangesAsync();
        }
        
        /// <summary>
        /// Get all contacts for contact list by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Domain.Entities.ContactListToContact>> GetAllForContactList(Guid id, CancellationToken token)
        {
            var entities = await _dataContext
                .ContactListToContacts.Where(x => x.ContactListId == id)
                .ToListAsync();

            var result = _mapper.Map <IEnumerable<Domain.Entities.ContactListToContact>>(entities);
            return result;
        }
    }
}
