﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Event = Irms.Domain.Entities.Event;
using EventLocation = Irms.Domain.Entities.EventLocation;
using Irms.Application.Abstract.Repositories;
using System.Linq;
using Irms.Data.EntityClasses;
using Irms.Application.Abstract;
using Irms.Domain.Entities;
using Z.EntityFramework.Plus;
using Irms.Application;

namespace Irms.Data.Repositories
{
    public class EventRepository : IEventRepository<Event, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public EventRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        /// <summary>
        /// get tenant only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Event> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Event.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Event Id is incorrect");
            }
            var result = _mapper.Map<EntityClasses.Event, Event>(data);
            return result;
        }

        public async Task<Event> GetEventWithLocation(Guid eventId, CancellationToken token)
        {
            var data = await _dataContext.Event
                .Include(x => x.EventLocation)
                .FirstOrDefaultAsync(x => x.Id == eventId, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Event Id is incorrect");
            }
            var result = _mapper.Map<EntityClasses.Event, Event>(data);
            return result;
        }

        /// <summary>
        /// create event
        /// </summary>
        /// <param name="entity">event domain entity</param>
        /// <param name="token"> cancellation token</param>
        /// <returns></returns>
        public async Task<Guid> Create(Event entity, CancellationToken token)
        {
            var data = _mapper.Map<Event, EntityClasses.Event>(entity);
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.TimeZoneName = "ksa";
            data.TimeZoneUtcOffset = "+03:00";
            data.IsActive = true;

            //add location info
            var location = _mapper.Map<EventLocation, EntityClasses.EventLocation>(entity.Location);
            location.Id = entity.Location.Id;
            location.TenantId = _tenant.Id;
            location.CreatedById = _user.Id;
            location.CreatedOn = DateTime.UtcNow;

            var country = await _dataContext.Country
                .FirstOrDefaultAsync(x => x.ShortIso == entity.Location.Country, token);

            location.CountryId = country.Id;
            data.EventLocation.Add(location);

            //add event features
            var features = entity.Features.Select(x => new EventFeature
            {
                Id = Guid.NewGuid(),
                EventId = entity.Id,
                ServiceCatalogFeatureId = x,
                TenantId = entity.TenantId
            }).ToList();
            data.EventFeature = features;


            // save to db
            await _dataContext.Event.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        /// <summary>
        /// update tenant basic info
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(Event entity, CancellationToken token)
        {
            var data = await _dataContext.Event.FirstOrDefaultAsync(x => x.Id == entity.Id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Event Id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;

            //update location
            var location = await _dataContext.EventLocation.FirstOrDefaultAsync(x => x.EventId == entity.Id, token);
            if (location == null)
            {
                throw new IncorrectRequestException("Event Id is incorrect");
            }

            var country = await _dataContext.Country
                .FirstOrDefaultAsync(x => x.ShortIso == entity.Location.Country, token);
            location.CountryId = country.Id;

            _mapper.Map(entity.Location, location);
            location.ModifiedOn = DateTime.UtcNow;
            location.ModifiedById = _user.Id;

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task Delete(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Event.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Event Id is incorrect");
            }

            data.IsDeleted = true;
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task UpdateFeatures(Event entity, CancellationToken token)
        {
            var (toAdd, toDel, _) = _dataContext.EventFeature.AsNoFilter().Where(x => x.EventId == entity.Id).Compare(entity.Features, (o, n) => o.ServiceCatalogFeatureId == n);

            //del
            _dataContext.EventFeature.RemoveRange(toDel);

            //add
            await _dataContext.EventFeature.AddRangeAsync(
                toAdd.Select(x => new EventFeature
                {
                    Id = Guid.NewGuid(),
                    EventId = entity.Id,
                    ServiceCatalogFeatureId = x,
                    TenantId = _tenant.Id
                }),
                token);

            await _dataContext.SaveChangesAsync(token);
        }
    }
}
