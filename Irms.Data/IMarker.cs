﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Irms.Tests.Unit")]
[assembly: InternalsVisibleTo("Irms.Tests.Integration")]
namespace Irms.Data
{
    public interface IMarker
    {
    }
}
