﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class CampaignEmailTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string Subject { get; set; }
        public string Preheader { get; set; }
        public string SenderEmail { get; set; }
        public string Body { get; set; }
        public string PlainBody { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string FormSettings { get; set; }
        public string BackgroundImagePath { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual CampaignInvitation CampaignInvitation { get; set; }
    }
}
