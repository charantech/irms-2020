﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class CustomField
    {
        public CustomField()
        {
            ContactCustomField = new HashSet<ContactCustomField>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string FieldName { get; set; }
        public int CustomFieldType { get; set; }
        public long? MinValue { get; set; }
        public long? MaxValue { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<ContactCustomField> ContactCustomField { get; set; }
    }
}
