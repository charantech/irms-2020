﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class RfiFormQuestion
    {
        public RfiFormQuestion()
        {
            RfiFormResponse = new HashSet<RfiFormResponse>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid RfiFormId { get; set; }
        public string Question { get; set; }
        public bool Mapped { get; set; }
        public string MappedField { get; set; }
        public int SortOrder { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual RfiForm RfiForm { get; set; }
        public virtual ICollection<RfiFormResponse> RfiFormResponse { get; set; }
    }
}
