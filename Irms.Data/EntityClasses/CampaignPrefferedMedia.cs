﻿using System;

namespace Irms.Data.EntityClasses
{
    public partial class CampaignPrefferedMedia
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventCampaignId { get; set; }
        public Guid ContactId { get; set; }
        public bool WhatsApp { get; set; }
        public bool Sms { get; set; }
        public bool Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual EventCampaign EventCampaign { get; set; }
    }
}
