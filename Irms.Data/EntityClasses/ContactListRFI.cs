﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class ContactListRFI
    {
        public ContactListRFI()
        {
            ContactListRfiAnswer = new HashSet<ContactListRFIAnswer>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid RfiFormId { get; set; }
        public Guid GuestListId { get; set; }
        public int Status { get; set; }
        public DateTime? SentDate { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        //public string EmailTemplate { get; set; }
        //public string SmsTemplate { get; set; }
        //public string WhatsappTemplate { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual RfiForm RfiForm { get; set; }
        public virtual ContactList GuestList { get; set; }
        public virtual ICollection<ContactListRFIAnswer> ContactListRfiAnswer { get; set; }
    }
}
