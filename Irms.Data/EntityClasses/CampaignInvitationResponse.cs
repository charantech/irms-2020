﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class CampaignInvitationResponse
    {
        public CampaignInvitationResponse()
        {
            CampaignInvitationResponseMediaTypes = new HashSet<CampaignInvitationResponseMediaType>();
        }
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public Guid ContactId { get; set; }
        public int? ResponseMediaType { get; set; }
        public int? Answer { get; set; }
        public DateTime? ResponseDate { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual CampaignInvitation CampaignInvitation { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ICollection<CampaignInvitationResponseMediaType> CampaignInvitationResponseMediaTypes { get; set; }
    }
}
