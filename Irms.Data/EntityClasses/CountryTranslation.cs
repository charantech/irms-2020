﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class CountryTranslation
    {
        public Guid Id { get; set; }
        public Guid CountryId { get; set; }
        public Guid LanguageId { get; set; }
        public string Translation { get; set; }

        public virtual Country Country { get; set; }
    }
}
