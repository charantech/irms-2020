﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public class Contact
    {
        public Contact()
        {
            ContactListToContacts = new HashSet<ContactListToContacts>();
            CustomFields = new HashSet<ContactCustomField>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Title { get; set; }

        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public int? GenderId { get; set; }

        public string Email { get; set; }
        public string AlternativeEmail { get; set; }

        public string MobileNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }

        public string WorkNumber { get; set; }
        public Guid? NationalityId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public Guid? IssuingCountryId { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }


        public bool Active { get; set; }
        public bool Deleted { get; set; }

        public bool? IsGuest { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<ContactListToContacts> ContactListToContacts { get; set; }
        public virtual ICollection<ContactCustomField> CustomFields { get; set; }
    }
}