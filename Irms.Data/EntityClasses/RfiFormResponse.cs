﻿using System;

namespace Irms.Data.EntityClasses
{
    public partial class RfiFormResponse
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid RfiFormId { get; set; }
        public Guid RfiFormQuestionId { get; set; }
        public Guid ContactId { get; set; }
        public string Answer { get; set; }
        public DateTime ResponseDate { get; set; }

        public virtual RfiForm RfiForm { get; set; }
        public virtual RfiFormQuestion RfiFormQuestion { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
