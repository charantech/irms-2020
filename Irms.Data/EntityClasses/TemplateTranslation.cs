﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class TemplateTranslation
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid TemplateId { get; set; }
        public Guid LanguageId { get; set; }
        public string SmsText { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }

        public virtual Language Language { get; set; }
        public virtual Template Te { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
