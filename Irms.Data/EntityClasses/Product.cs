﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class Product
    {
        public Product()
        {
            ProductServiceCatalogFeature = new HashSet<ProductServiceCatalogFeature>();
            TenantSubscription = new HashSet<TenantSubscription>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? FinalPrice { get; set; }
        public byte CurrencyId { get; set; }
        public byte LicensePeriodId { get; set; }
        public int LicensePeriodDays { get; set; }
        public bool IsShowOnWeb { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Currency Currency { get; set; }
        public virtual LicensePeriod LicensePeriod { get; set; }
        public virtual ICollection<ProductServiceCatalogFeature> ProductServiceCatalogFeature { get; set; }
        public virtual ICollection<TenantSubscription> TenantSubscription { get; set; }
    }
}
