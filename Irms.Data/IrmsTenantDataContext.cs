﻿using Irms.Data.EntityClasses;
using Irms.Application;
using System;
using System.Linq;
using System.Security;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Irms.Data
{
    public partial class IrmsTenantDataContext : IrmsDataContext
    {
        public IrmsTenantDataContext(DbContextOptions<IrmsDataContext> options, TenantBasicInfo tenant)
            : base(options)
        {
            if (tenant == null)
            {
                throw new SecurityException("Tenant doesn't exist");
            }

            AddFilter(tenant.Id);
        }

        private void AddFilter(Guid tenantId)
        {
            this.Filter<UserInfo>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<EventLog>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Language>(x => x.Where(q => q.TenantId == tenantId));
        }
    }
}
