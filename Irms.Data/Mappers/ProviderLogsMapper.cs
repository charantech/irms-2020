﻿using Irms.Application.Events.Commands;
using Irms.Domain.Entities;

namespace Irms.Data.Mappers
{
    public class ProviderLogsMapper : AutoMapper.Profile
    {
        public ProviderLogsMapper()
        {
            CreateMap<ProviderLogs, EntityClasses.ProviderLogs>()
                .ForMember(x => x.ProviderType, x => x.MapFrom(m => (int)m.ProviderType));

            CreateMap<EntityClasses.ProviderLogs, ProviderLogs>()
                .ForMember(x => x.ProviderType, x => x.MapFrom(m => (ProviderType)m.ProviderType));

            CreateMap<ProviderLogsCmd, ProviderLogs>();
        }
    }
}
