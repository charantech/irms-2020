﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Mappers
{
    public class LicenseMapper : AutoMapper.Profile
    {
        public LicenseMapper()
        {
            CreateMap<Irms.Domain.Entities.LicensePeriod, Irms.Data.EntityClasses.LicensePeriod>()
                .ForMember(x => x.Id, x => x.MapFrom(m => m.Id))
                .ForMember(x => x.Title, x => x.MapFrom(m => m.Title))
                .ForMember(x => x.Days, x => x.MapFrom(m => m.Days));
        }
    }
}
