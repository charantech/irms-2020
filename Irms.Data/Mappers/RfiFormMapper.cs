﻿using Irms.Application.Campaigns.Commands;
using Irms.Application.RfiForms.Commands;

namespace Irms.Data.Mappers
{
    public class RfiFormMapper : AutoMapper.Profile
    {
        public RfiFormMapper()
        {
            CreateMap<Domain.Entities.RfiForm, EntityClasses.RfiForm>()
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<EntityClasses.RfiForm, Domain.Entities.RfiForm>();

            //create
            CreateMap<UpsertRfiFormCmd, Domain.Entities.RfiForm>();
        }
    }
}
