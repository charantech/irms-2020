﻿using Irms.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Mappers
{
    public class LanguageMapper : AutoMapper.Profile
    {
        public LanguageMapper()
        {
            CreateMap<Domain.Entities.Language, Language>();
        }
    }
}
