﻿using Irms.Application.DataModule.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Mappers
{
    public class DataModuleMapper : AutoMapper.Profile
    {
        public DataModuleMapper()
        {
            CreateMap<UpdatePersonalInfoCmd, Domain.Entities.Contact>()
                .ForMember(x => x.GenderId, y => y.MapFrom(z => (int?)z.Gender));

            CreateMap<UpdateGlobalContactPersonalInfoCmd, Domain.Entities.Contact>()
                .ForMember(x => x.GenderId, y => y.MapFrom(z => (int?)z.Gender));
        }
    }
}
