﻿using Irms.Application.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    public class TransactionManager : ITransactionManager
    {
        private readonly IrmsDataContext _context;

        public TransactionManager(IrmsDataContext context)
        {
            _context = context;
        }

        public async Task<ITransaction> BeginTransactionAsync(
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var transaction = await _context.Database.BeginTransactionAsync(cancellationToken);
            return new BasicTransaction(transaction);
        }

        public async Task<ITransaction> BeginTransactionAsync(
            IsolationLevel isolationLevel,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var transaction = await _context.Database.BeginTransactionAsync(isolationLevel, cancellationToken);
            return new BasicTransaction(transaction);
        }
    }
}
