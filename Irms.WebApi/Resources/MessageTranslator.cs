﻿using Irms.Application.Abstract.Services;
using System.Globalization;
using System.Resources;

namespace Irms.WebApi.Resources
{
    public class MessageTranslator : IMessageTranslator
    {
        private readonly ResourceManager _manager;

        public MessageTranslator()
        {
            _manager = new ResourceManager(GetType().Namespace + ".Messages", GetType().Assembly);
        }

        public string Translate(string message, CultureInfo culture)
        {
            try
            {
                var translation = _manager.GetString(message, culture);

                return string.IsNullOrEmpty(translation) ? message : translation;
            }
            catch (MissingManifestResourceException)
            {
                return message;
            }
            catch (MissingSatelliteAssemblyException)
            {
                return message;
            }
        }
    }
}
