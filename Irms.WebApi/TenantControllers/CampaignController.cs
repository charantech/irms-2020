﻿using Irms.Application.CampaignInvitations.ReadModels;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.ReadModels;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Data.Read.Event.Queries;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class CampaignController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CampaignController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("list")]
        public async Task<IPageResult<CampaignListItem>> GetCampaignList([FromBody]GetCampaignList query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return result;
        }

        /// <summary>
        /// get campaign
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<CampaignInfo> GetCampaignInfo(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInfo(id), token);
            return result;
        }

        /// <summary>
        /// get campaign
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{listId}/check-reachability")]
        public async Task<(bool reachabilitySubscribed, ReachabilityStats stats)> CheckReachability(Guid listId, CancellationToken token)
        {
            var result = await _mediator.Send(new CheckReachability(listId), token);
            return result;
        }

        /// <summary>
        /// get contacts reachability
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("contacts-reachability")]
        public async Task<(bool reachabilitySubscribed, ReachabilityStats stats, IPageResult<GuestReachabilityListItem> page)> GetContactsReachabilityList([FromBody]GetGuestsReachabilityList query)
        {
            (bool reachabilitySubscribed, ReachabilityStats stats) = await _mediator.Send(new CheckReachability(query.GuestListId), CancellationToken.None);
            //query.IgnoreReachability = !reachabilitySubscribed;
            var result = await _mediator.Send(query, CancellationToken.None);
            return (reachabilitySubscribed, stats, result);
        }

        /// <summary>
        /// fetching contacts without check for reachability
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        //[HttpPost("contacts-without-reachability")]
        //public async Task<IPageResult<GuestReachabilityListItem>> GetContactsWithoutReachabilityList([FromBody] GetGuestsReachabilityList query, CancellationToken token)
        //{
        //    query.IgnoreReachability = false;

        //    var result = await _mediator.Send(query, token);
        //    return result;
        //}

        /// <summary>
        /// get campaign
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/{listId}/campaign-preffered-media")]
        public async Task<PrefferedMediaStats> GetPrefferedMediaStats(Guid campaignId, Guid listId, CancellationToken token)
        {
            var result = await _mediator.Send(new GetPrefferedMediaStats(campaignId, listId), token);
            return result;
        }



        /// <summary>
        /// create campaign
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> Create([FromBody]CreateCampaignCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// create campaign for list analysis
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("list-analysis")]
        public async Task<ListAnalysisCampaignResponse> CreateListAnalysisCampaign([FromBody]CreateListAnalysisCampaignCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// this method will call update campaign handler
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Unit> Update([FromBody]UpdateCampaignCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPut("prefferedMedia")]
        public async Task<Unit> UpdatePrefferedMedia([FromBody]UpdatePreferredMediaCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// delete campaign by id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<Unit> Delete([FromBody]DeleteCampaignCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPost("test-campaign")]
        public async Task<TestCampaignSendInfoModel> TestCampaign([FromBody]TestCampaignCmd cmd)
        {
            var result = await _mediator.Send(cmd, CancellationToken.None);
            return result;
        }

        [HttpGet("go-live/{campaignId}")]
        public async Task<CampaignGoLiveResponse> GoLive(Guid campaignId)
        {
            var result = await _mediator.Send(new CampaignGoLiveCmd(campaignId), CancellationToken.None);
            return result;
        }

        [HttpGet("list-analysis-go-live/{campaignId}")]
        public async Task<CampaignGoLiveResponse> ListAnalysisGoLive(Guid campaignId)
        {
            var result = await _mediator.Send(new ListAnalysisCampaignGoLiveCmd(campaignId), CancellationToken.None);
            return result;
        }

        [HttpPost("campaign-name")]
        public async Task<string> GetCampaignName([FromBody] GetCampaignNameQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }
    }
}
