﻿using Irms.Application.RfiForms.Commands;
using Irms.Data.Read.RfiForm.Queries;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class RfiFormController : ControllerBase
    {
        private readonly IMediator _mediator;
        public RfiFormController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{id}")]
        public async Task<Data.Read.RfiForm.ReadModels.RfiForm> GetRfiForm(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetRfiFormQry(id), token);
            return result;
        }

        /// <summary>
        /// create/update rfi form
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost] 
        public async Task<Guid> UpsertRfiForm([FromForm]UpsertRfiFormCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
    }
}