﻿using System;
using System.Threading;
using System.Threading.Tasks;
using IdentityServer4.Events;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Irms.Data.IdentityClasses;
using Irms.Data;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.UsersInfo.Queries;
using static IdentityModel.OidcConstants;
using System.Text.RegularExpressions;

namespace Irms.WebApi.Auth
{
    public class MultiTenantPasswordValidator : IResourceOwnerPasswordValidator
    {
        //private readonly string _adminPortal = "admin";
        //private readonly string _tenantPortal = "tenant";

        private readonly IEventService _events;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<MultiTenantPasswordValidator> _logger;
        private readonly IMediator _mediator;
        private readonly IOTPCodeNotifier _notifier;

        public MultiTenantPasswordValidator(
            IEventService events,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            ILogger<MultiTenantPasswordValidator> logger,
            IMediator mediator,
            IOTPCodeNotifier notifier
            )
        {
            _events = events;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _mediator = mediator;
            _notifier = notifier;
        }


        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context) => await ValidateAsyncInternal(context);

        /// <summary>
        /// validate user
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private async Task<Unit> ValidateAsyncInternal(ResourceOwnerPasswordValidationContext context)
        {
            var u = context.UserName;

            var user = await _userManager.FindByNameAsync(u);
            //var referer = context.Request.Raw["portal"];
            //if (referer == _adminPortal)
            //{
            string otpCode = context.Request.Raw["code"];
            string token = context.Request.Raw["token"];

            if (string.IsNullOrEmpty(otpCode) && string.IsNullOrEmpty(token))
            {
                await AuthenticateAdmin(context, user, u);
            }
            else
            {
                await VerifyOtp(context, user, token, otpCode);
            }
            //}
            return Unit.Value;
        }

        /// <summary>
        /// private method for admin authentication
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user"></param>
        /// <param name="u"></param>
        /// <returns></returns>
        private async Task<Unit> AuthenticateAdmin(ResourceOwnerPasswordValidationContext context, ApplicationUser user, string u)
        {
            if (user == null)
            {
                return await FailWithMessage(context, "invalid credentials", "Authentication failed for username: {username}, reason: invalid credentials", u);
            }

            var admin = await _mediator.Send(new GetUserInfoIdByAspUser(user.Id));
            if (admin == null)
            {
                return await FailWithMessage(context, "invalid credentials", "Authentication failed for username: {username}, reason: invalid credentials", u);
            }

            if (!user.IsEnabled)
            {
                return await FailWithMessage(context, "is disabled", "Authentication failed for username: {username}, reason: user is disabled", u);
            }

            var result = await _signInManager.PasswordSignInAsync(user.UserName, context.Password, true, true);
            if (result.IsLockedOut)
            {
                return await FailWithMessage(context, "locked out", "Authentication failed for username: {username}, reason: locked out", u);
            }

            if (result.IsNotAllowed)
            {
                return await FailWithMessage(context, "not allowed", "Authentication failed for username: {username}, reason: not allowed", u);
            }

            if (result.RequiresTwoFactor)
            {
                var code = await _userManager.GenerateTwoFactorTokenAsync(user, "Phone");
                Console.WriteLine($"otp code is {code}");

                //TODO Uncomment when SMS has been resolved:
                bool isSent = await _notifier.NotifyBySms(user.PhoneNumber, code, CancellationToken.None);
                if (!isSent)
                {
                    return await FailWithMessage(context, "message sending failed", "Failed to send OTP. username: {username},", u);
                }

                var defaultToken = await _userManager.GenerateTwoFactorTokenAsync(user, "Default");
                _logger.LogInformation("otp code sent for username: {username}", u);
                string message = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    phone = Regex.Replace(user.PhoneNumber, @"\d(?!\d{0,2}$)", "*").Replace('+', '*'),
                    token = defaultToken
                });

                context.Result = new GrantValidationResult(TokenRequestErrors.UnauthorizedClient, message);
                await _events.RaiseAsync(new UserLoginFailureEvent(user.UserName, "Requires2FA", false));

                return Unit.Value;
            }
            else if (result.Succeeded)
            {
                //login without two factor authentication
                var sub = await _userManager.GetUserIdAsync(user);
                _logger.LogInformation("Credentials validated for username: {username}", u);
                await _events.RaiseAsync(new UserLoginSuccessEvent(u, sub, u, false));
                context.Result = new GrantValidationResult(sub, AuthenticationMethods.Password);
                return Unit.Value;
            }

            return await FailWithMessage(context, "invalid credentials", "Authentication failed for username: {username}, reason: invalid credentials", u);
        }

        /// <summary>
        /// private method to verify otp for admin
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user"></param>
        /// <param name="token"></param>
        /// <param name="otpCode"></param>
        /// <returns></returns>
        private async Task<Unit> VerifyOtp(ResourceOwnerPasswordValidationContext context, ApplicationUser user, string token, string otpCode)
        {
            //verify token
            var tokenValidated = await _userManager.VerifyTwoFactorTokenAsync(user, "Default", token);
            if (!tokenValidated)
            {
                return await FailWithMessage(context, "invalid token with otp code", "Invalid otp token. username: {username},", user.UserName);
            }

            //verify code
            var codeValidated = await _userManager.VerifyTwoFactorTokenAsync(user, "Phone", otpCode);
            if (!codeValidated)
            {
                return await FailWithMessage(context, "invalid  otp code", "Invalid otp code. username: {username},", user.UserName);
            }

            var sub = await _userManager.GetUserIdAsync(user);
            _logger.LogInformation("Credentials validated for username: {username}", user.UserName);
            await _events.RaiseAsync(new UserLoginSuccessEvent(user.UserName, sub, user.UserName, false));
            context.Result = new GrantValidationResult(sub, AuthenticationMethods.Password);
            return Unit.Value;
        }

        /// <summary>
        /// write fail response... private method
        /// </summary>
        /// <param name="context"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        private async Task<Unit> FailWithMessage(ResourceOwnerPasswordValidationContext context, string title, string description, string username)
        {
            _logger.LogInformation(description, username);
            context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, title);
            await _events.RaiseAsync(new UserLoginFailureEvent(username, title, false));

            return Unit.Value;
        }
    }














    //public class MultiTenantPasswordValidator : IResourceOwnerPasswordValidator
    //{
    //    private readonly string _adminPortal = "admin";
    //    private readonly string _publicPortal = "public";

    //    private readonly IEventService _events;
    //    private readonly SignInManager<ApplicationUser> _signInManager;
    //    private readonly UserManager<ApplicationUser> _userManager;
    //    private readonly ILogger<MultiTenantPasswordValidator> _logger;
    //    private readonly IMediator _mediator;
    //    private readonly TenantBasicInfo _tenant;
    //    //TODO Uncomment when SMS has been resolved:
    //    //private readonly IOTPCodeNotifier _notifier;

    //    public MultiTenantPasswordValidator(
    //        IEventService events,
    //        SignInManager<ApplicationUser> signInManager,
    //        UserManager<ApplicationUser> userManager,
    //        ILogger<MultiTenantPasswordValidator> logger,
    //        IMediator mediator,
    //        TenantBasicInfo tenant//,
    //                              //TODO Uncomment when SMS has been resolved:
    //                              //IOTPCodeNotifier notifier
    //        )
    //    {
    //        _events = events;
    //        _signInManager = signInManager;
    //        _userManager = userManager;
    //        _logger = logger;
    //        _mediator = mediator;
    //        _tenant = tenant;
    //        //_notifier = notifier;
    //    }


    //    public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context) => await ValidateAsyncInternal(context);

    //    private async Task<Unit> ValidateAsyncInternal(ResourceOwnerPasswordValidationContext context)
    //    {
    //        var u = context.UserName;

    //        var user = await _userManager.FindByNameAsync(u);

    //        if (await GetUserIsUnavaliable(user, context))
    //        {
    //            return await FailWithMessage(context, "invalid username", "No user found matching username: {username}", u);
    //        }

    //        if (!await CanSignIntoTenant(user))
    //        {
    //            return await FailWithMessage(context, "incorrect tenant", "Authentication failed for username: {username}, reason: tenant/dept mismatch", u);
    //        }

    //        if (!user.IsEnabled)
    //        {
    //            //added condition to fix issue Mvms-1163, user: SAif ur Rehman, Dated # 27-May-219
    //            var referer = context.Request.Raw["portal"];

    //            if (referer == _adminPortal)
    //            {
    //                //throw new NotImplementedException();
    //                var isRegistrationFinished = await _mediator.Send(new GetGuestIsRegistrationFinished(user.Id));
    //                if (isRegistrationFinished)
    //                {
    //                    return await FailWithMessage(context, "is disabled", "Authentication failed for username: {username}, reason: user is disabled", u);
    //                }

    //                return await FailWithMessage(context, "not approved", "Authentication failed for username: {username}, reason: user is not a Guest", u);

    //            }
    //        }

    //        var result = await _signInManager.CheckPasswordSignInAsync(user, context.Password, true);
    //        if (result.Succeeded)
    //        {
    //            //Task https://takamul.atlassian.net/browse/MVMS-1194, by Saif ur Rehman. Dated: June 16, 2019.

    //            var referer = context.Request.Raw["portal"];
    //            if (referer == _adminPortal)
    //            {
    //                var twoFactorEnabled = await _userManager.SetTwoFactorEnabledAsync(user, true);
    //                if (twoFactorEnabled.Succeeded)
    //                {
    //                    var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);

    //                    //TODO Uncomment when SMS has been resolved:
    //                    bool isSent = true;//await _notifier.NotifyBySms(user.PhoneNumber, code, CancellationToken.None);
    //                    if (!isSent)
    //                    {
    //                        return await FailWithMessage(context, "message sending failed", "Failed to send OTP. username: {username},", u);
    //                    }
    //                }
    //            }

    //            var sub = await _userManager.GetUserIdAsync(user);
    //            _logger.LogInformation("Credentials validated for username: {username}", u);
    //            await _events.RaiseAsync(new UserLoginSuccessEvent(u, sub, u, false));
    //            context.Result = new GrantValidationResult(sub, "pwd");
    //            return Unit.Value;
    //        }
    //        else
    //        {

    //        }

    //        if (result.IsLockedOut)
    //        {
    //            return await FailWithMessage(context, "locked out", "Authentication failed for username: {username}, reason: locked out", u);
    //        }

    //        if (result.IsNotAllowed)
    //        {
    //            return await FailWithMessage(context, "not allowed", "Authentication failed for username: {username}, reason: not allowed", u);
    //        }

    //        return await FailWithMessage(context, "invalid credentials", "Authentication failed for username: {username}, reason: invalid credentials", u);
    //    }


    //    private async Task<bool> GetUserIsUnavaliable(ApplicationUser user, ResourceOwnerPasswordValidationContext context)
    //    {
    //        if (user == null)
    //        {
    //            return true;
    //        }

    //        var referer = context.Request.Raw["portal"];
    //        var isUnavailable = false;

    //        if (referer == _adminPortal)
    //        {
    //            var admin = await _mediator.Send(new GetUserUserInfoId(user.Id));
    //            if (admin == null)
    //            {
    //                isUnavailable = true;
    //            }
    //        }
    //        else if (referer == _publicPortal)
    //        {
    //            isUnavailable = !(await IsGuest(user.Id));
    //        }

    //        return isUnavailable;
    //    }

    //    private async Task<Unit> FailWithMessage(ResourceOwnerPasswordValidationContext context, string title, string description, string username)
    //    {
    //        _logger.LogInformation(description, username);
    //        context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, title);
    //        await _events.RaiseAsync(new UserLoginFailureEvent(username, title, false));

    //        return Unit.Value;
    //    }

    //    private async Task<bool> CanSignIntoTenant(ApplicationUser user)
    //    {
    //        //super admin can sign into any tenant
    //        if (user.IsSuperAdmin)
    //        {
    //            return true;
    //        }

    //        //Guests and admins can *NOT* sign into other tenants
    //        if (user.TenantId != _tenant.Id)
    //        {
    //            return false;
    //        }

    //        //Guests can sign into any department within a tenant
    //        if (await IsGuest(user.Id))
    //        {
    //            return true;
    //        }

    //        //admins can sign into their department only
    //        return user.DepartmentId == _tenant.DepartmentId;
    //    }

    //    private bool? _isGuest { get; set; }

    //    private async Task<bool> IsGuest(Guid userId)
    //    {
    //        if (!_isGuest.HasValue)
    //        {
    //            var Guest = await _mediator.Send(new GetAspNetUserGuestId(userId));
    //            _isGuest = Guest != null;
    //        }

    //        return _isGuest.Value;
    //    }
    //}
}
