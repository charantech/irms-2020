﻿using System.Linq;
using System.Threading.Tasks;
using Irms.Application.Abstract;
using Irms.Application.Templates.Commands;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using Irms.WebApi.LocalizationTransform.Template.Models;

namespace Irms.WebApi.LocalizationTransform.Template
{
    public class TemplateTranslator : ITemplateTranslator
    {
        private readonly ILanguageIds _languageIds;

        public TemplateTranslator(ILanguageIds languageIds)
        {
            _languageIds = languageIds;
        }

        public async Task<TemplateDetailsRead> TranslateReadModel(TemplateDetails src)
        {
            var enId = await _languageIds.En();
            var arId = await _languageIds.Ar();

            var en = src.Translations.FirstOrDefault(x => x.LanguageId == enId);
            var ar = src.Translations.FirstOrDefault(x => x.LanguageId == arId);

            return new TemplateDetailsRead
            {
                Id = src.Id,
                Name = src.Name,
                Type = src.Type,
                EmailCompatible = src.EmailCompatible,
                SmsCompatible = src.SmsCompatible,
                ModifiedOn = src.ModifiedOn,
                ModifiedBy = src.ModifiedBy,

                ArabicEnabled = ar != null,

                EnglishEmailSubject = en?.EmailSubject ?? src.EmailSubject,
                EnglishEmailBody = en?.EmailBody ?? src.EmailBody,
                EnglishSmsText = en?.SmsText ?? src.SmsText,

                ArabicEmailSubject = ar?.EmailSubject,
                ArabicEmailBody = ar?.EmailBody,
                ArabicSmsText = ar?.SmsText
            };
        }

        public async Task<CreateTemplateCmd> TranslateCreateModel(TemplateDetailsCreate src)
        {
            var enId = await _languageIds.En();
            var arId = await _languageIds.Ar();

            var tr = new TemplateTranslation(
                enId,
                src.EnglishEmailSubject,
                src.EnglishEmailBody,
                src.EnglishSmsText)
                .Enumerate();

            if (src.ArabicEnabled)
            {
                var ar = new TemplateTranslation(
                    arId,
                    src.ArabicEmailSubject,
                    src.ArabicEmailBody,
                    src.ArabicSmsText)
                    .Enumerate();

                tr = tr.Concat(ar);
            }

            return new CreateTemplateCmd(
                src.Name,
                src.Type,
                src.EmailCompatible,
                src.SmsCompatible,
                src.EnglishEmailSubject,
                src.EnglishEmailBody,
                src.EnglishSmsText,
                tr);
        }

        public async Task<UpdateTemplateCmd> TranslateUpdateModel(TemplateDetailsUpdate src)
        {
            var enId = await _languageIds.En();
            var arId = await _languageIds.Ar();

            var tr = new TemplateTranslation(
                    enId,
                    src.EnglishEmailSubject,
                    src.EnglishEmailBody,
                    src.EnglishSmsText)
                .Enumerate();

            if (src.ArabicEnabled)
            {
                var ar = new TemplateTranslation(
                        arId,
                        src.ArabicEmailSubject,
                        src.ArabicEmailBody,
                        src.ArabicSmsText)
                    .Enumerate();

                tr = tr.Concat(ar);
            }

            return new UpdateTemplateCmd(
                src.Id,
                src.Name,
                src.EmailCompatible,
                src.SmsCompatible,
                src.EnglishEmailSubject,
                src.EnglishEmailBody,
                src.EnglishSmsText,
                tr);
        }
    }
}
