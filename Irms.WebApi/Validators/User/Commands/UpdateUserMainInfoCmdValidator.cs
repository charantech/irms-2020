﻿using FluentValidation;
using Irms.Application.UsersInfo.Commands;

namespace Irms.WebApi.Validators.User.Commands
{
    public class UpdateUserMainInfoCmdValidator : AbstractValidator<UpdateUserMainInfoCmd>
    {
        public UpdateUserMainInfoCmdValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.LastName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Gender)
                .IsInEnum();

            RuleFor(x => x.BirthDate)
                .NotEmpty();

            RuleFor(x => x.PhoneNo)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Email)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.PassportNo)
                .MaximumLength(50);

            RuleFor(x => x.NationalId)
                .MaximumLength(50);

            RuleFor(x => x.SearchText)
                .MaximumLength(1000);
        }
    }
}
