﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.Campaign.Commands
{
    public class UpdatePreferredMediaCmdValidator : AbstractValidator<UpdatePreferredMediaCmd>
    {
        public UpdatePreferredMediaCmdValidator()
        {
            RuleForEach(x => x.GuestPreferredMediaList)
                .NotNull();
        }
    }

    public class GuestPreferredMediaCmdValidator : AbstractValidator<GuestPreferredMedia>
    {
        public GuestPreferredMediaCmdValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty();
        }
    }
}
