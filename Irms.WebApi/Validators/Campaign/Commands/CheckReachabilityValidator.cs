﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.Campaign.Commands
{
    public class CheckReachabilityValidator : AbstractValidator<CheckReachability>
    {
        public CheckReachabilityValidator()
        {
            RuleFor(e => e.GuestListId)
                .NotEmpty();
        }
    }
}
