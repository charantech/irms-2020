﻿using FluentValidation;
using Irms.Application.Contact.Commands;

namespace Irms.WebApi.Validators.Contact.Commands
{
    public class CreateContactCmdValidator : AbstractValidator<CreateContactCmd>
    {
        public CreateContactCmdValidator()
        {
            RuleFor(x => x.FullName)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(e => e.MobileNumber)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(e => e.Email)
                .NotEmpty()
                .MaximumLength(100);
        }
    }
}
