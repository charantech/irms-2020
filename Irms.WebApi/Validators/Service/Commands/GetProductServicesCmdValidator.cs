﻿using FluentValidation;
using Irms.Data.Read.Service.Queries;

namespace Irms.WebApi.Validators.Service.Commands
{
    public class GetProductServicesCmdValidator : AbstractValidator<GetProductServicesCmd>
    {
        public GetProductServicesCmdValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
