﻿using FluentValidation;
using Irms.Data.Read.Product.Queries;

namespace Irms.WebApi.Validators.Product.Queries
{
    public class ProductListQueryValidator : AbstractValidator<GetProductListQuery>
    {
        public ProductListQueryValidator()
        {
            RuleFor(x => x.PageNo)
                .GreaterThan(0);

            RuleFor(x => x.PageSize)
                .InclusiveBetween(1, 100);

            RuleFor(x => x.TimeFilter)
                .IsInEnum();
        }
    }
}
