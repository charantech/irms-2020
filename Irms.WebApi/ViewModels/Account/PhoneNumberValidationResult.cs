﻿namespace Irms.WebApi.ViewModels.Account
{
    public class PhoneNumberValidationResult
    {
        public PhoneNumberValidationResult(bool isValid, string formattedPhoneNo)
        {
            IsValid = isValid;
            FormattedPhoneNo = formattedPhoneNo;
        }

        public bool IsValid { get; }
        public string FormattedPhoneNo { get; }
    }
}
