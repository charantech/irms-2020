﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Irms.WebApi.Helpers
{
    public class LanguageDetector : ILanguageDetector
    {
        private const string HeaderKey = "X-Language";

        public string DetectRequestLanguage(HttpContext ctx)
        {
            return ctx?.Request?.Headers?[HeaderKey].FirstOrDefault();
        }
    }
}
