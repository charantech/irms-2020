﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Domain.Entities;
using MediatR;

namespace Irms.WebApi.Helpers
{
    public class LanguageCache : ILanguageCache
    {
        private readonly IMediator _mediator;
        private IEnumerable<Language> _cache;

        public LanguageCache(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IEnumerable<Language>> GetSystemLanguages(CancellationToken cancellationToken = default)
        {
            if (_cache == null)
            {
                var ls = await _mediator.Send(new GetAllLanguages(), cancellationToken);
                _cache = ls.Select(x => new Language(x.Id, x.Culture, x.Name));
            }

            return _cache;
        }
    }
}
