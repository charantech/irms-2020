﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Irms.Application.Abstract;
using Irms.Domain;
using Microsoft.AspNetCore.Http;

namespace Irms.WebApi.Helpers
{
    public class CurrentLanguage : ICurrentLanguage
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly ILanguageDetector _detector;
        private readonly ILanguageCache _cache;

        public CurrentLanguage(IHttpContextAccessor accessor, ILanguageCache cache, ILanguageDetector detector)
        {
            _accessor = accessor;
            _cache = cache;
            _detector = detector;
        }

        public CultureInfo GetRequestCulture()
        {
            var cultStr = _detector.DetectRequestLanguage(_accessor.HttpContext);
            if (string.IsNullOrEmpty(cultStr))
            {
                return default;
            }

            return new CultureInfo(cultStr);
        }

        public async Task<Guid?> GetRequestLanguageId()
        {
            var cultStr = _detector.DetectRequestLanguage(_accessor.HttpContext);
            if (string.IsNullOrEmpty(cultStr))
            {
                return default;
            }

            var languages = await _cache.GetSystemLanguages();
            return languages.FirstOrDefault(x => x.Culture.EqualsIgnoreCase(cultStr))?.Id;
        }
    }
}
