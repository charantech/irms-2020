﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Irms.WebApi.Helpers
{
    public class LanguageDetectorWithRemap : ILanguageDetector
    {
        private const string DefaultEn = "en-US";
        private const string DefaultAr = "ar-SA";

        private readonly ILanguageDetector _inner;
        private readonly Regex _en = new Regex("en-[A-Z]{2}", RegexOptions.Compiled);
        private readonly Regex _ar = new Regex("ar-[A-Z]{2}", RegexOptions.Compiled);

        public LanguageDetectorWithRemap(ILanguageDetector inner)
        {
            _inner = inner;
        }

        public string DetectRequestLanguage(HttpContext ctx)
        {
            var cultureString = _inner.DetectRequestLanguage(ctx);
            if (string.IsNullOrEmpty(cultureString))
            {
                return cultureString;
            }

            //remap language ar-XX => ar-SA, en-XX => en-US
            return _ar.IsMatch(cultureString) ? DefaultAr : DefaultEn;
        }
    }
}
