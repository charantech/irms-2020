﻿using Irms.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;

namespace Irms.WebApi.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params RoleType[] roles)
        {
            var rolesStr = roles
                .Select(r => Enum.GetName(r.GetType(), r))
                .Union(new[] { RoleType.SuperAdmin.ToString() });

            Roles = string.Join(",", rolesStr);
        }
    }
}
