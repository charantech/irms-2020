﻿using System.Collections.Generic;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Irms.WebApi.Helpers
{
    public class SwaggerLanguageHeaderProvider : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<OpenApiParameter>();
            }
            
            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "X-Language",
                In = ParameterLocation.Header,
                //Type = "string",
                Required = true,
                //Enum = new List<object> { "en-US", "ar-SA" },
                //Default = "en-US"
                Schema = new OpenApiSchema 
                { 
                    Type = "string", 
                    Enum = new List<IOpenApiAny> { new OpenApiString("en-US"), new OpenApiString("ar-SA") }, 
                    Default = new OpenApiString("en-US")
                }
            });
        }

    }
}
