﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Tenants.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Tenant.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Irms.WebApi.AdminControllers
{
    [AuthorizeRoles(RoleType.SuperAdmin)]
    [Route("api/[controller]")]
    public class TenantController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TenantController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// get tenant list by filters, pagination
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public async Task<IPageResult<TenantListItem>> GetTenantList([FromBody]TenantListQuery query, CancellationToken token)
        {
            var tenants = await _mediator.Send(query, token);
            return tenants;
        }

        /// <summary>
        /// get tenant basic info
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<TenantDetails> Get(Guid id, CancellationToken token)
        {
            var tenant = await _mediator.Send(new GetTenantDetails(id), token);
            return tenant;
        }

        /// <summary>
        /// get tenant contact info
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/contactInfo")]
        public async Task<TenantContactInfo> GetTenantContactInfo(Guid id, CancellationToken token)
        {
            var contactInfo = await _mediator.Send(new GetTenantContactInfo(id), token);
            return contactInfo;
        }

        /// <summary>
        /// get tenant configurations
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/config")]
        public async Task<TenantConfigInfo> GetTenantConfigInfo(Guid id, CancellationToken token)
        {
            var config = await _mediator.Send(new GetTenantConfigInfo(id), token);
            return config;
        }

        /// <summary>
        /// create customer/tenant
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> Create([FromForm]CreateTenantCmd cmd, CancellationToken token)
        {
            var id = await _mediator.Send(cmd, token);
            return id;
        }

        /// <summary>
        /// check if contact info email is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("checkEmailUniqueness")]
        public async Task<bool> CheckEmailUniqueness([FromBody]CheckContactInfoEmailUniqueness cmd, CancellationToken token)
        {
            var isUnique = await _mediator.Send(cmd, token);
            return isUnique;
        }

        /// <summary>
        /// update tenant basic info
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromForm]UpdateTenantCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// update tenant contact info
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("contactInfo")]
        public async Task UpdateContactInfo([FromBody]UpdateContactInfoCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }


        /// <summary>
        /// update tenant config
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("config")]
        public async Task UpdateConfig([FromBody]UpdateConfigCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// TODO: delete tenant by id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task Delete([FromBody]DeleteTenantCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// get client/tenant logo
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("logo")]
        public async Task<TenantLogo> GetTenantLogo(CancellationToken token)
        {
            return await _mediator.Send(new GetTenantLogo(), token);
        }

    }
}
