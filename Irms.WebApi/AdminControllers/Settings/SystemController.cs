﻿using System.Threading;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace Irms.WebApi.AdminControllers.Settings
{
    [Route("api/[controller]")]
    public class SystemController : Controller
    {
        private readonly IConfiguration _config;
        public SystemController(IConfiguration config)
        {
            _config = config;
        }
        [AllowAnonymous]
        [HttpGet("version")]
        public string GetVersion(CancellationToken token)
        {
            var ver = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return ver;
        }

        [AllowAnonymous]
        [HttpGet("url")]
        public string GetFroendUrl(CancellationToken token)
        {
            var url = _config["JwtAuthority:FrontendUrl"];
            return url;
        }
    }
}
