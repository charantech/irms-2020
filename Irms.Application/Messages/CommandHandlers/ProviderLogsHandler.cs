﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Events.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;

namespace Irms.Application.Events.CommandHandlers
{
    public class ProviderLogsHandler : IRequestHandler<ProviderLogsCmd, Unit>
    {
        private readonly IProviderLogsRepository<ProviderLogs, Guid> _repo;
        private readonly IMapper _mapper;

        public ProviderLogsHandler(
            IProviderLogsRepository<ProviderLogs, Guid> repo,
            IMapper mapper
            )
        {
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(ProviderLogsCmd message, CancellationToken token)
        {
            var log = _mapper.Map<ProviderLogs>(message);
            log.Create();

            await _repo.Create(log, token);

            return Unit.Value;
        }
    }
}
