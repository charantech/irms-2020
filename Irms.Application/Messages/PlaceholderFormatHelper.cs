﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Irms.Tests.Unit")]
namespace Irms.Application.Messages
{
    internal static class PlaceholderFormatHelper
    {
        public static string GetPlaceholder(string fieldName)
        {
            return "{{" + fieldName + "}}";
        }

        public static string BakeLink(string linkPath, string linkText)
        {
            return $"<a href=\"{linkPath}\">{linkText}</a>";
        }
    }
}
