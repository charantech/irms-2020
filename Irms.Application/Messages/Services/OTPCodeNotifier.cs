﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Templates.Queries;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Messages.Services
{
    /// <summary>
    /// Implementation of <see cref="IOTPCodeNotifier"/> for sending one time password 
    /// </summary>
    public class OTPCodeNotifier : IOTPCodeNotifier
    {
        private readonly IMediator _mediator;
        private readonly ISmsSender _smsSender;
        private readonly ICurrentLanguage _language;
        private readonly IPhoneNumberValidator _phoneValidator;

        public OTPCodeNotifier(IMediator mediator, ISmsSender smsSender, ICurrentLanguage language, IPhoneNumberValidator phoneValidator)
        {
            _mediator = mediator;
            _smsSender = smsSender;
            _language = language;
            _phoneValidator = phoneValidator;
        }

        public IEnumerable<string> Placeholders => new string[0];

        public async Task<bool> NotifyBySms(string phoneNo, string code, CancellationToken cancellationToken)
        {
            var template = await _mediator.Send(new GetActiveSmsTemplate(TemplateType.OTPCode), cancellationToken);
            if (template == null)
            {
                return false;
            }

            template.Translate(await _language.GetRequestLanguageId());
            var (isValid, phone) = _phoneValidator.Validate(phoneNo);
            if (!isValid)
            {
                throw new IncorrectRequestException("Phone number format is invalid");
            }

            var recipients = new SmsMessage.Recipient(phone).Enumerate();
            var saudiLocalTime = DateTime.UtcNow.AddHours(3); //3 hrs for saudi local time.

            string formattedTime = saudiLocalTime.ToString("dd/MM/yyyy HH:mm:ss");

            string message = string.Format(template.SmsText, code, formattedTime);
            var sms = new SmsMessage(message, recipients);
            await _smsSender.SendSms(sms, cancellationToken);
            return true;
        }
    }
}
