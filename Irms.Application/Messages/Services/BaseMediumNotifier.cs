﻿using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Messages.Services
{
    public class BaseMediumNotifier : IBaseMediumNotifier
    {
        private readonly IEmailBGSender _emailSender;
        private readonly ISmsBGSender _smsSender;

        public BaseMediumNotifier(
            IEmailBGSender emailSender,
            ISmsBGSender smsSender)
        {
            _emailSender = emailSender;
            _smsSender = smsSender;
        }

        public async Task<bool> NotifyByEmail(string subject, string body, string recipient, Guid tenantId, CancellationToken token)
        {
            var emailMsg = new EmailMessage(body, new EmailMessage.Recipient(recipient, subject).Enumerate());
            return await _emailSender.SendEmail(tenantId, emailMsg, token);
        }

        public async Task<bool> NotifyBySms(Guid contactId, string body, string recipientPhone, Guid tenantId, CancellationToken token)
        {
            var smsMsg = new SmsMessage(body, new SmsMessage.Recipient(contactId, recipientPhone).Enumerate());
            return await _smsSender.SendWebhookSms(tenantId, smsMsg, token);
        }
    }
}
