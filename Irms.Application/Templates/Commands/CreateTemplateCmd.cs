﻿using System;
using System.Collections.Generic;
using Irms.Domain.Entities.Templates;
using MediatR;

namespace Irms.Application.Templates.Commands
{
    /// <summary>
    /// Request object based on which an SMS or Email template will created
    /// </summary>
    public class CreateTemplateCmd : IRequest<Guid>
    {
        public CreateTemplateCmd(
            string name,
            TemplateType type,
            bool emailCompatible,
            bool smsCompatible,
            string emailSubject,
            string emailBody,
            string smsText,
            IEnumerable<TemplateTranslation> translations)
        {
            Name = name;
            Type = type;
            EmailCompatible = emailCompatible;
            SmsCompatible = smsCompatible;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            SmsText = smsText;
            Translations = translations;
        }

        public string Name { get; }
        public TemplateType Type { get; }

        public bool EmailCompatible { get; }
        public bool SmsCompatible { get; }

        public string EmailSubject { get; }
        public string EmailBody { get; }

        public string SmsText { get; }

        public IEnumerable<TemplateTranslation> Translations { get; }
    }
}
