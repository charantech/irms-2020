﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Templates.Commands;
using Irms.Domain.Entities.Templates;
using Irms.Application.Templates.Events;

namespace Irms.Application.Templates.CommandHandlers
{
    /// <summary>
    /// Handler for saveing new SMS or Email template in DB based on request <see cref="CreateTemplateCmd"/>
    /// </summary>
    public class CreateTemplateHandler : IRequestHandler<CreateTemplateCmd, Guid>
    {
        private readonly IRepository<Template, Guid> _repository;
        private readonly IMediator _mediator;

        public CreateTemplateHandler(IRepository<Template, Guid> repository, IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        public async Task<Guid> Handle(CreateTemplateCmd request, CancellationToken cancellationToken)
        {
            var template = new Template(request.Name,
                request.EmailCompatible,
                request.SmsCompatible,
                request.EmailBody,
                request.EmailSubject,
                request.SmsText,
                request.Type,
                request.Translations);

            var id = await _repository.Create(template, cancellationToken);
            await _mediator.Publish(new TemplateCreated(id), cancellationToken);
            return id;
        }
    }
}
