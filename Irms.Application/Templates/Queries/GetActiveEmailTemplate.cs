﻿using Irms.Domain.Entities.Templates;
using MediatR;

namespace Irms.Application.Templates.Queries
{
    /// <summary>
    /// Request object based on which handler return active Email template
    /// </summary>
    public class GetActiveEmailTemplate : IRequest<Template>
    {
        public GetActiveEmailTemplate(TemplateType templateType)
        {
            TemplateType = templateType;
        }

        public TemplateType TemplateType { get; }
    }
}
