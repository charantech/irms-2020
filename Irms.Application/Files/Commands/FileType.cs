﻿using System;

namespace Irms.Application.Files.Commands
{
    public enum FileType
    {
        Other = 0,
        Picture = 1,
        Document = 2,
        Pdf = 4
    }
}