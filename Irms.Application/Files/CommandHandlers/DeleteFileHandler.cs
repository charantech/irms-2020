﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Files.Commands;
using Irms.Application.Files.Events;
using Irms.Domain.Entities;
using Irms.Application.Abstract;

namespace Irms.Application.Files.CommandHandlers
{
    public class DeleteFileHandler : IRequestHandler<DeleteFileCmd, Unit>
    {
        private readonly IMediator _mediator;
        private readonly IFileUploadService _fileUpload;

        public DeleteFileHandler(IMediator mediator, IFileUploadService fileUpload)
        {
            _mediator = mediator;
            _fileUpload = fileUpload;
        }

        /// <summary>
        /// handler for delete file from azure
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteFileCmd message, CancellationToken cancellationToken)
        {
            await _fileUpload.DeleteFile(message.FileName);

            var e = new FileDeleted(message.FileName);
            await _mediator.Publish(e, cancellationToken);
            return Unit.Value;
        }
    }
}
