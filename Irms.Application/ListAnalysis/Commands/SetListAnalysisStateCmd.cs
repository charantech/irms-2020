﻿using Irms.Domain.Entities;
using MediatR;
using System;

namespace Irms.Application.ListAnalysis.Commands
{
    public class SetListAnalysisStateCmd : IRequest<Unit>
    {
        public Guid ListId { get; set; }
        public Guid EventId { get; set; }
        public ListAnalysisStep ListAnalysisStep { get; set; }
        public int? ListAnalysisPageNo { get; set; }
        public int? ListAnalysisPageSize { get; set; }
    }

}