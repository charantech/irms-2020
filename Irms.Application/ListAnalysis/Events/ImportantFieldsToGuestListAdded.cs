﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.ListAnalysis.Events
{
    public class ImportantFieldsToGuestListAdded : IEvent
    {
        public ImportantFieldsToGuestListAdded(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The important fields of list analysis has been saved for guest list {x(Id)} by {u}";
    }
}
