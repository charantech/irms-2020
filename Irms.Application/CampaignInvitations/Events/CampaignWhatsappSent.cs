﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Events
{
    public class CampaignWhatsappSent : IEvent
    {
        public CampaignWhatsappSent(IEnumerable<string> numbers)
        {
            Numbers = numbers;
        }

        public IEnumerable<string> Numbers { get; }
        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Whatsapp messages for numbers {string.Join(",", Numbers)} was sent by {u}";
    }
}
