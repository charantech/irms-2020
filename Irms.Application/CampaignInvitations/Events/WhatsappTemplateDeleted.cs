﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.CampaignInvitations.Events
{
    public class WhatsappTemplateDeleted : IEvent
    {
        public WhatsappTemplateDeleted(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Whatsapp template for Invitation with id {Id} was deleted by {u}";
    }
}
