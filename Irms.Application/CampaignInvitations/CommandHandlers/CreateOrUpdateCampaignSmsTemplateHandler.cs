﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Application.Files.Commands;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class CreateOrUpdateCampaignSmsTemplateHandler : IRequestHandler<CreateOrUpdateCampaignSmsTemplateCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public CreateOrUpdateCampaignSmsTemplateHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IConfiguration config
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _config = config;
        }

        /// <summary>
        /// creates or updates (if exists) campaign sms template
        /// </summary>
        /// <param name="message">new(updated) sms template</param>
        /// <param name="token">cancellation token</param>
        /// <returns>template id</returns>
        public async Task<Guid> Handle(CreateOrUpdateCampaignSmsTemplateCmd message, CancellationToken token)
        {
            var template = _mapper.Map<CampaignSmsTemplate>(message);

            var existingSmsTemplate = await _repo.GetCampaignSmsTemplateById(message.Id, token);
            if (existingSmsTemplate == null)
            {
                template.Create();

                template.RSVPHtml = await UploadBodyImages(message.RSVPHtml, token);
                template.WelcomeHtml = await UploadBodyImages(message.WelcomeHtml, token);
                template.AcceptHtml = await UploadBodyImages(message.AcceptHtml, token);
                template.RejectHtml = await UploadBodyImages(message.RejectHtml, token);
                template.BackgroundImagePath = await UploadNewImage(message.BackgroundImagePath, token);
                template.ThemeJson = DeleteImageIfExist(message.ThemeJson);
                await _repo.CreateCampaignSmsTemplate(template, token);
            }
            else
            {
                template.RSVPHtml = await UploadExistingBodyImages(message.RSVPHtml, template.RSVPHtml, token);
                template.WelcomeHtml = await UploadExistingBodyImages(message.WelcomeHtml, template.WelcomeHtml, token);
                template.AcceptHtml = await UploadExistingBodyImages(message.AcceptHtml, template.AcceptHtml, token);
                template.RejectHtml = await UploadExistingBodyImages(message.RejectHtml, template.RejectHtml, token);
                template.BackgroundImagePath = await UploadExistingImage(message.BackgroundImagePath, existingSmsTemplate.BackgroundImagePath, token);
                template.ThemeJson = DeleteImageIfExist(template.ThemeJson);

                await _repo.UpdateCampaignSmsTemplate(template, token);
            }

            await _mediator.Publish(new CampaignSmsTemplateCreatedOrUpdated(template.Id), token);
            return template.Id;
        }

        #region extract image from body and save to azure

        private string DeleteImageIfExist(string body)
        {
            var images = GetImagesInHTMLString(body, true);
            foreach (var img in images)
            {
                body = body.Replace(img, string.Empty);
            }

            return body;
        }

        private async Task<string> UploadBodyImages(string body, CancellationToken token)
        {
            var images = GetImagesInHTMLString(body);
            foreach (var img in images)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignSms)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                body = body.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }
            return body;
        }

        private async Task<string> UploadExistingBodyImages(string newBody, string existingBody, CancellationToken token)
        {
            var newImages = GetImagesInHTMLString(newBody);
            var existingImages = GetImagesInHTMLString(existingBody);
            var (toAdd, toDel, toUpd) = existingImages.Compare(newImages, ((n, o) => n == o));

            //add
            foreach (var img in toAdd)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignSms)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                newBody = newBody.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }

            //del
            foreach (var img in toDel)
            {
                string path = img.Replace($"{_config["AzureStorage:BaseUrl"]}", string.Empty);
                await _mediator.Send(new DeleteFileCmd(path), token);
            }

            //upd
            foreach (var img in toUpd)
            {
                string path = img.n;
                if (path.StartsWith("data:image"))
                {
                    path = $"{FilePath.GetFileDirectory(SystemModule.CampaignSms)}{Guid.NewGuid()}_image.png";
                    await _mediator.Send(new UploadFileCmd(path, img.n, FileType.Picture), token);
                    newBody = newBody.Replace(img.n, $"{_config["AzureStorage:BaseUrl"]}{path}");
                }
            }

            return newBody;
        }

        private List<string> GetImagesInHTMLString(string htmlString, bool isUri = false)
        {
            List<string> images = new List<string>();
            string pattern = isUri ? @"(data:image\/[^;]+;base64[^""]+)" : @" < img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Groups[1].Value);
            }

            return images;
        }

        #endregion



        /// <summary>
        /// Upload new image
        /// </summary>
        /// <param name="backgroundImagePath">image</param>
        /// <returns>image path</returns>
        private async Task<string> UploadNewImage(string backgroundImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(backgroundImagePath))
            {
                string res = CutExtension(backgroundImagePath);

                var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignSms)}{Guid.NewGuid()}_image.{res}";
                await _mediator.Send(new UploadFileCmd(path, backgroundImagePath, FileType.Picture), token);
                return path;
            }

            return string.Empty;
        }

        /// <summary>
        /// Override image
        /// </summary>
        /// <param name="backgroundImagePath">image</param>
        /// <returns>image path</returns>
        private async Task<string> UploadExistingImage(string newImagePath, string existingImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(newImagePath))
            {
                string res = CutExtension(newImagePath);
                if (res.IsNotNullOrEmpty())
                {
                    if (existingImagePath.IsNotNullOrEmpty())
                    {
                        await _mediator.Send(new DeleteFileCmd(existingImagePath), token);
                    }

                    var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignSms)}{Guid.NewGuid().ToString()}_image.{res}";
                    await _mediator.Send(new UploadFileCmd(path, newImagePath, FileType.Picture), token);

                    return path;
                }
                return "";
            }

            return string.Empty;
        }

        private string CutExtension(string input)
        {
            int length = input.IndexOf(";base64") - "data:image/".Length;
            if (length < 0)
            {
                return string.Empty;
            }
            string extension = input.Substring("data:image/".Length, length);
            return extension;
        }

        private string CutImageFromString(string input)
        {
            int index = input.IndexOf(";base64,") + ";base64,".Length;
            if (index < 0)
            {
                return string.Empty;
            }
            return input.Substring(index);
        }
    }
}
