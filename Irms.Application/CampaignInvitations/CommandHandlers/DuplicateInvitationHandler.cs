﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class DuplicateInvitationHandler : IRequestHandler<DuplicateInvitationCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IFileUploadService _fileUploadService;

        public DuplicateInvitationHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IFileUploadService fileUploadService
            )
        {
            _repo = repo;
            _mediator = mediator;
            _fileUploadService = fileUploadService;
        }

        /// <summary>
        /// duplicates an invitation
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(DuplicateInvitationCmd request, CancellationToken token)
        {
            var campaignInvitation = await _repo.GetById(request.InvitationId, token);

            var items = await _repo.LoadInvitationGroup(campaignInvitation.EventCampaignId, campaignInvitation.InvitationType, token);

            var newQueueId = items.Max(x => x.SortOrder) + 1;

            var newInvitation = new CampaignInvitation
            {
                StartDate = campaignInvitation.StartDate,
                Active = campaignInvitation.Active,
                EventCampaignId = campaignInvitation.EventCampaignId,
                Interval = campaignInvitation.Interval,
                IntervalType = campaignInvitation.IntervalType,
                InvitationType = campaignInvitation.InvitationType,
                Title = campaignInvitation.Title,
                SortOrder = newQueueId,
                Id = Guid.NewGuid()
            };

            await _repo.Create(newInvitation, token);
            await DuplicateSms(request, newInvitation, token);
            await DuplicateEmail(request, newInvitation, token);

            await _mediator.Publish(new InvitationDuplicated(newInvitation.Id), token);

            return request.InvitationId;
        }

        /// <summary>
        /// duplicates an email for invitation (if it exists)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="newInvitation"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task DuplicateEmail(DuplicateInvitationCmd request, CampaignInvitation newInvitation, CancellationToken token)
        {
            var emailTemplate = await _repo.GetCampaignEmailTemplateByInvitationId(request.InvitationId, token);

            if (emailTemplate == null)
                return;

            string backgroundImage = emailTemplate.BackgroundImagePath;

            if (!string.IsNullOrEmpty(backgroundImage))
            {
                var existingFile = await _fileUploadService.LoadFile(backgroundImage);
                backgroundImage = Guid.NewGuid().ToString();
                var stream = new MemoryStream(existingFile);
                await _fileUploadService.UploadFile(backgroundImage, stream);
            }

            var newEmailTemplate = new CampaignEmailTemplate
            {
                AcceptHtml = emailTemplate.AcceptHtml,
                BackgroundImagePath = backgroundImage,
                Body = emailTemplate.Body,
                CampaignInvitationId = newInvitation.Id,
                RejectHtml = emailTemplate.RejectHtml,
                ThemeJson = emailTemplate.ThemeJson,
                PlainBody = emailTemplate.PlainBody,
                Preheader = emailTemplate.Preheader,
                SenderEmail = emailTemplate.SenderEmail,
                Subject = emailTemplate.Subject,
                Id = Guid.NewGuid()
            };

            await _repo.CreateCampaignEmailTemplate(newEmailTemplate, token);
        }

        /// <summary>
        /// duplicates a sms for invitation (if it exists)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="newInvitation"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task DuplicateSms(DuplicateInvitationCmd request, CampaignInvitation newInvitation, CancellationToken token)
        {
            var smsTemplate = await _repo.GetCampaignSmsTemplateByInvitationId(request.InvitationId, token);

            if (smsTemplate == null)
                return;

            string backgroundImage = smsTemplate.BackgroundImagePath;

            if (!string.IsNullOrEmpty(backgroundImage))
            {
                var existingFile = await _fileUploadService.LoadFile(backgroundImage);
                backgroundImage = Guid.NewGuid().ToString();
                var stream = new MemoryStream(existingFile);
                await _fileUploadService.UploadFile(backgroundImage, stream);
            }

            var newSmsTemplate = new CampaignSmsTemplate
            {
                AcceptButtonText = smsTemplate.AcceptButtonText,
                AcceptHtml = smsTemplate.AcceptHtml,
                BackgroundImagePath = backgroundImage,
                Body = smsTemplate.Body,
                CampaignInvitationId = newInvitation.Id,
                ProceedButtonText = smsTemplate.ProceedButtonText,
                RejectButtonText = smsTemplate.RejectButtonText,
                RejectHtml = smsTemplate.RejectHtml,
                RSVPHtml = smsTemplate.RSVPHtml,
                SenderName = smsTemplate.SenderName,
                ThemeJson = smsTemplate.ThemeJson,
                WelcomeHtml = smsTemplate.WelcomeHtml,
                Id = Guid.NewGuid()
            };

            await _repo.CreateCampaignSmsTemplate(newSmsTemplate, token);
        }
    }
}
