﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class SendTestSmsHandler : IRequestHandler<SendTestSmsCmd, IEnumerable<string>>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly ISmsSender _smsSender;
        private readonly IPhoneNumberValidator _phoneValidator;
        public SendTestSmsHandler(ICampaignInvitationRepository<CampaignInvitation,
            Guid> repo,
            IMediator mediator,
            ISmsSender smsSender,
            IPhoneNumberValidator phoneValidator)
        {
            _repo = repo;
            _mediator = mediator;
            _smsSender = smsSender;
            _phoneValidator = phoneValidator;
        }

        /// <summary>
        /// handler that performs sms sending
        /// </summary>
        /// <param name="request">id and numbers</param>
        /// <param name="token">cancellation token</param>
        /// <returns>phone numbers</returns>
        public async Task<IEnumerable<string>> Handle(SendTestSmsCmd request, CancellationToken token)
        {
            var result = await _repo.GetCampaignSmsTemplateById(request.InvitationId, token);

            if (result == null)
            {
                throw new IncorrectRequestException("Template was not found");
            }

            string smsBody = result.Body.ReplaceDefaultValuePlaceholders();
            if (request.SmsList.Count() == 0)
            {
                return null;
            }

            foreach (var recipientPhone in request.SmsList)
            {
                var formattedPhone = _phoneValidator.Validate(recipientPhone, out var isValid);
                if (!isValid)
                {
                    throw new IncorrectRequestException("Phone number format is incorrect");
                }

                var recipient = new SmsMessage.Recipient(formattedPhone).Enumerate();
                var smsMessage = new SmsMessage(smsBody, recipient);
                await _smsSender.SendSms(smsMessage, token);
            }

            await _mediator.Publish(new CampaignSmsSent(request.SmsList), token);
            return request.SmsList;
        }
    }
}
