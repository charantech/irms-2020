﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class DeleteWhatsappTemplateHandler : IRequestHandler<DeleteWhatsappTemplateCmd, Unit>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;

        public DeleteWhatsappTemplateHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        /// <summary>
        /// delete whatsapp template
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteWhatsappTemplateCmd request, CancellationToken token)
        {
            var whatsapptemplate = await _repo.GetCampaignWhatsappTemplateByInvitationId(request.InvitationId, token);
            if (whatsapptemplate != null)
            {
                await _repo.DeleteWhatsappTemplate(whatsapptemplate.Id, token);
            }

            await _mediator.Publish(new WhatsappTemplateDeleted(request.InvitationId), token);

            return Unit.Value;
        }
    }
}
