﻿using Irms.Application.Campaigns.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Campaigns.Queries
{
    public class GetCampaignPlaceholderList : IRequest<IEnumerable<CampaignPlaceholderListItem>>
    {
        public GetCampaignPlaceholderList(
         int pageNo,
         int pageSize,
         string searchText,
         Guid invitationId,
         int templateTypeId,
         bool response)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
            InvitationId = invitationId;
            TemplateTypeId = templateTypeId;
            Response = response;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
        public Guid InvitationId { get; }
        public int TemplateTypeId { get; }
        public bool Response { get; }
    }
}
