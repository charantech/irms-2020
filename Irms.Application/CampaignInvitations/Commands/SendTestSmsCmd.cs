﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class SendTestSmsCmd : IRequest<IEnumerable<string>>
    {
        public Guid InvitationId { get; set; }
        public IEnumerable<string> SmsList { get; set; }
    }
}
