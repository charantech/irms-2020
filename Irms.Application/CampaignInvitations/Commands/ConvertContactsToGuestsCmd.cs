﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class ConvertContactsToGuestsCmd : IRequest<Guid>
    {
        public Guid CampaignId { get; set; }
    }
}
