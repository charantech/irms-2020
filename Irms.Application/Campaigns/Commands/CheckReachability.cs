﻿using System;
using Irms.Application.Campaigns.ReadModels;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class CheckReachability : IRequest<(bool reachabilitySubscribed, ReachabilityStats stats)>
    {
        public CheckReachability(Guid listId)
        {
            GuestListId = listId;
        }

        public Guid GuestListId { get; }
    }
}
