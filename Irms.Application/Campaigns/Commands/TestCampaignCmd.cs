﻿using Irms.Application.CampaignInvitations.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Campaigns.Commands
{
    public class TestCampaignCmd : IRequest<TestCampaignSendInfoModel>
    {
        public Guid Id { get; set; }
        public IEnumerable<string> Emails { get; set; }
        public IEnumerable<string> PhoneNumbers { get; set; }
        public IEnumerable<string> WhatsappNumbers { get; set; }
    }
}
