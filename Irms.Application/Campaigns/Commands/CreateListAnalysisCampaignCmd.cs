﻿using System;
using Irms.Application.Campaigns.ReadModels;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class CreateListAnalysisCampaignCmd : IRequest<ListAnalysisCampaignResponse>
    {
        public Guid EventId { get; set; }
        public Guid GuestListId { get; set; }
    }
}
