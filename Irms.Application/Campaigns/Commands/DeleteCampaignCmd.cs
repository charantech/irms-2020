﻿using System;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class DeleteCampaignCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
    }
}
