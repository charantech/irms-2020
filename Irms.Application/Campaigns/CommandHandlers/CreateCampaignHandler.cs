﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;
using System.Linq;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class CreateCampaignHandler : IRequestHandler<CreateCampaignCmd, Guid>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public CreateCampaignHandler(
            ICampaignRepository<Campaign, Guid> repo,
            IMediator mediator,
            IMapper mapper
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateCampaignCmd message, CancellationToken token)
        {
            var campaign = _mapper.Map<Campaign>(message);
            campaign.Create();
            //accept code
            while (true)
            {
                string code = GenerateUniqueCode();
                bool isUnique = await _repo.CheckBOTCodeUniqueness(code, token);
                if (isUnique)
                {
                    campaign.AcceptCode = code;
                    break;
                }
            }

            //reject code
            while (true)
            {
                string code = GenerateUniqueCode();
                bool isUnique = await _repo.CheckBOTCodeUniqueness(code, token);
                if (isUnique)
                {
                    campaign.RejectCode = code;
                    break;
                }
            }

            await _repo.Create(campaign, token);

            await _mediator.Publish(new CampaignCreated(campaign.Id), token);
            return campaign.Id;
        }

        private string GenerateUniqueCode()
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 3)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}