﻿using System;

namespace Irms.Application.Campaigns.ReadModels
{
    public class GuestInfoListItem
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
    }
}
