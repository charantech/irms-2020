﻿using System;

namespace Irms.Application.Campaigns.ReadModels
{
    public class ListAnalysisCampaignResponse
    {
        public Guid CampaignId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public Guid RfiFormId { get; set; }
    }
}
