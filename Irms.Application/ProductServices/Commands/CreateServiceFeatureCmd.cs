﻿using Irms.Application.Product.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.ProductServices.Commands
{
    public class CreateServiceFeatureCmd : IRequest<Guid>
    {
        public CreateServiceFeatureCmd(Guid serviceId)
        {
            ServiceId = serviceId;
        }
        public ServiceFeature ServiceFeature { get; set; }
        public Guid ServiceId { get; set; }
    }
}
