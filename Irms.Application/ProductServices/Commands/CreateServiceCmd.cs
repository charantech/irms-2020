﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.ProductServices.Commands
{
    public class CreateServiceCmd : IRequest<Guid>
    {
        public CreateServiceCmd(string title, bool isActive)
        {
            Title = title;
            IsActive = isActive;
        }

        public string Title { get; }
        public bool IsActive { get; }
    }
}
