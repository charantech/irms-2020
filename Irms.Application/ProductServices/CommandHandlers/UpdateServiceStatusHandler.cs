﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.ProductServices.Commands;
using Irms.Application.ProductServices.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.ProductServices.CommandHandlers
{
    public class UpdateServiceStatusHandler : IRequestHandler<UpdateServiceStatusCmd, Unit>
    {
        private readonly IMediator _mediator;
        private readonly IServiceRepository<ServiceCatalog, Guid> _repository;
        public UpdateServiceStatusHandler(IMediator mediator, IServiceRepository<Irms.Domain.Entities.ServiceCatalog, Guid> repository)
        {
            _mediator = mediator;
            _repository = repository;
        }

        public async Task<Unit> Handle(UpdateServiceStatusCmd request, CancellationToken token)
        {
            var service = await _repository.GetById(request.ServiceId, token);
            if(service == null)
            {
                throw new IncorrectRequestException($"Service not found by id: {request.ServiceId}");
            }
            if(request.IsServiceActive)
            {
                service.Activate();
            }
            else
            {
                service.Deactivate();
            }

            await _repository.Update(service, token);

            await _mediator.Publish(new ServiceStatusUpdated(service.Id), token);
            return Unit.Value;
        }
    }
}
