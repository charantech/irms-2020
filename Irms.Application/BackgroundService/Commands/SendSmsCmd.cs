﻿using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations;
using MediatR;

namespace Irms.Application.BackgroundService.Commands
{
    public class SendSmsCmd : IRequest<(bool sent, SmsMessage msg)>
    {
        public SendSmsCmd(InvitationMessage msg)
        {
            Msg = msg;
        }

        public InvitationMessage Msg { get; set; }
    }
}
