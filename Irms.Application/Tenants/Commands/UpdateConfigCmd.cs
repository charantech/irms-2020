﻿using System;
using MediatR;

namespace Irms.Application.Tenants.Commands
{
    public class UpdateConfigCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public bool HasOwnDomain { get; set; }
        public string ClientUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string EmailFrom { get; set; }
    }
}
