﻿using System;
using MediatR;

namespace Irms.Application.Tenants.Commands
{
    public class CreateTenantCmd : IRequest<Guid>
    {
        //tenant info
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Logo { get; set; }
        public Guid CountryId { get; set; }
        public string City { get; set; }

        //contact info
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
    }
}
