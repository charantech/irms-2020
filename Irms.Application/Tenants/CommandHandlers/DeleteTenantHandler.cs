﻿//using System;
//using System.Threading;
//using System.Threading.Tasks;
//using MediatR;
//using Irms.Application.Abstract;
//using Irms.Application.Tenants.Commands;
//using Irms.Application.Tenants.Events;
//using Irms.Domain.Entities.Tenant;
//using Irms.Application.Abstract.Repositories;

//namespace Irms.Application.Tenants.CommandHandlers
//{
//    public class DeleteTenantHandler : IRequestHandler<DeleteTenantCmd, Unit>
//    {
//        private readonly ITenantRepository<Tenant, Guid> _tenantRepository;
//        private readonly IMediator _mediator;
//        private readonly ITransactionManager _transactionManager;

//        public DeleteTenantHandler(
//            ITenantRepository<Tenant, Guid> tenantRepository,
//            IMediator mediator,
//            ITransactionManager transactionManager)
//        {
//            _tenantRepository = tenantRepository;
//            _mediator = mediator;
//            _transactionManager = transactionManager;
//        }

//        public async Task<Unit> Handle(DeleteTenantCmd message, CancellationToken cancellationToken)
//        {
//            foreach (var tenantId in message.TenantIds)
//            {
//                await _tenantRepository.Disable(tenantId, cancellationToken);

//                var e = new TenantDeleted(tenantId);
//                await _mediator.Publish(e, cancellationToken);
//            }

//            return Unit.Value;
//        }
//    }
//}
