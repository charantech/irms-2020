﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Tenants.Commands;
using Irms.Application.Tenants.Events;
using Irms.Domain.Entities.Tenant;
using Irms.Application.Abstract.Repositories;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Files.Commands;

namespace Irms.Application.Tenants.CommandHandlers
{
    public class CreateTenantHandler : IRequestHandler<CreateTenantCmd, Guid>
    {
        private readonly ITenantRepository<Tenant, Guid> _tenantRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public CreateTenantHandler(
            ITenantRepository<Tenant, Guid> tenantRepository,
            IMediator mediator,
            IMapper mapper)
        {
            _tenantRepository = tenantRepository;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateTenantCmd message, CancellationToken token)
        {
            string fileName = null;
            //upload logo to azure storage
            if (message.Logo != null)
            {
                fileName = $"{FilePath.GetFileDirectory(SystemModule.Customers)}{Guid.NewGuid()}_logo.png";
                await _mediator.Send(new UploadFileCmd(fileName, message.Logo, FileType.Picture), token);
            }

            var tenant = _mapper.Map<Tenant>(message);
            var contactInfo = _mapper.Map<TenantContactInfo>(message);
            tenant.Create();
            tenant.SetLogoPath(fileName);
            tenant.CreateContactInfo(contactInfo);

            await _tenantRepository.Create(tenant, token);

            var tenantCreated = new TenantCreated(tenant.Id);
            await _mediator.Publish(tenantCreated, token);

            return tenant.Id;
        }
    }
}
