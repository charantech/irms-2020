﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Tenants.Events
{
    public class TenantUpdated : IEvent
    {
        public TenantUpdated(Guid tenantId)
        {
            TenantId = tenantId;
        }

        public Guid TenantId { get; }
        public Guid ObjectId => TenantId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The basic info of the tenant {x(TenantId)} has been changed by {u}";
    }
}
