﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Tenants.Events
{
    public class TenantConfigUpdated : IEvent
    {
        public TenantConfigUpdated(Guid tenantId)
        {
            TenantId = tenantId;
        }

        public Guid TenantId { get; }

        public Guid ObjectId => TenantId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The configuration of the tenant {x(TenantId)} has been changed by {u}";
    }
}
