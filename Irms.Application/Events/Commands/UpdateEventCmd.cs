﻿using System;
using MediatR;

namespace Irms.Application.Events.Commands
{
    public class UpdateEventCmd : IRequest<Unit>
    {
        //event
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public short? EventTypeId { get; set; }
        public bool IsLimitedAttendees { get; set; }
        public int? MaximumAttendees { get; set; }
        public int? OverflowAttendees { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string TimeZoneUtcOffset { get; set; }
        public string TimeZoneName { get; set; }

        //location
        public string LocationName { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Country { get; set; }

    }
}
