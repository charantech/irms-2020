﻿using System;
using MediatR;

namespace Irms.Application.Events.Commands
{
    public class DeleteEventCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
    }
}
