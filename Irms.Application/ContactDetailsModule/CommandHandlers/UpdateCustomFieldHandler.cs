﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CustomFields.Commands;
using Irms.Application.DataModule.Events;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class UpdateCustomFieldHandler : IRequestHandler<Commands.UpdateCustomFieldCmd, Unit>
    {
        private readonly ICustomFieldRepository<Domain.Entities.CustomField, Guid> _repo;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        public UpdateCustomFieldHandler(
            ICustomFieldRepository<Domain.Entities.CustomField, Guid> repo,
            IMapper mapper,
            IMediator mediator,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository
            )
        {
            _mapper = mapper;
            _repo = repo;
            _mediator = mediator;
            _contactRepository = contactRepository;
        }

        public async Task<Unit> Handle(Commands.UpdateCustomFieldCmd request, CancellationToken token)
        {
            var contactCustomFields = await _repo.LoadCustomFieldValues(request.Id, token);

            bool wasEdited = false;
            foreach (var customField in request.Data.Fields)
            {
                var contactCustomField = contactCustomFields.FirstOrDefault(x => x.CustomFieldId == customField.Id);
                if (string.IsNullOrEmpty(customField.Value))
                {
                    if(contactCustomField != null)
                    {
                        wasEdited = true;
                        await _repo.DeleteContactCustomField(contactCustomField.Id, token);
                    }
                }
                else
                {
                    wasEdited = true;
                    await _repo.UpdateContactCustomField(request.Id, customField.Title, customField.Value, token);
                }
            }

            if (wasEdited)
            {
                var user = await _contactRepository.GetById(request.Id, token);
                await _contactRepository.Update(user, token);
            }

            await _mediator.Publish(new CustomFieldUpdated(request.Id), token);

            return Unit.Value;
        }
    }
}
