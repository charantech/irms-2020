﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.Events;
using Irms.Application.DataModule.ReadModels;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class UpdatePersonalInfoHandler : IRequestHandler<UpdatePersonalInfoCmd, PersonalInfoModel>,
        IRequestHandler<UpdateGlobalContactPersonalInfoCmd, PersonalInfoModel>
    {
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        public UpdatePersonalInfoHandler(
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository,
            IMediator mediator,
            IMapper mapper
            )
        {
            _mapper = mapper;
            _mediator = mediator;
            _contactRepository = contactRepository;
        }

        public async Task<PersonalInfoModel> Handle(UpdatePersonalInfoCmd request, CancellationToken token)
        {
            var contact = await _contactRepository.GetById(request.Id, token);

            _mapper.Map(request, contact);
            contact.DocumentTypeId = request.DocumentTypeId;
            await _contactRepository.Update(contact, token);

            await _mediator.Publish(new PersonalInfoUpdated(request.Id), token);

            return new PersonalInfoModel
            {
                Id = contact.Id,
                FullName = contact.FullName
            };
        }

        /// <summary>
        /// update global contact info
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<PersonalInfoModel> Handle(UpdateGlobalContactPersonalInfoCmd request, CancellationToken token)
        {
            var contact = await _contactRepository.GetById(request.Id, token);

            _mapper.Map(request, contact);
            contact.DocumentTypeId = request.DocumentTypeId;
            await _contactRepository.Update(contact, token);

            await _mediator.Publish(new PersonalInfoUpdated(request.Id), token);

            return new PersonalInfoModel
            {
                Id = contact.Id,
                FullName = contact.FullName
            };
        }
    }
}
