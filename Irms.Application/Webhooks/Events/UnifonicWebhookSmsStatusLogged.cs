﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Webhooks.Events
{
    public class UnifonicWebhookSmsStatusLogged : ITenantEvent
    {
        public UnifonicWebhookSmsStatusLogged(Guid logId, Guid tenantId)
        {
            LogId = logId;
            TenantId = tenantId;
        }

        public Guid LogId { get; }
        public string Name { get; }
        public Guid TenantId { get; }

        public Guid ObjectId => LogId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x) => $"The unifonic webook message {x(LogId)} status has been logged";
    }
}
