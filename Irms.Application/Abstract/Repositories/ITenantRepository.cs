﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using Irms.Domain.Entities.Tenant;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface ITenantRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task Disable(Guid id, CancellationToken cancellationToken);
        Task UpdateContactInfo(TenantContactInfo contactInfo, CancellationToken token);
    }
}
