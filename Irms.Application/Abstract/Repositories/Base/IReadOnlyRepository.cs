﻿using System.Threading;
using System.Threading.Tasks;
using Irms.Domain.Abstract;

namespace Irms.Application.Abstract.Repositories.Base
{
    public interface IReadOnlyRepository<TEntity, in TKey> : IBaseRepository
        where TEntity : IEntity<TKey>
    {
        Task<TEntity> GetById(TKey id, CancellationToken token);
    }
}
