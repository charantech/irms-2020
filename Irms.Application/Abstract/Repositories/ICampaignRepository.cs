﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Campaigns.Commands;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;

namespace Irms.Application.Abstract.Repositories
{
    public interface ICampaignRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task UpdatePreferredMedia(UpdatePreferredMediaCmd request, CancellationToken token);
        Task<bool[]> GetExistingMedias(Guid id, CancellationToken token);
        Task<(string name, string email)> GetTenantAdmin(CancellationToken token);
        Task<bool> CheckBOTCodeUniqueness(string code, CancellationToken token);
    }
}
