﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;

namespace Irms.Application.Abstract.Repositories
{
    public interface IContactListToContactsRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : IEntity<TKey>
    {
        Task CreateRange(List<ContactListToContact> entity, CancellationToken token);
        Task DeleteRangeFromContactList(IEnumerable<Guid> contactIds, Guid contactListId, CancellationToken token);
        Task<IEnumerable<Domain.Entities.ContactListToContact>> GetAllForContactList(Guid id, CancellationToken token);
        Task DeleteRangeByIds(IEnumerable<Guid> ids, CancellationToken token);
    }
}
