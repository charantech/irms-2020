﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.BackgroundService.ReadModels;
using Irms.Application.CampaignInvitations;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface IBackgroundServiceRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task LogMessages(List<CampaignInvitationMessageLog> logs, CancellationToken token);
        Task ChangeInvitationStatus(Guid campaignInvitationId, InvitationStatus sent, CancellationToken token);
        Task ChangeGuestsStatus(IEnumerable<CampaignInvitationMessageLog> guests, CancellationToken token);
        Task<CampaignInvitationMessageLog> GetMessageById(string messageSid, CancellationToken token);
        Task LogProviderResponse(ProviderResponse response, CancellationToken token);
        Task<IEnumerable<CampaignInvitationMessageLog>> GetMessagesByEmails(IEnumerable<string> emails, CancellationToken token);
        Task<InstantBotResponse> GetInvitationResponse(string from, string body, CancellationToken token);
        Task UpdateInvitationResponse(CampaignInvitationResponse template, CancellationToken token);
    }
}
