﻿namespace Irms.Application.Abstract
{
    public interface IEmailReachabilityManager
    {
        bool CheckEmailReachability(string email);
    }
}
