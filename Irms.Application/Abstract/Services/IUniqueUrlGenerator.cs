﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Abstract.Services
{
    public interface IUniqueUrlGenerator
    {
        public string Generate();
    }
}
