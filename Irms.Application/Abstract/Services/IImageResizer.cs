﻿using System.IO;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services
{
    public interface IImageResizer
    {
        Task ResizeImage(Stream inputStream, Stream outputStream, int maxDimension, int quality);
    }
}
