﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Abstract.Services
{
    public interface IPhoneNumberValidator
    {
        IValidPhoneNumber Validate(string unvalidatedPhone, out bool isValid, string defaultCountry = default);
        (bool isValid, IValidPhoneNumber number) Validate(string unvalidatedPhone, string defaultCountry = default);
        bool CheckPhoneValidation(string phone);
    }
}
