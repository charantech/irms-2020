﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Abstract.Services
{
    public interface IValidPhoneNumber
    {
        string Number { get; }
    }
}
