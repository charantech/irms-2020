﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications
{
    public interface ISmsBGSender
    {
        Task<bool> SendSms(Guid tenantId, SmsMessage message, CancellationToken token);
        Task<bool> SendWebhookSms(Guid tenantId, SmsMessage sms, CancellationToken token);
    }
}
