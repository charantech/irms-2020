﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Irms.Application.Abstract.Services.Notifications
{
    /// <summary>
    /// This class represent minimum requirements for sending the Email
    /// </summary>
    public class EmailMessage
    {
        public EmailMessage(
            string htmlTemplate,
            IEnumerable<Recipient> recipients,
            Guid? campaignInvitationId = null)
        {
            HtmlTemplate = htmlTemplate;
            Recipients = recipients;
            CampaignInvitationId = campaignInvitationId;
        }

        public string MessageId { get; set; }
        public ProviderType ProviderType { get; set; }
        public Guid? CampaignInvitationId { get; set; }
        public string HtmlTemplate { get; }
        public IEnumerable<Recipient> Recipients { get; }

        public class Recipient
        {
            public Recipient(
                string emailAddress,
                string subject,
                IEnumerable<TemplateVariable> templateVariables = null)
            {
                EmailAddress = emailAddress;
                Subject = subject;
                TemplateVariables = templateVariables ?? Enumerable.Empty<TemplateVariable>();
            }

            public Recipient(
                Guid id,
                string emailAddress,
                string subject,
                IEnumerable<TemplateVariable> templateVariables = null)
            {
                Id = id;
                EmailAddress = emailAddress;
                Subject = subject;
                TemplateVariables = templateVariables ?? Enumerable.Empty<TemplateVariable>();
            }

            public Guid Id { get; }
            public string EmailAddress { get; }
            public string Subject { get; }
            public IEnumerable<TemplateVariable> TemplateVariables { get; }
        }

        public class TemplateVariable
        {
            public TemplateVariable(string name, string value)
            {
                Name = name;
                Value = value;
            }

            public string Name { get; }
            public string Value { get; }
        }
    }
}
