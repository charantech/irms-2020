﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface IHangfireJobsWorker
    {
        void SetupRsvpSending(Guid campaignInvitationId, DateTime when);
        void SetupPendingSending(Guid campaignInvitationId, DateTime when);
        //Task StartUnifonicWebhookAsync(CancellationToken none);
        void SetupAcceptedSending(Guid campaignInvitationId, Guid contactId, DateTime when);
        void SetupRejectedSending(Guid campaignInvitationId, Guid contactId, DateTime when);
        void SetupListAnalysisSending(Guid campaignInvitationId, DateTime startDate);
    }
}
