﻿using System;
using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface ILanguageIds
    {
        string EnCulture { get; }
        string ArCulture { get; }

        Task<Guid> En();
        Task<Guid> Ar();
    }
}
