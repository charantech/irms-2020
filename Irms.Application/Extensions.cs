﻿using Irms.Application.Abstract.Repositories.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Irms.Application
{
    public static class Extensions
    {
        public static bool IsNotNullOrEmpty(this string str) => !string.IsNullOrEmpty(str);
        public static string GetErrorMessage(this IIdentityResult result)
        {
            return string.Join(",", result.Errors.Select(x => $"{x.Code}: {x.Description}"));
        }

        public static (IEnumerable<TNew> toAdd, IEnumerable<TOld> toDel, IEnumerable<(TOld o, TNew n)> toUpd) Compare<TOld, TNew>(
            this IEnumerable<TOld> oldItems, IEnumerable<TNew> newItems, Func<TOld, TNew, bool> matcher)
        {
            var toAdd = newItems.Where(n => !oldItems.Any(o => matcher(o, n)));
            var toDel = oldItems.Where(o => !newItems.Any(n => matcher(o, n)));
            var toUpdate = oldItems.Select(o => (o, n: newItems.FirstOrDefault(n => matcher(o, n)))).Where(x => x.n != null);
            return (toAdd.ToList(), toDel.ToList(), toUpdate.ToList());
        }

        public static string SplitCamelCase(this string input) => Regex.Replace(input, "(?<=[a-z])([A-Z])", " $1", RegexOptions.Compiled).Trim();

        #region replace placeholder with default value...
        public static string ReplaceDefaultValuePlaceholders(this string body)
        {
            while (true)
            {
                var pos1 = body.IndexOf("{{");
                int pos2 = body.IndexOf("}}");
                if (pos1 == -1 || pos2 == -1)
                {
                    break;
                }

                body = body.Replace(body.Substring(pos1, pos2 - pos1 + 2), ExtractDefaultValue(body.Substring(pos1, pos2 - pos1 + 2)));
            }

            return body;
        }

        public static string ExtractDefaultValue(string placeholder)
        {
            int pos = placeholder.IndexOf("|");
            if (pos > -1)
            {
                return placeholder.Substring(pos + 1).Replace("}}", string.Empty).Replace("&nbsp;", string.Empty).Trim();
            }
            return placeholder.Replace("{{", string.Empty).Replace("}}", string.Empty);
        }
        #endregion
    }
}