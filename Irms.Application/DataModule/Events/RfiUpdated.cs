﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Events
{
    public class RfiUpdated : IEvent
    {
        public RfiUpdated(Guid id)
        {
            Id = id;
        }
        public Guid Id  { get; }

        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Rfi form {x(Id)} has been updated by {u}";
    }
}
