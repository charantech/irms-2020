﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Events
{
    public class AddedImportantFields : IEvent
    {
        public AddedImportantFields(Guid contactListId)
        {
            ContactListId = contactListId;
        }
        public Guid ContactListId { get; }

        public Guid ObjectId => ContactListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact list important fields {x(ContactListId)} was updated by {u}";
    }
}
