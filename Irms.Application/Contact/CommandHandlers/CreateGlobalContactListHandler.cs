﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Events.Events;
using Irms.Application.Contact.Commands;

namespace Irms.Application.Contact.CommandHandlers
{
    public class CreateGlobalContactListHandler : IRequestHandler<CreateGlobalContactListCmd, Guid?>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public CreateGlobalContactListHandler(
            TenantBasicInfo tenant,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator,
            IMapper mapper
            )
        {
            _contactListRepository = contactListRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid?> Handle(CreateGlobalContactListCmd message, CancellationToken token)
        {
            if (await _contactListRepository.IsNameAlreadyExists(message.Name, token))
            {
                return null;
            }

            var contactList = _mapper.Map<ContactList>(message);
            contactList.Create(_tenant.Id);
            contactList.SetGlobal();

            await _contactListRepository.Create(contactList, token);

            var globalContactListCreated = new GlobalContactListCreated(contactList.Id);
            await _mediator.Publish(globalContactListCreated, token);

            return contactList.Id;
        }
    }
}
