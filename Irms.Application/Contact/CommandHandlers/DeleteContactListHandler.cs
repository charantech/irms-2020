﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class DeleteContactListHandler : IRequestHandler<DeleteContactListCmd, Guid?>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public DeleteContactListHandler(TenantBasicInfo tenant,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator,
            IMapper mapper)
        {
            _contactListRepository = contactListRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// Removes contact list with data in it
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid?> Handle(DeleteContactListCmd message, CancellationToken token)
        {
            if (await _contactListRepository.IsContactListAssignToACampaign(message.Id, token))
            {
                throw new IncorrectRequestException("Contact list is assigned to a campaign!");
            }

            await _contactListRepository.Delete(message.Id, token);

            var contactListRemoved = new ContactListRemoved(message.Id);
            await _mediator.Publish(contactListRemoved, token);

            return message.Id;
        }
    }
}
