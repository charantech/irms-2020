﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Contact.Events
{
    public class GuestListCreated : IEvent
    {
        public GuestListCreated(Guid contactListId)
        {
            ContactListId = contactListId;
        }
        public Guid ContactListId { get; }

        public Guid ObjectId => ContactListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The guest list  {x(ContactListId)} has been created by {u}";
    }
}
