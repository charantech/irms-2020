﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Events.Events
{
    public class GlobalContactCreated : IEvent
    {
        public GlobalContactCreated(Guid contactId)
        {
            ContactId = contactId;
        }

        public Guid ContactId { get; }

        public Guid ObjectId => ContactId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact {x(ContactId)} has been created by {u}";
    }
}
