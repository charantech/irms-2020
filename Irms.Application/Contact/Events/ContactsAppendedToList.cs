﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Irms.Application.Contact.Events
{
    /// <summary>
    /// Event that fires after contacts were added to the list
    /// </summary>
    public class ContactsAppendedToList : IEvent
    {
        public ContactsAppendedToList(Guid contactListId, IEnumerable<Guid> contactIds)
        {
            ContactListId = contactListId;
            ContactIds = contactIds;
        }
        public Guid ContactListId { get; }

        public IEnumerable<Guid> ContactIds { get; }

        public Guid ObjectId => ContactListId;

        public Guid? ObjectId2 => null;

        public Guid? ObjectId3 => null;

        public string Format(Func<Guid, string> x, string u)
        {
            string defaultMessage = $"The guest list {x(ContactListId)} has been appended by {u}";
            if (ContactIds.Count() > 0)
            {
                defaultMessage += $". Contacts with ids was added: { string.Join(",", ContactIds.Select(x => x.ToString())) }.";
            }
            return defaultMessage;
        }
    }
}
