﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Contact.Commands
{
    public class AddGuestsInGuestListCmd : IRequest<Guid>
    {
        public Guid ListId { get; set; }
        public List<Guid> GuestIds { get; set; }
    }
}
