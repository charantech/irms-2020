﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Contact.Commands
{
    public class DeleteContactCmd : IRequest<Unit>
    {
        public List<Guid> Ids { get; set; }
    }
}
