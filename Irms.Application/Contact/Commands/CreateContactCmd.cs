﻿using System;
using MediatR;

namespace Irms.Application.Contact.Commands
{
    public class CreateContactCmd : IRequest<Guid?>
    {
        public Guid? ContactListId { get; set; }
        public bool IsGuest { get; set; }
        public string ContactList { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public int? GenderId { get; set; }

        public string Email { get; set; }
        public string AlternativeEmail { get; set; }

        public string MobileNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }

        public string WorkNumber { get; set; }
        public Guid? NationalityId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public Guid? IssuingCountryId { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
    }
}
