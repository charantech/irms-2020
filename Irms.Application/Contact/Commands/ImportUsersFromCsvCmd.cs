﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Commands
{
    public class ImportUsersFromCsvCmd : IRequest<Unit>
    {
        public ImportUsersFromCsvCmd(Dictionary<string, string> listInfo, List<Dictionary<string, string>> contacts)
        {
            ListInfo = listInfo;
            Contacts = contacts;
        }

        public Dictionary<string, string> ListInfo { get; set; }
        public List<Dictionary<string, string>> Contacts { get; set; }
    }
}
