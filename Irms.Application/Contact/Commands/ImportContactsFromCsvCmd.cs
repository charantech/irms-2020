﻿using MediatR;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Commands
{
    public class ImportContactsFromCsvCmd : IRequest<Unit>
    {
        public List<Dictionary<string, string>> Contacts { get; set; }
        public ImportCsvListInfo List { get; set; }

        public class ImportCsvListInfo
        {
            public Guid? EventId { get; set; }
            public Guid? EventGuestId { get; set; }
            public string List { get; set; }
            public Guid? ListId { get; set; }
            public string ListName { get; set; }
        }
    }
}
