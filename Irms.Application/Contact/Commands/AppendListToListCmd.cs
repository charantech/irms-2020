﻿using MediatR;
using System;

namespace Irms.Application.Contact.Commands
{
    public class AppendListToListCmd : IRequest<Unit>
    {
        public Guid FromListId { get; set; }
        public Guid ToListId { get; set; }
    }
}
