﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Product.Events
{
    class ProductUpdated : IEvent
    {
        public ProductUpdated(Guid productId, string productName)
        {
            ProductId = productId;
            ProductName = productName;
        }

        public Guid ProductId { get; }
        public string ProductName { get; }

        public Guid ObjectId => ProductId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"{u} has updated the product \"{ProductName}\" with ID {x(ProductId)}";
    }
}
