﻿using MediatR;
using System;

namespace Irms.Application.Product.Commands
{
    public class DeleteProductByIdCmd : IRequest<Unit>
    {
        public DeleteProductByIdCmd(Guid productId)
        {
            ProductId = productId;
        }

        public Guid ProductId { get; }
    }
}
