﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Product.Commands;
using Irms.Application.Product.Events;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Product.CommandHandlers
{
    public class CreateProductHandler : IRequestHandler<CreateProductCmd, Guid>
    {
        private readonly IMediator _mediator;
        private readonly IProductRepository<Irms.Domain.Entities.Product, Guid> _repository;
        private readonly IRepository<Irms.Domain.Entities.LicensePeriod, byte> _licenseRepository;
        private readonly IMapper _mapper;
        private readonly ITransactionManager _transactionManager;

        public CreateProductHandler(IMediator mediator,
                                    IProductRepository<Irms.Domain.Entities.Product, Guid> repository,
                                    IRepository<Irms.Domain.Entities.LicensePeriod, byte> licenseRepository,
                                    IMapper mapper,
                                    ITransactionManager transactionManager)
        {
            _mediator = mediator;
            _repository = repository;
            _mapper = mapper;
            _transactionManager = transactionManager;
            _licenseRepository = licenseRepository;
        }
        /// <summary>
        /// this method handle Create Product request. 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateProductCmd request, CancellationToken cancellationToken)
        {
            var serviceCatalog = _mapper.Map<List<Domain.Entities.ServiceCatalog>>(request.Services);             

            Guid id;
            //using Transaction Manager for controlling the process of creation new Product
            //if something went wrong Transaction Manager will roll back all changes
            using (var tr = await _transactionManager.BeginTransactionAsync(cancellationToken))
            {
                var license = new Domain.Entities.LicensePeriod(); 
                license.Days = request.LicenseDays;
                license.Title = request.LicenseName;

                var createProductRequest = new Domain.Entities.Product(
                request.Id,
                request.ProductName,
                request.Price,
                request.DisountPercentage,
                request.IsShowOnWeb,
                request.CurrencyId,
                license,
                serviceCatalog);

                id = await _repository.Create(createProductRequest, cancellationToken);

                tr.Commit();
            }
            await _mediator.Publish(new ProductCreated(id, request.ProductName));
            return id;
        }
    }
}
