﻿using System;

namespace Irms.Data.Read.Subscription.ReadModels
{
    public class SubscriptionListItem
    {
        public Guid Id { get; set; }
        public string Product { get; set; }
        public string Tenant { get; set; }
        public DateTime ExpiryDateTime { get; set; }
        public bool IsActive { get; set; }
    }
}
