﻿using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Data.Read.Contact.ReadModels.EmailsUniqueListReadModel;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetEmailUniqueCheckQueryHandler: IRequestHandler<GetEmailsUniqueCheck, EmailsUniqueListReadModel>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _repo;
        private readonly IMediator _mediator;
        public GetEmailUniqueCheckQueryHandler(
            TenantBasicInfo tenant,
            IContactRepository<Domain.Entities.Contact, Guid> repo,
            IMediator mediator
        )
        {
            _tenant = tenant;
            _repo = repo;
            _mediator = mediator;
        }

        public async Task<EmailsUniqueListReadModel> Handle(GetEmailsUniqueCheck request, CancellationToken token)
        {
            var data = await _repo.GetExistingEmails(request.Emails, token);
            var result = new List<EmailUniqueReadModel>();

            request.Emails
                .ToList()
                .ForEach(x =>
                {
                    if (data.Contains(x))
                    {
                        result.Add(new EmailUniqueReadModel
                        {
                            Email = x,
                            IsExists = true
                        });
                    }
                    else
                    {
                        result.Add(new EmailUniqueReadModel
                        {
                            Email = x,
                            IsExists = false
                        });
                    }
                });

            return new EmailsUniqueListReadModel { Data = result };
        }
    }
}
