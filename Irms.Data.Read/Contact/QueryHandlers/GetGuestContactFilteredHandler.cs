﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.Queries;
using System.Threading.Tasks;
using Irms.Data.Read.Contact.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetGuestContactFilteredHandler : IPageHandler<GetGuestContactByListFiltered, GuestContactItemFiltered>
    {
        private const string CountQuery = @"
                SELECT
                  COUNT(c.Id)
                FROM dbo.Contact as c
                INNER JOIN dbo.ContactListToContacts as cltc on cltc.ContactId = c.Id 
                INNER JOIN dbo.ContactList as cl on cl.Id=cltc.ContactListId
                WHERE cltc.TenantId = @tenant
                  AND cl.IsGuest = @guest
                  AND cltc.ContactListId = @listId 
                {0}";

        private const string PageQuery = @"
                WITH STATUS AS (
	               SELECT 
		               MAX(COALESCE(cir.Answer, 0) )AS Answer, 
		                cl.Id AS contactListId, 
		                e.id AS eventId,
						c.id AS contactId
	                FROM ContactList cl 
		                INNER JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
		                INNER JOIN dbo.Event AS e ON ec.EventId = e.Id
						INNER JOIN dbo.ContactListToContacts cltc ON cltc.ContactListId = cl.Id
						INNER JOIN dbo.Contact AS c ON cltc.ContactId = c.Id
		                INNER JOIN dbo.CampaignInvitation AS ci ON ci.EventCampaignId = ec.Id AND ci.InvitationTypeId IN (0,1)
		                LEFT JOIN dbo.CampaignInvitationResponse AS cir ON 
						cir.CampaignInvitationId = ci.Id AND c.Id = cir.ContactId
	                WHERE cl.Id = @listId
	                AND e.Id = @eventId
					AND ec.Id = (SELECT TOP 1 ec.Id FROM EventCampaign ec
										WHERE ec.GuestListId= @listId
										AND ec.Active = 1 
										ORDER BY ec.CreatedOn DESC)
					GROUP BY cl.id, e.id, c.id
                )
                SELECT 
						c.Id,
                       c.FullName,
                       c.Email,
	                   c.MobileNumber,
	                   c.CreatedOn,
	                   cltc.ContactListId,
                       CASE
			                WHEN st.Answer IS NULL OR st.Answer = 0 OR st.Answer = 3 THEN 'Pending'
			                WHEN st.Answer = 1 THEN 'Accepted'
			                WHEN st.Answer = 2 THEN 'Rejected'
                       END AS Status
                FROM dbo.Contact as c
                       INNER JOIN dbo.ContactListToContacts AS cltc ON cltc.ContactId = c.Id
                       INNER JOIN dbo.ContactList AS cl ON cl.Id=cltc.ContactListId 
                       INNER JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
                       INNER JOIN dbo.Event AS e ON ec.EventId = e.Id
                       LEFT JOIN STATUS AS st ON st.eventId = e.Id AND st.contactListId = cl.Id AND st.contactId = c.Id
                WHERE cltc.ContactListId = @listId
		                AND e.Id = @eventId
						AND ec.Id = (SELECT TOP 1 ec.Id FROM EventCampaign ec
										WHERE ec.GuestListId= @listId 
										AND ec.Active = 1 
										ORDER BY ec.CreatedOn DESC)
                ORDER BY c.CreatedOn DESC
                OFFSET @skip ROWS
                FETCH NEXT @take ROWS ONLY";

        private const string InformationQuery = @"
            SELECT c.Id,
                c.FullName,
                c.Email,
                c.MobileNumber,
                c.CreatedOn,
                c.ModifiedOn,
                cltc.ContactListId
              FROM dbo.Contact as c
              INNER JOIN dbo.ContactListToContacts as cltc on cltc.ContactId = c.Id
                INNER JOIN dbo.ContactList as cl on cl.Id=cltc.ContactListId 
              WHERE cltc.TenantId = @tenant
                  AND cl.IsGuest = @guest
                  AND cltc.ContactListId = @listId 
                {0}
              ORDER BY c.CreatedOn DESC
              OFFSET @skip ROWS
              FETCH NEXT @take ROWS ONLY
";

        private const string IsReadonlyQuery = @"
             SELECT DISTINCT COALESCE(active, 0) 
             FROM eventCampaign ec 
             WHERE ec.GuestListId = '{0}' 
             GROUP BY active 
             HAVING active > 0";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        private const string SearchClause = "AND c.FullName like @searchText";

        public GetGuestContactFilteredHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// this method will return list of guest contacts by listId and search text by FullName with pagination 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<GuestContactItemFiltered>> Handle(GetGuestContactByListFiltered request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            var parameters = new
            {
                tenant = _tenant.Id,
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                guest = request.IsGuest,
                listId = request.ListId,
                eventId = request.EventId,
                skip = request.Skip(),
                take = request.Take()
            };

            var conditionStr = string.Join("\r\n", clauses);
            var countQuery = string.Format(CountQuery, conditionStr);

            var isReadonlyQuery = string.Format(IsReadonlyQuery, request.ListId);
            var pageQuery = string.Format(PageQuery, conditionStr);
            var informationQuery = string.Format(InformationQuery, conditionStr);

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
                var isReadonly = await connection.ExecuteScalarAndLog<bool>(isReadonlyQuery);
                IEnumerable<GuestContactItemFiltered> output;
                if (isReadonly)
                    output = await connection.QueryAndLog<GuestContactItemFiltered>(pageQuery, parameters);
                else
                    output = await connection.QueryAndLog<GuestContactItemFiltered>(informationQuery, parameters);

                return new PageResult<GuestContactItemFiltered>(output, total, isReadonly);
            }
        }
    }
}
