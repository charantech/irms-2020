﻿using MediatR;

namespace Irms.Data.Read.Contact.Queries
{
    public class IsPhoneUnique : IRequest<bool>
    {
        public IsPhoneUnique(string mobileNumber)
        {
            MobileNumber = mobileNumber;
        }

        public string MobileNumber { get; }
    }
}
