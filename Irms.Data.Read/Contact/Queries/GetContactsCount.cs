﻿using System;
using MediatR;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactsCount : IRequest<long>
    {
        public GetContactsCount(bool isGuest)
        {
            IsGuest = isGuest;
        }

        public bool IsGuest { get; }
    }
}
