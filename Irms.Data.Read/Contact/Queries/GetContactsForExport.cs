﻿using Irms.Data.Read.Contact.ReadModels;
using Irms.Infrastructure.Services.ExcelExport;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactsForExport : IRequest<ContactsExportModel>
    {
        public Guid ContactListId { get; set; }

        public IEnumerable<Guid> ContactIds { get; set; }

        public string Filename { get; set; }

        public IEnumerable<string> Columns { get; set; }

        public IEnumerable<Guid> CustomFieldIds { get; set; }

        public Guid EventId { get; set; }
    }
}
