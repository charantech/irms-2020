﻿using MediatR;

namespace Irms.Data.Read.Contact.Queries
{
    public class IsEmailUnique : IRequest<bool>
    {
        public IsEmailUnique(string email)
        {
            Email = email;
        }

        public string Email { get; }
    }
}
