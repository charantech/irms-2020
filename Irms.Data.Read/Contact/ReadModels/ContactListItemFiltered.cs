﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactListItemFiltered
    {
        public Guid Id { get; set;}
        public string Name { get; set;}
        public int Count { get; set; }
        public string Event { get; set; }
        public Guid LastUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsReadonly { get; set; }
        public bool IsLive { get; set; }
    }
}
