﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactListItem
    {
        public Guid Id { get; set;}
        public string Name { get; set;}
    }
}
