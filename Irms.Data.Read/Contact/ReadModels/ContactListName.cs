﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactListName
    {
        public string ListName { get; set; }
        public bool IsLive { get; set; }
    }
}
