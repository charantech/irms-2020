﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class GuestContactItemFiltered
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid ContactListId { get; set; }
        public int Count { get; set; }
        public string Status { get; set; }
    }
}
