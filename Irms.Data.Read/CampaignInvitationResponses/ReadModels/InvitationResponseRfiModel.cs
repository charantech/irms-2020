﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitationResponses.ReadModels
{
    public class InvitationResponseRfiModel
    {
        // would help a bit with code structure
        public Guid Id { get; set; }
        public Guid CampaignInvitationResponseId { get; set; }
        public MediaType MediaType { get; set; }
        public InvitationType RfiType { get; set; }

        // statics for each invitation responses
        public string Welcome { get; set; }

        public string Submit { get; set; }

        public string Thanks { get; set; }

        public string FormTheme { get; set; }

        public string FormSettings { get; set; }

        public string BackgroundImageUrl { get; set; }

        public IEnumerable<InvitationResponseRfiQuestionModel> Questions { get; set; }

        // Error
        public string Error { get; set; }


        // Question model
        public class InvitationResponseRfiQuestionModel
        {
            public Guid QuestionId { get; set; }

            public string Question { get; set; }
            public string Response { get; set; }
        }
    }
}
