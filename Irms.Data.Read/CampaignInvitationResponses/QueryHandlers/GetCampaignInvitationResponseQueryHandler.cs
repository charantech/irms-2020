﻿using AutoMapper;
using Irms.Application.Abstract.Services;
using Irms.Data.Read.CampaignInvitationResponses.Queries;
using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitationResponses.QueryHandlers
{
    public class GetCampaignInvitationResponseQueryHandler : IRequestHandler<GetCampaignInvitationResponseQuery, InvitationResponseReadModel>
    {
        private readonly IrmsDataContext _context;
        private readonly ITemplateTagProvider _tagProvider;
        private readonly IConfiguration _config;

        public GetCampaignInvitationResponseQueryHandler(
            IrmsDataContext context,
            ITemplateTagProvider tagProvider,
            IConfiguration config
        )
        {
            _context = context;
            _tagProvider = tagProvider;
            _config = config;
        }

        public async Task<InvitationResponseReadModel> Handle(GetCampaignInvitationResponseQuery request, CancellationToken token)
        {
            var media = await _context.CampaignInvitationResponseMediaType
                .Include(x => x.CampaignInvitationResponse)
                .ThenInclude(x => x.CampaignInvitation)
                .ThenInclude(x => x.EventCampaign)
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            var existingResponse = await _context.CampaignInvitationResponse
                .Include(x => x.CampaignInvitation)
                .Where(x => x.CampaignInvitation.EventCampaignId == media.CampaignInvitationResponse.CampaignInvitation.EventCampaignId)
                .FirstOrDefaultAsync(x => x.Answer.HasValue && media.CampaignInvitationResponse.ContactId == x.ContactId, token);

            if (media == null)
            {
                return InvitationFail("Link is not found or was removed by tenant.");
            }

            var responseMediaType = media.CampaignInvitationResponse.Answer;
            if (responseMediaType.HasValue && responseMediaType != 0 || existingResponse != null)
            {
                return InvitationFail("You have already answered to this invitation");
            }

            //if campaign entry criteria set to 'Contact has already submitted list analysis form'
            if (media.CampaignInvitationResponse.CampaignInvitation.EventCampaign.EntryCriteriaTypeId == 2 && media.AcceptOrReject != ResponseMediaType.REJECTED)
            {
                //check if list analysis pending...
                var contactId = media.CampaignInvitationResponse.ContactId;
                var listId = media.CampaignInvitationResponse.CampaignInvitation.EventCampaign.GuestListId;

                var pendingRfi = await _context.CampaignInvitationResponseMediaType
                    .FirstOrDefaultAsync(x => x.AcceptOrReject == media.AcceptOrReject
                    && x.MediaType == media.MediaType
                    && x.CampaignInvitationResponse.ContactId == contactId
                    && x.CampaignInvitationResponse.CampaignInvitation.EventCampaign.GuestListId == listId
                    && x.CampaignInvitationResponse.CampaignInvitation.InvitationTypeId == (int)InvitationType.ListAnalysis
                    && x.CampaignInvitationResponse.Answer == null);

                if (pendingRfi != null)
                {
                    return new InvitationResponseReadModel
                    {
                        PendingRfiId = pendingRfi.Id
                    };
                }
            }

            // Store telemetry
            var telemetry = new EntityClasses.ContactInvitationTelemetry
            {
                MediaType = (int)media.MediaType,
                CampaignInvitationId = media.CampaignInvitationResponse.CampaignInvitationId,
                ContactId = media.CampaignInvitationResponse.ContactId,
                OpenedOn = DateTime.UtcNow,
                TenantId = media.TenantId,
                Id = Guid.NewGuid()
            };
            _context.ContactInvitationTelemetry.Add(telemetry);
            await _context.SaveChangesAsync(token);


            var invitationMetainfo = await PreloadTemplate(media);

            var response = new InvitationResponseReadModel
            {
                CampaignInvitationId = media.CampaignInvitationResponse.CampaignInvitationId,
                CampaignInvitationResponseId = media.CampaignInvitationResponseId,
                MediaType = media.MediaType
            };

            response = FormResponse(media, invitationMetainfo, response);

            // proceed
            var tags = await _tagProvider.LoadTagsForContactAndEvent(media.CampaignInvitationResponse.ContactId,
                media.CampaignInvitationResponse.CampaignInvitation.EventCampaign.EventId,
                media.CampaignInvitationResponse.CampaignInvitationId,
                token);

            response.AcceptedHtml = _tagProvider.ReplaceTagsForTemplate(response.AcceptedHtml ?? "", tags);
            response.RejectedHtml = _tagProvider.ReplaceTagsForTemplate(response.RejectedHtml ?? "", tags);
            response.WelcomeHtml = _tagProvider.ReplaceTagsForTemplate(response.WelcomeHtml ?? "", tags);
            response.RsvpHtml = _tagProvider.ReplaceTagsForTemplate(response.RsvpHtml ?? "", tags);

            return response;
        }

        private async Task<EntityClasses.CampaignInvitation> PreloadTemplate(EntityClasses.CampaignInvitationResponseMediaType campaignInvitationResponseMedia)
        {
            var dataQuery = _context.CampaignInvitation;
            var id = campaignInvitationResponseMedia.CampaignInvitationResponse.CampaignInvitationId;
            switch (campaignInvitationResponseMedia.MediaType)
            {
                case MediaType.Email:
                    return await dataQuery.Include(x => x.CampaignEmailTemplate)
                        .FirstOrDefaultAsync(x => x.Id == id);
                case MediaType.Sms:
                    return await dataQuery.Include(x => x.CampaignSmsTemplate)
                        .FirstOrDefaultAsync(x => x.Id == id);
                case MediaType.WhatsApp:
                    return await dataQuery.Include(x => x.CampaignWhatsappTemplate)
                        .FirstOrDefaultAsync(x => x.Id == id);
                default:
                    throw new ArgumentOutOfRangeException("Media type is wrong. ");
            }
        }

        private InvitationResponseReadModel FormResponse(EntityClasses.CampaignInvitationResponseMediaType media, EntityClasses.CampaignInvitation invitationMetainfo, InvitationResponseReadModel response)
        {
            switch (media.MediaType)
            {
                case MediaType.Email:
                    var emailTemplate = invitationMetainfo.CampaignEmailTemplate.First();
                    if (media.AcceptOrReject == ResponseMediaType.ACCEPTED)
                    {
                        response.AcceptedHtml = emailTemplate.AcceptHtml;
                    }
                    else if (media.AcceptOrReject == ResponseMediaType.REJECTED)
                    {
                        response.RejectedHtml = emailTemplate.RejectHtml;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("Something is wrong with accepted/rejected email response type. Check generation issues");
                    }

                    response.AcceptedOrRejected = media.AcceptOrReject;

                    response.BackgroundImageUrl = $"{_config["AzureStorage:BaseUrl"]}{emailTemplate.BackgroundImagePath}";


                    return response;
                case MediaType.Sms:
                    var smsTemplate = invitationMetainfo.CampaignSmsTemplate.First();
                    response.WelcomeHtml = smsTemplate.WelcomeHtml;
                    response.RsvpHtml = smsTemplate.Rsvphtml;
                    response.AcceptedHtml = smsTemplate.AcceptHtml;
                    response.RejectedHtml = smsTemplate.RejectHtml;
                    response.AcceptButtonText = smsTemplate.AcceptButtonText;
                    response.RejectButtonText = smsTemplate.RejectButtonText;
                    response.ProceedButtonText = smsTemplate.ProceedButtonText;
                    response.ThemeObj = smsTemplate.ThemeJson;

                    response.BackgroundImageUrl = $"{_config["AzureStorage:BaseUrl"]}{smsTemplate.BackgroundImagePath}";

                    return response;
                case MediaType.WhatsApp:
                    var whatsappTemplate = invitationMetainfo.CampaignWhatsappTemplate.First();
                    response.WelcomeHtml = whatsappTemplate.WelcomeHtml;
                    response.RsvpHtml = whatsappTemplate.Rsvphtml;
                    response.AcceptedHtml = whatsappTemplate.AcceptHtml;
                    response.RejectedHtml = whatsappTemplate.RejectHtml;
                    response.AcceptButtonText = whatsappTemplate.AcceptButtonText;
                    response.RejectButtonText = whatsappTemplate.RejectButtonText;
                    response.ProceedButtonText = whatsappTemplate.ProceedButtonText;
                    response.ThemeObj = whatsappTemplate.ThemeJson;

                    response.BackgroundImageUrl = whatsappTemplate.BackgroundImagePath != null ?
                    $"{_config["AzureStorage:BaseUrl"]}{whatsappTemplate.BackgroundImagePath}" :
                    null;

                    return response;
                default:
                    throw new ArgumentOutOfRangeException("Media type is wrong. ");
            }
        }

        private InvitationResponseReadModel InvitationFail(string message)
        {
            return new InvitationResponseReadModel { Error = message };
        }
    }
}
