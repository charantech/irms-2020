﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Campaign.Queries
{
    public class GetCampaignNameQuery : IRequest<string>
    {
        public Guid Id { get; set; }
    }
}
