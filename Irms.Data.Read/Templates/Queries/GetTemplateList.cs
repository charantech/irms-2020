﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Domain.Entities.Templates;

namespace Irms.Data.Read.Templates.Queries
{
    /// <summary>
    /// Request object withc include filters based on which handler return list of templates
    /// </summary>
    public class GetTemplateList : IPageQuery<TemplateListItem>
    {
        public GetTemplateList(
            TemplateType templateType,
            TemplateCompatibility templateCompatibility,
            int pageNo,
            int pageSize,
            string searchText)
        {
            TemplateType = templateType;
            TemplateCompatibility = templateCompatibility;
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public TemplateType TemplateType { get; }
        public TemplateCompatibility TemplateCompatibility { get; }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
    }
}
