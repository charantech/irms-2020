﻿using System;
using Irms.Data.Read.Templates.ReadModels;
using MediatR;

namespace Irms.Data.Read.Templates.Queries
{
    /// <summary>
    /// Request object based on which handler returns template deatials by template Id
    /// </summary>
    public class GetTemplateDetails : IRequest<TemplateDetails>
    {
        public GetTemplateDetails(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
