﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Templates.ReadModels;

namespace Irms.Data.Read.Templates.Queries
{
    public class GetTemplateSelectionList : IPageQuery<TemplateSelectionListItem>
    {
        public GetTemplateSelectionList(int pageNo, int pageSize, string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
    }
}
