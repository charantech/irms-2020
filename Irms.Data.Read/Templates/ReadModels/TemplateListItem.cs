﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Templates.ReadModels
{
    public class TemplateListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool EmailCompatible { get; set; }
        public bool SmsCompatible { get; set; }
    }
}
