﻿using System;
using System.Collections.Generic;
using Irms.Domain.Entities.Templates;

namespace Irms.Data.Read.Templates.ReadModels
{
    public class TemplateDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public TemplateType Type { get; set; }

        public bool EmailCompatible { get; set; }
        public bool SmsCompatible { get; set; }

        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }

        public string SmsText { get; set; }

        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public IEnumerable<TemplateTranslation> Translations { get; set; }
    }
}
