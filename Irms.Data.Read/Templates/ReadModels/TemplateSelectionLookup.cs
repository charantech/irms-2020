﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Templates.ReadModels
{
    public class TemplateSelectionLookup
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
    }
}
