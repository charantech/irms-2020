﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Irms.Data.Read.Registration
{
    public static class FullTextHelpers
    {
        public static string MakeQuery(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return searchText;
            }

            searchText = searchText.Trim();

            //partial match
            if (!searchText.Contains(" "))
            {
                return $"\"{searchText}*\"";
            }

            //exact match + multiple words
            var words = searchText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(x => $"\"{x}*\"");
            return string.Join(" AND ", words);
        }

        public static string MakeLike(string searchText)
        {
            return searchText == null ? null : $"%{searchText.Replace("[", "[[]").Replace("%", "[%]")}%";
        }

        public static string IntToEnumStringList<T>(string list, char separator = ' ')
            where T : struct, IConvertible
        {
            if (string.IsNullOrEmpty(list))
            {
                return list;
            }

            var values = list.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries).Distinct().Select(x =>
            {
                if (int.TryParse(x, out var intValue))
                {
                    if (Enum.IsDefined(typeof(T), intValue))
                    {
                        return (T)(object)intValue;
                    }

                    return default;
                }

                return default;
            });

            return string.Join(", ", values);
        }

        public static string MakeCreatedByClause(string column)
        {
            return column == null ? null : $"AND {column} = @createdBy";
        }
    }
}
