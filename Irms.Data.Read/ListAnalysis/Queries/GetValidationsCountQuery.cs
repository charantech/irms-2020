﻿using Irms.Data.Read.ListAnalysis.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.ListAnalysis.Queries
{
    public class GetValidationsCountQuery : IRequest<ValidationsCount>
    {
        public GetValidationsCountQuery(Guid listId, IEnumerable<string> reservedFields, IEnumerable<Guid> customFields)
        {
            ListId = listId;
            ReservedFields = reservedFields;
            CustomFields = customFields;
        }

        public Guid ListId { get; }
        public IEnumerable<string> ReservedFields { get; }
        public IEnumerable<Guid> CustomFields { get; }
    }
}
