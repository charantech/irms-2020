﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using AutoMapper;
using System;
using Irms.Data.Read.ListAnalysis.Queries;
using Irms.Data.Read.ListAnalysis.ReadModels;
using Irms.Domain.Entities;

namespace Irms.Data.Read.ListAnalysis.QueryHandlers
{
    public class GetListAnalysisImportantFieldsHandler : IRequestHandler<GetListAnalysisImportantFieldsQuery, ListAnalysisImportantFields>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetListAnalysisImportantFieldsHandler(IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        /// <summary>
        /// get important fields
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ListAnalysisImportantFields> Handle(GetListAnalysisImportantFieldsQuery request, CancellationToken token)
        {
            var impFields = await _context.ListAnalysisImportantFields
                .Where(x => x.TenantId == _tenant.Id && x.GuestListId == request.ListId)
                .ToListAsync(token);

            return new ListAnalysisImportantFields
            {
                ReservedFields = impFields.Where(x => x.FieldType == (int)FieldType.Reserved).Select(x => new Field(x.Field, x.IsRequired)),
                CustomsFields = impFields.Where(x => x.FieldType == (int)FieldType.Custom).Select(x => new Field(x.Field, x.IsRequired))
            };
        }
    }
}