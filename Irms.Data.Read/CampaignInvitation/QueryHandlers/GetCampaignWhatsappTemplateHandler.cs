﻿using AutoMapper;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignWhatsappTemplateHandler : IRequestHandler<GetCampaignWhatsappTemplateQuery, CampaignWhatsappTemplate>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public GetCampaignWhatsappTemplateHandler(IrmsDataContext context,
            IMapper mapper,
            IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        public async Task<CampaignWhatsappTemplate> Handle(GetCampaignWhatsappTemplateQuery request, CancellationToken token)
        {
            var tem = await _context.CampaignWhatsappTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == request.CampaignInvitationId, token);
            if (tem == null)
            {
                return null;
            }

            var template = _mapper.Map<ReadModels.CampaignWhatsappTemplate>(tem);
            template.CopyTemplate = await _context.CampaignSmstemplate
                .AnyAsync(x => x.CampaignInvitationId == request.CampaignInvitationId, token);

            template.ListName = await _context.CampaignInvitation
                .Where(x => x.Id == request.CampaignInvitationId)
                .Select(x => x.EventCampaign.GuestList.Name)
                .FirstOrDefaultAsync(token);

            if (template != null && !string.IsNullOrEmpty(template.BackgroundImagePath))
            {
                template.BackgroundImagePath = $"{_config["AzureStorage:BaseUrl"]}{template.BackgroundImagePath}";
            }

            return template;
        }
    }
}
