﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignWhatsappTemplateQuery : IRequest<CampaignWhatsappTemplate>
    {
        public GetCampaignWhatsappTemplateQuery(Guid id)
        {
            CampaignInvitationId = id;
        }

        public Guid CampaignInvitationId { get; set; }
    }
}
