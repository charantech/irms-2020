﻿using System;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignEmailTemplate
    {
        public Guid Id { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string Subject { get; set; }
        public string Preheader { get; set; }
        public string SenderEmail { get; set; }
        public string Body { get; set; }
        public string PlainBody { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }
        public string ListName { get; set; }

    }
}
