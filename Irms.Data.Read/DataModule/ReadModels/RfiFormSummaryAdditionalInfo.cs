﻿using Irms.Data.Read.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    /// <summary>
    /// Read model for retrieving rfi form additional information (one item)
    /// </summary>
    public class RfiFormSummaryAdditionalInfo
    {
        public Guid Id { get; set; }
        public string Response { get; set; }
    }
}
