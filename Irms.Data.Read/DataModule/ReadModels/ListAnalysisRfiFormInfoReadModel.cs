﻿using System;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class ListAnalysisRfiFormInfoReadModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string ListName { get; set; }
        public int Responses { get; set; }
        public int UniqueVisits { get; set; }
        public int TotalVisits { get; set; }
    }
}
