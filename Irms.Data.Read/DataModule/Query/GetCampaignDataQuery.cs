﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetCampaignDataQuery : IRequest<IPageResult<CampaignDataReadModel>>
    {
        public GetCampaignDataQuery(Guid eventId)
        {
            EventId = eventId;
        }

        public Guid EventId { get; set; }
    }
}
