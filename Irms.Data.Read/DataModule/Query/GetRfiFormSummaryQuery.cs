﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Command for retrieving information on summary rfi page
    /// </summary>
    public class GetRfiFormSummaryQuery : IRequest<RfiFormSummaryReadModel>
    {
        public GetRfiFormSummaryQuery(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
