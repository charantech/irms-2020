﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetRfiFormInfoQuery : IPageQuery<RfiFormInfoReadModel>
    {
        public GetRfiFormInfoQuery(Guid id,
            int pageNo,
            int pageSize)
        {
            Id = id;
            PageNo = pageNo;
            PageSize = pageSize;
        }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public Guid Id { get; set; }
        public string SearchText { get; set; }
    }
}
