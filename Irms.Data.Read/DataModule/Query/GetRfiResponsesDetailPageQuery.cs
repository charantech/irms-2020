﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetRfiResponsesDetailPageQuery
    {
        public Guid ContactId { get; set; }
        public Guid FormId { get; set; }
    }
}
