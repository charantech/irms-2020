﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Query;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(FetchQuery)]
    public class GetGlobalGuestsCountHandler : IRequestHandler<GetGlobalGuestsCountQuery, int>
    {
        private const string FetchQuery = @"
            SELECT 
            COUNT(*) 
            FROM Contact c
            WHERE c.IsGuest = 1
            AND c.TenantId=@tenantId";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetGlobalGuestsCountHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _tenant = tenant;
            _connectionString = connectionString;
        }

        /// <summary>
        /// Handler for a command to fetch global guests count
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(GetGlobalGuestsCountQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                tenantId = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var data = await connection.ExecuteScalarAndLog<int>(FetchQuery, parameters);
                return data;
            }
        }
    }
}
