﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Infrastructure.Services.ExcelExport;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(ExportQuery)]
    public class GetRfiFormExportDataHandler : IRequestHandler<GetRfiFormExportDataQuery, IEnumerable<RfiFormAnswersExportModel>>
    {
        private const string ExportQuery = @"
			SELECT
			rfr.Id,
			rfr.RfiFormQuestionId AS QuestionId,
			rfr.ContactId,
			c.FullName AS GuestName,
			rfr.ResponseDate AS SubmissionDate,
			rfq.SortOrder AS SortOrder,
			CASE 
				WHEN JSON_VALUE(rfq.Question, '$.type') = 'select' THEN
					(
					SELECT 
						existingChoices.Name
					FROM RfiFormResponse userResponse
					INNER JOIN 
						(
						SELECT 
							JSON_VALUE(AllChoices.Value, '$.label') AS Name,
							JSON_VALUE(AllChoices.Value, '$.value') AS Id
						FROM RfiFormQuestion rfqI
						OUTER APPLY OPENJSON(JSON_QUERY(rfqI.Question, '$.choices')) AllChoices
						WHERE rfqI.Id = rfq.Id
						) existingChoices
						ON existingChoices.Id = userResponse.Answer OR existingChoices.Name = userResponse.Answer
					WHERE userResponse.Id = rfr.Id
					AND (existingChoices.Id = userResponse.Answer OR existingChoices.Name = userResponse.Answer)
					)

				WHEN JSON_VALUE(rfq.Question, '$.type') = 'selectSearch' THEN
					REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
					(
					SELECT
						joinedData.Name
					FROM RfiFormResponse rfrI
					OUTER APPLY OPENJSON(rfr.Answer) UserChoice
					INNER JOIN 
						(
						SELECT 
							JSON_VALUE(AllChoices.Value, '$.label') AS Name,
							JSON_VALUE(AllChoices.Value, '$.value') AS Id
						FROM RfiFormQuestion rfqI
						OUTER APPLY OPENJSON(JSON_QUERY(rfqI.Question, '$.choices')) AllChoices
						WHERE rfqI.Id =rfq.Id
						) AS joinedData 
						ON joinedData.Id=UserChoice.value
					WHERE rfrI.Id = rfr.Id FOR JSON AUTO
					{0}
					
				ELSE
					CASE
						WHEN LEN(rfr.Answer)  > 3 THEN 
							rfr.Answer
						ELSE
							rfr.Answer
					END 
			END AS Answer
			FROM RfiFormResponse rfr
			INNER JOIN RfiFormQuestion rfq ON rfq.Id = rfr.RfiFormQuestionId
			INNER JOIN Contact c ON c.Id = rfr.ContactId
			WHERE rfr.RfiFormId = @formId
			AND rfr.TenantId=@tenantId
			ORDER BY ContactId, rfq.SortOrder";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        private readonly IMediator _mediator;
        public GetRfiFormExportDataHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant,
            IMediator mediator)
        {
            _connectionString = connectionString;
            _tenant = tenant;
            _mediator = mediator;
        }

		/// <summary>
		/// Exporting rfi form query
		/// </summary>
		/// <param name="request"></param>
		/// <param name="token"></param>
		/// <returns></returns>
        public async Task<IEnumerable<RfiFormAnswersExportModel>> Handle(GetRfiFormExportDataQuery request, CancellationToken token)
        {   
			var parameters = new
			{
				formId = request.FormId,
				tenantId = _tenant.Id
			};
			var exportQuery = ExportQuery.Replace("{0}", "), '[{', ''), '\"}]',''), '}]', ''), '\"Name\"', ''), ':\"', ''), '\"}', ''), ',{', ',')");
			using(IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
				var data = await connection.QueryAndLog<RfiFormAnswersExportModel>(exportQuery, parameters);
				return data.Where(x => request.Questions.Contains(x.QuestionId));
            }
		}
    }
}
