﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    /// <summary>
    /// Handles request for rfi form response page, returns page with guest name ad submission date items
    /// </summary>
    [HasSqlQuery(CountQuery, SearchClause)]
    [HasSqlQuery(PageQuery, SearchClause)]
    public class GetRfiFormResponseHandler : IPageHandler<GetRfiFormResponseQuery, RfiFormResponseReadModel>
    {
        private const string CountQuery = @"
            SELECT
            COUNT(DISTINCT c.Id)
            FROM RfiFormResponse rfr
            INNER JOIN Contact c ON c.Id = rfr.ContactId
            WHERE RfiFormId=@formId
            AND rfr.TenantId=@tenantId
            {0}";

        private const string PageQuery = @"
            SELECT
            c.Id,
            c.FullName AS GuestName,
            MAX(rfr.ResponseDate) AS SubmissionDate
            FROM RfiFormResponse rfr
            INNER JOIN Contact c ON c.Id = rfr.ContactId
            WHERE RfiFormId=@formId
            AND rfr.TenantId=@tenantId
            {0}
            GROUP BY c.Id, c.FullName";

        private const string SearchClause = @"
            AND c.FullName like @searchText";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetRfiFormResponseHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _tenant = tenant;
            _connectionString = connectionString;
        }

        public async Task<IPageResult<RfiFormResponseReadModel>> Handle(GetRfiFormResponseQuery request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            var parameters = new
            {
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                formId = request.FormId,
                tenantId = _tenant.Id,
                skip = request.Skip(),
                take = request.Take()                
            };

            string pageQuery = PageQuery;
            string countQuery = CountQuery;

            pageQuery = string.Format(PageQuery, string.Join("\r\n", clauses));
            countQuery = string.Format(CountQuery, string.Join("\r\n", clauses));
            
            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
                var data = await connection.QueryAndLog<RfiFormResponseReadModel>(pageQuery, parameters);
                return new PageResult<RfiFormResponseReadModel>(data, total);
            }
        }
    }
}
