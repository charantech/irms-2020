﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(FetchQuery)]
    public class GetSelectedImportantFieldsHandler : IRequestHandler<GetSelectedImportantFieldsQuery, IEnumerable<SelectedImportantField>>
    {
        private const string FetchQuery = @"SELECT
            Id,
            FieldName
            CustomFieldId
            FROM
            ContactListImportantField
            WHERE TenantId=@tenantId
            AND ContactListId=@listId";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetSelectedImportantFieldsHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        public async Task<IEnumerable<SelectedImportantField>> Handle(GetSelectedImportantFieldsQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                listId = request.ListId,
                tenantId = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var data = await connection.QueryAndLog<SelectedImportantField>(FetchQuery, parameters);
                return data;
            }
        }
    }
}
