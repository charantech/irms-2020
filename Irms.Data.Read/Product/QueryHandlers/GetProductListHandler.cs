﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Product.Queries;
using Irms.Data.Read.Product.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Product.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetProductListHandler : IPageTimeHandler<GetProductListQuery, ProductListItem>
    {
        private const string CountQuery = @"
            SELECT count(*)
            FROM [Product] p
			WHERE [p].[IsDeleted] = 0 
			AND [p].[CreatedOn] >= @date 
                {0}";
        //
        private const string PageQuery = @"
            SELECT [p].[Id]
				  ,[p].[Title]
				  ,[p].[Price] as BasePrice
				  ,[p].[DiscountPercentage]
				  ,[p].[FinalPrice] as Price
				  ,[p].[LicensePeriodId] as LicenseId
				  ,[l].[Title] as LicenseTitle
				  ,[l].[Days] as LicenseDays				  
            FROM [Product] p
            INNER JOIN [LicensePeriod] l ON [l].[Id] = [p].[LicensePeriodId]
            WHERE [p].[IsDeleted] = 0
              AND [p].[CreatedOn] >= @date 
              {0}
            ORDER BY [p].CreatedOn DESC
              OFFSET @skip ROWS
              FETCH NEXT @take ROWS ONLY";


        private const string SearchClause = "AND [p].[Title] like @searchText";

        private readonly IConnectionString _connectionString;
        
        public GetProductListHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IPageResult<ProductListItem>> Handle(GetProductListQuery request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }
            
            var parameters = new
            {
                date = request.MinDate(),
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                skip = request.Skip(),            
                take = request.Take(),
            };


            var conditionStr = string.Join("\r\n", clauses);
            var pageQuery = string.Format(PageQuery, conditionStr);
            var countQuery = string.Format(CountQuery, conditionStr);

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var total = await db.ExecuteScalarAndLog<int>(countQuery, parameters);
                var page = await db.QueryAndLog<ProductListItem>(pageQuery, parameters);

                return new PageResult<ProductListItem>(page, total);
            }
        }
    }
}
