﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Product.ReadModels
{
    public class ProductListItem
    {
        public ProductListItem() { }
        public ProductListItem(Guid id, string title, float basePrice, float discountPercentage, float finalPrice, byte licenseId, string licenseTitle, int? licenseDays)
        {
            Id = id;
            Title = title;
            BasePrice = basePrice;
            DiscountPercentage = discountPercentage;
            Price = finalPrice;
            LicenseId = licenseId;
            LicenseTitle = licenseTitle;
            LicenseDays = licenseDays;
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public float BasePrice { get; set; }
        public float DiscountPercentage { get; set; }
        public float Price { get; set; }
        public byte LicenseId { get; set; }

        public string LicenseTitle { get; set; }
        public int? LicenseDays { get; set; }
    }
}
