﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Tenant.ReadModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Irms.Domain;
using Irms.Application;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    public class GetTenantLogoHandler : IRequestHandler<GetTenantLogo, TenantLogo>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        public GetTenantLogoHandler(TenantBasicInfo tenant,
            IrmsDataContext context,
            IConfiguration config)
        {
            _tenant = tenant;
            _context = context;
            _config = config;
        }

        /// <summary>
        /// get tenant basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TenantLogo> Handle(GetTenantLogo request, CancellationToken token)
        {
            var tenant = await _context.Tenant
                .FirstOrDefaultAsync(x => x.Id == _tenant.Id, token);

            if (tenant == null)
            {
                throw new IncorrectRequestException("Can't find tenant by id.");
            }

            string logo = string.Empty;
            if (tenant.LogoPath.IsNotNullOrEmpty())
            {
                logo = $"{_config["AzureStorage:BaseUrl"]}{tenant.LogoPath}";
            }

            return new TenantLogo(logo);
        }
    }
}
