﻿using Irms.Data.Read.Dictionary.ReadModels;
using System;

namespace Irms.Data.Read.Tenant.ReadModels
{
    public class TenantDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string LogoPath { get; set; }
        public CountryItem Country { get; set; }
        public string City { get; set; }
        public bool IsActive { get; set; }
    }
}
