﻿using System;

namespace Irms.Data.Read.Tenant.ReadModels
{
    public class TenantListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string LogoPath { get; set; }
    }
}
