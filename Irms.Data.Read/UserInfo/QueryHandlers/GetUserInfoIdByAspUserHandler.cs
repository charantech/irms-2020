﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Application.UsersInfo.Queries;
using Irms.Application;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetUserInfoIdByAspUserHandler : IRequestHandler<GetUserInfoIdByAspUser, Guid?>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetUserInfoIdByAspUserHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<Guid?> Handle(GetUserInfoIdByAspUser request, CancellationToken cancellationToken)
        {
            if (!request.UserId.HasValue)
            {
                return null;
            }

            var emp = await _context.UserInfo
                .FirstOrDefaultAsync(
                    x => x.UserId == request.UserId
                         && !x.IsDeleted
                         && x.TenantId == _tenant.Id,
                    cancellationToken);

            return emp?.Id;
        }
    }
}
