﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Application.Abstract;
using Irms.Data.Read.UserInfo.Queries;
using Irms.Data.Read.UserInfo.ReadModels;
using Irms.Domain.Entities;
using Irms.Application;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetUserInfoHandler : IRequestHandler<GetUserInfo, UserInfoReadModel>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;
        private readonly ICurrentUser _currentUser;

        public GetUserInfoHandler(IrmsTenantDataContext context, ICurrentUser currentUser, TenantBasicInfo tenant)
        {
            _context = context;
            _currentUser = currentUser;
            _tenant = tenant;
        }

        public async Task<UserInfoReadModel> Handle(GetUserInfo request, CancellationToken cancellationToken)
        {
            var emp = await _context.UserInfo
                .FirstOrDefaultAsync(
                    x => x.Id == request.Id
                         && !x.IsDeleted
                         && x.TenantId == _tenant.Id
                         && x.UserId != _currentUser.Id,
                    cancellationToken);

            return new UserInfoReadModel(
                emp.Id,
                (RoleType)emp.RoleId,
                emp.FullName,
                (Gender)emp.GenderId,
                emp.BirthDate.Value,
                emp.MobileNo,
                emp.Email,
                emp.PassportNo,
                emp.NationalId,
                emp.IsActive);
        }
    }
}
