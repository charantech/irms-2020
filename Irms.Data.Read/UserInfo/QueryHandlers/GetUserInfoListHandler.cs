﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.UserInfo.Queries;
using Irms.Data.Read.UserInfo.ReadModels;
using Irms.Domain.Entities;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetUserInfoListHandler : IPageHandler<GetUserInfoList, UserBasicInfo>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;
        private readonly ICurrentUser _currentUser;

        public GetUserInfoListHandler(IrmsTenantDataContext context, ICurrentUser currentUser, TenantBasicInfo tenant)
        {
            _context = context;
            _currentUser = currentUser;
            _tenant = tenant;
        }

        public async Task<IPageResult<UserBasicInfo>> Handle(
            GetUserInfoList request,
            CancellationToken cancellationToken)
        {
            var q = _context.UserInfo
                .Where(x => !x.IsDeleted
                            && x.TenantId == _tenant.Id
                            && x.RoleId != (int)RoleType.SuperAdmin
                            && x.UserId != _currentUser.Id);

            if (request.SearchText.IsNotNullOrEmpty())
            {
                q = q.Where(x => x.FullName.Contains(request.SearchText));
            }

            var count = await q.CountAsync(cancellationToken);
            var page = await q.Select(x => new UserBasicInfo(
                    x.Id,
                    x.FullName,
                    x.MobileNo,
                    x.NationalId,
                    (RoleType)x.RoleId,
                    x.IsActive))
                .Skip(request.Skip())
                .Take(request.Take())
                .ToListAsync(cancellationToken);

            return new PageResult<UserBasicInfo>(page, count);
        }
    }
}
