﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CustomField.ReadModels
{
    public class CustomFieldReadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int CustomFieldType { get; set; }
        public long? MinValue { get; set; }
        public long? MaxValue { get; set; }
    }
}
