﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Read.CustomField.Queries;
using Irms.Data.Read.CustomField.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CustomField.QueryHandler
{
    public class GetCustomFieldsQueryHandler : IRequestHandler<GetCustomFieldsQuery, IEnumerable<CustomFieldReadModel>>
    {
        private readonly IMapper _mapper;
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;
        public GetCustomFieldsQueryHandler(
            IMapper mapper,
            IrmsDataContext context,
            TenantBasicInfo tenant
        )
        {
            _mapper = mapper;
            _context = context;
            _tenant = tenant;
        }

        public async Task<IEnumerable<CustomFieldReadModel>> Handle(GetCustomFieldsQuery request, CancellationToken token)
        {
            var data = _context.CustomField
                .Where(x => x.TenantId == _tenant.Id);

            if (request.Fields != null && request.Fields.Count() > 0)
            {
                data = data.Where(x => request.Fields.Contains(x.Id));
            }

            var list = await data.ToListAsync(token);
            var result = _mapper.Map<IEnumerable<CustomFieldReadModel>>(list);
            return result;
        }
    }
}
