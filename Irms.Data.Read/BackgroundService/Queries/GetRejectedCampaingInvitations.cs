﻿using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.BackgroundService.Queries
{
    public class GetRejectedCampaingInvitations : IRequest<IEnumerable<ReadModels.CampaingInvitationListItem>>
    {
    }
}
