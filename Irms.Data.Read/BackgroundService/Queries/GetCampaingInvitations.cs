﻿using Irms.Domain.Entities;
using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.BackgroundService.Queries
{
    public class GetRsvpPendingCampaingInvitations : IRequest<IEnumerable<ReadModels.CampaingInvitationListItem>>
    {
        public GetRsvpPendingCampaingInvitations(InvitationType invitationType)
        {
            InvitationType = invitationType;
        }

        public InvitationType InvitationType { get; }
    }
}
