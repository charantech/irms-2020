﻿using Irms.Application.Abstract;
using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetCurrencyHandler : IRequestHandler<GetCurrency, IEnumerable<CurrencyItem>>
    {
        private const string Query = @"
        SELECT [Id]
              ,[Title]
        FROM [dbo].[Currency]
        ORDER BY ID";

        private readonly IConnectionString _connectionString;
        private readonly ICurrentLanguage _currentLanguage;

        public GetCurrencyHandler(IConnectionString connectionString, ICurrentLanguage currentLanguage)
        {
            _connectionString = connectionString;
            _currentLanguage = currentLanguage;
        }
        public async Task<IEnumerable<CurrencyItem>> Handle(GetCurrency request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<CurrencyItem>(Query);
                return list;
            }
        }
    }
}
