﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Application;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetAllLanguagesHandler : IRequestHandler<GetAllLanguages, IEnumerable<Language>>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly TenantBasicInfo _tenant;
        public GetAllLanguagesHandler(IrmsDataContext dataContext, TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _tenant = tenant;
        }

        public async Task<IEnumerable<Language>> Handle(GetAllLanguages request, CancellationToken cancellationToken)
        {
            return await _dataContext.Language
                .Where(x => x.TenantId == _tenant.Id)
                .Select(x => new Language(x.Id, x.Name, x.Culture))                
                .ToListAsync(cancellationToken);
        }
    }
}
