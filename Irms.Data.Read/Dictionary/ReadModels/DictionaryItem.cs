﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class DictionaryItem
    {
        public DictionaryItem(Guid id, string value)
        {
            Id = id;
            Value = value;
        }

        public Guid Id { get; }
        public string Value { get; }
    }
}
