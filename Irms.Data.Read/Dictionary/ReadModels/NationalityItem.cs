﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class NationalityItem
    {
        public NationalityItem(Guid id, string nationality)
        {
            Id = id;
            Nationality = nationality;
        }

        public Guid Id { get; }
        public string Nationality { get; }
    }
}
