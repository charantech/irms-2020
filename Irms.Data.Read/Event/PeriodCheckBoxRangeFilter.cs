﻿namespace Irms.Data.Read.Event
{
    public enum PeriodCheckBoxRangeFilter
    {
        Upcoming = 0,
        Live = 1,
        Past = 2
    }
}
