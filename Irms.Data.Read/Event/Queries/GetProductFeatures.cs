﻿using Irms.Data.Read.Event.ReadModels;
using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.Event.Queries
{
    public class GetProductFeatures : IRequest<IEnumerable<ProductFeatureListItem>>
    {
    }
}
