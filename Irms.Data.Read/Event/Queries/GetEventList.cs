﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Event.ReadModels;
using System.Collections.Generic;

namespace Irms.Data.Read.Event.Queries
{
    public class GetEventList : IPageQuery<EventListItem>
    {
        public GetEventList(
         List<PeriodCheckBoxRangeFilter> filter,
         int pageNo,
         int pageSize,
         string searchText)
        {
            Filters = filter;
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public List<PeriodCheckBoxRangeFilter> Filters { get; }
        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
    }
}
