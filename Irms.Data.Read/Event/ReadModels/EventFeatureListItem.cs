﻿using System;

namespace Irms.Data.Read.Event.ReadModels
{
    public class EventFeatureListItem
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsPurchased { get; set; }
        public bool IsChecked { get; set; }
    }
}
