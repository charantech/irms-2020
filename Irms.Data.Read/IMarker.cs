﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Irms.Tests.Unit")]
[assembly: InternalsVisibleTo("Irms.Tests.Integration")]
namespace Irms.Data.Read
{
    public interface IMarker
    {
    }
}
