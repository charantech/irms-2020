﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    public class GetCustomFieldHandler : IRequestHandler<GetCustomFieldsQuery, CustomFieldReadModel>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;
        public GetCustomFieldHandler(
            IrmsDataContext context,
            TenantBasicInfo tenant
            )
        {
            _tenant = tenant;
            _context = context;
        }
        public async Task<CustomFieldReadModel> Handle(GetCustomFieldsQuery request, CancellationToken token)
        {
            var result = new List<CustomFieldEntryModel>();

            var allCustomFields = await _context.CustomField
                .Where(x => x.TenantId == _tenant.Id)
                .ToListAsync(token);

            var userAnswers = await _context.ContactCustomField
                .Where(x => x.ContactId == request.Id)
                .ToListAsync(token);

            foreach(var customField in allCustomFields)
            {
                var readModel = new CustomFieldEntryModel
                {
                    Id = customField.Id,
                    MinValue = customField.MinValue,
                    MaxValue = customField.MaxValue,
                    CustomFieldType = customField.CustomFieldType,
                    Title = customField.FieldName
                };

                var answer = userAnswers.FirstOrDefault(x => x.CustomFieldId == customField.Id);
                if(answer != null)
                {
                    readModel.Value = answer.Value;
                }

                result.Add(readModel);
            }

            return new CustomFieldReadModel(result);
        }
    }
}
