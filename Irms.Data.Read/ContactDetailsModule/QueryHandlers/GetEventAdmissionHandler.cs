﻿using Dapper;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(EventAdmissionQuery)]
    public class GetEventAdmissionHandler : IRequestHandler<GetEventAdmissionQuery, EventAdmissionReadModel>
    {
        private const string EventAdmissionQuery = @"
			WITH STATUS AS (
				SELECT 
					MAX(COALESCE(cir.Answer, 0) )AS Answer, 
					cl.Id AS contactListId, 
					e.id AS eventId,
					c.id AS contactId
				FROM ContactList cl 
					INNER JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
					INNER JOIN dbo.Event AS e ON ec.EventId = e.Id
					INNER JOIN dbo.ContactListToContacts cltc ON cltc.ContactListId = cl.Id
					INNER JOIN dbo.Contact AS c ON cltc.ContactId = c.Id
					INNER JOIN dbo.CampaignInvitation AS ci ON ci.EventCampaignId = ec.Id AND ci.InvitationTypeId IN (0,1)
					LEFT JOIN dbo.CampaignInvitationResponse AS cir ON 
					cir.CampaignInvitationId = ci.Id AND c.Id = cir.ContactId
				WHERE cl.Id = @listId
				AND ec.Id = (SELECT TOP 1 ec.Id FROM EventCampaign ec
									WHERE ec.GuestListId= @listId
									AND ec.Active = 1 
									ORDER BY ec.CreatedOn DESC)
				GROUP BY cl.id, e.id, c.id
			)
			SELECT 
					CASE
						WHEN st.Answer = 0 OR st.Answer = 3 THEN 'Pending'
						WHEN st.Answer = 1 THEN 'Accepted'
						WHEN st.Answer = 2 THEN 'Rejected'
						WHEN st.Answer IS NULL THEN ''
					END AS Status,
					'S25' AS Seat,
					'P4-24' AS Parking,
					'https://picsum.photos/200/300' AS BadgeUrl
			FROM dbo.Contact as c
					INNER JOIN dbo.ContactListToContacts AS cltc ON cltc.ContactId = c.Id
					INNER JOIN dbo.ContactList AS cl ON cl.Id=cltc.ContactListId 
					LEFT JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
					LEFT JOIN dbo.Event AS e ON ec.EventId = e.Id
					LEFT JOIN STATUS AS st ON st.eventId = e.Id AND st.contactListId = cl.Id AND st.contactId = c.Id
			WHERE cltc.ContactListId = @listId AND c.Id=@contactId";

        private readonly IConnectionString _connectionString;
        public GetEventAdmissionHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<EventAdmissionReadModel> Handle(GetEventAdmissionQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
               listId = request.ListId,
               contactId = request.ContactId
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var details = await connection.QueryFirstOrDefaultAsync<EventAdmissionReadModel>(EventAdmissionQuery, parameters);
                return details;
            }
        }
    }
}
