﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Queries
{
    public class GetPersonalInfoQuery : IRequest<PersonalInfoReadModel>
    {
        public GetPersonalInfoQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
