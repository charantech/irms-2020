﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class ContactInvitationTelemetryMapper : AutoMapper.Profile
    {
        public ContactInvitationTelemetryMapper()
        {
            CreateMap<EntityClasses.ContactInvitationTelemetry, ContactInvitationTelemetry>()
                .ReverseMap();
        }
    }
}
