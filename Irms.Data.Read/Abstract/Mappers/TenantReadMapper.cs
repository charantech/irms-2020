﻿using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Data.Read.Tenant.ReadModels;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class TenantReadMapper : AutoMapper.Profile
    {
        public TenantReadMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.Tenant, TenantDetails>();
            CreateMap<EntityClasses.TenantContactInfo, TenantContactInfo>();
            CreateMap<EntityClasses.UserInfo, TenantConfigInfo>();
            CreateMap<EntityClasses.Tenant, TenantConfigInfo>();
        }
    }
}
