﻿using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class CampaignInvitationResponseReadMapper : AutoMapper.Profile
    {
        public CampaignInvitationResponseReadMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.CampaignInvitationResponse, CampaignInvitationResponse>();
            CreateMap<CampaignInvitationResponse, EntityClasses.CampaignInvitationResponse>()
                .ForMember(f => f.Answer, m => m.MapFrom(o => (int?)o.Answer))
                .ForMember(f => f.ResponseMediaType, m=>m.MapFrom(o => (int?)o.ResponseMediaType));

            //also for all the media types
            CreateMap<EntityClasses.CampaignInvitationResponseMediaType, CampaignInvitationResponseMediaType>();
            CreateMap<CampaignInvitationResponseMediaType, EntityClasses.CampaignInvitationResponseMediaType>();

            //mapper for read model
            CreateMap<EntityClasses.CampaignEmailTemplate, InvitationResponseReadModel>()
                .ForMember(f => f.AcceptedHtml, m => m.MapFrom(o => o.AcceptHtml))
                .ForMember(f => f.RejectedHtml, m => m.MapFrom(o => o.RejectHtml))
                .ForMember(f => f.WelcomeHtml, m => m.Ignore())
                .ForMember(f => f.RsvpHtml, m => m.Ignore())
                .ForMember(f => f.CampaignInvitationResponseId, m => m.Ignore());

            CreateMap<EntityClasses.CampaignSmstemplate, InvitationResponseReadModel>()
                .ForMember(f => f.AcceptedHtml, m => m.MapFrom(o => o.AcceptHtml))
                .ForMember(f => f.RejectedHtml, m => m.MapFrom(o => o.RejectHtml))
                .ForMember(f => f.RsvpHtml, m => m.MapFrom(o => o.Rsvphtml))
                .ForMember(f => f.WelcomeHtml, m => m.MapFrom(o => o.WelcomeHtml))
                .ForMember(f => f.CampaignInvitationResponseId, m => m.Ignore());
        }
    }
}
