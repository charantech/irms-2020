﻿using Irms.Data.Read.Campaign.ReadModels;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class CampaignReadMapper : AutoMapper.Profile
    {
        public CampaignReadMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.EventCampaign, CampaignInfo>();
        }
    }
}
