﻿using System.Collections.Generic;

namespace Irms.Data.Read.Abstract
{
    public interface IPageResult<out T>
    {
        IEnumerable<T> Items { get; }
        int TotalCount { get; }
    }
}
