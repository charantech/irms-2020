﻿namespace Irms.Data.Read
{
    public enum PeriodFilterRange
    {
        Upcoming = 0,
        Current = 1,
        Past = 2,
        All = 3,
        UpcomingAndCurrent = 4
    }
}
