﻿using System;
using System.Collections.Generic;

namespace Irms.Data.Read.RfiForm.ReadModels
{
    public class RfiForm
    {
        public Guid Id { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string WelcomeHtml { get; set; }
        public string SubmitHtml { get; set; }
        public string ThanksHtml { get; set; }
        public string FormTheme { get; set; }
        public string FormSettings { get; set; }
        public string ThemeBackgroundImagePath { get; set; }
        public List<RfiFormQuestion> RfiFormQuestions { get; set; }
    }
}
