﻿namespace Irms.Infrastructure.Services
{
    public sealed class MalathConfiguration
    {
        public MalathConfiguration(string username, string password, string senderName)
        {
            Username = username;
            Password = password;
            SenderName = senderName;
        }

        public string Username { get; }
        public string Password { get; }
        public string SenderName { get; }

        public bool IsValid => !string.IsNullOrWhiteSpace(Username)
                               && !string.IsNullOrWhiteSpace(Password)
                               && !string.IsNullOrWhiteSpace(SenderName);
    }
}