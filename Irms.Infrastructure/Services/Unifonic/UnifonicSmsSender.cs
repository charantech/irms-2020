﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text.RegularExpressions;
//using System.Threading;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
//using Irms.Application.Abstract.Services.Notifications;
//using System.Net.Http;
//using Newtonsoft.Json;
//using Irms.Application.Events.Commands;
//using Irms.Domain.Entities;
//using Irms.Application;
//using MediatR;

//namespace Irms.Infrastructure.Services.Unifonic
//{
//    /// <summary>
//    /// Implementation of SMS sender by Unifonic API
//    /// </summary>
//    public class UnifonicSmsSender : IUnifonicSmsSender
//    {
//        private readonly Regex _notNumber = new Regex("[^0-9]+", RegexOptions.Compiled);
//        private readonly Regex _isUnicode = new Regex("[^\x00-\x7F]+", RegexOptions.Compiled);

//        private readonly IUnifonicConfigurationProvider _configurationProvider;
//        private readonly ILogger<UnifonicSmsSender> _logger;
//        private readonly TenantBasicInfo _tenant;
//        private readonly IMediator _mediator;

//        public UnifonicSmsSender(
//            IUnifonicConfigurationProvider configurationProvider,
//            ILogger<UnifonicSmsSender> logger,
//            TenantBasicInfo tenant,
//            IMediator mediator)
//        {
//            _configurationProvider = configurationProvider;
//            _logger = logger;
//            _tenant = tenant;
//            _mediator = mediator;
//        }

//        public async Task<bool> SendSms(SmsMessage message, CancellationToken token)
//        {
//            var cfg = await _configurationProvider.GetConfiguration(token);
//            if (!cfg.IsValid)
//            {
//                throw new Exception("Unifonic SMS is not configured");
//            }

//            var recipients = message.Recipients.Select(x => _notNumber.Replace(x.Phone, string.Empty));
//            var text = FillPlaceholders(message);

//            var client = new HttpClient();
//            var pairs = new List<KeyValuePair<string, string>>
//                {
//                    new KeyValuePair<string, string>("AppSid", cfg.AccountSid),
//                    new KeyValuePair<string, string>("SenderID", cfg.Sender),
//                    new KeyValuePair<string, string>("Recipient", string.Join(',',recipients)),
//                    new KeyValuePair<string, string>("Body", text)
//                };

//            var content = new FormUrlEncodedContent(pairs);
//            try
//            {
//                var response = await client.PostAsync(cfg.ApiUrl, content, token);
//                var result = await response.Content.ReadAsStringAsync();
//                if (response.IsSuccessStatusCode && result.Contains("MessageID"))
//                {
//                    foreach (var recipient in message.Recipients)
//                    {
//                        _logger.LogInformation("SMS to number {0} has been sent", recipient.Phone, message);
//                    }

//                    var cmd = new ProviderLogsCmd
//                    {
//                        ProviderType = ProviderType.Unifonic,
//                        TenantId = _tenant.Id,
//                        Request = JsonConvert.SerializeObject(message),
//                        RequestDate = DateTime.UtcNow,
//                        Response = JsonConvert.SerializeObject(result),
//                        ResponseDate = DateTime.UtcNow,
//                        CampaignInvitationId = message.CampaignInvitationId
//                    };

//                    await _mediator.Send(cmd, token);
//                    return true;
//                }
//                else
//                {
//                    _logger.LogError("SMS sending failed, Unifonic error: " + result);
//                    return false;
//                }
//            }
//            catch (Exception ex)
//            {
//                _logger.LogError("SMS sending failed, Unifonic error: " + ex.Message);
//                return false;
//            }
//        }

//        public async Task<bool> SendWebhookSms(SmsMessage message, CancellationToken token)
//        {
//            var cfg = await _configurationProvider.GetConfiguration(token);
//            if (!cfg.IsValid)
//            {
//                throw new Exception("Unifonic SMS is not configured");
//            }

//            var recipients = message.Recipients.Select(x => _notNumber.Replace(x.Phone, string.Empty));
//            var text = FillPlaceholders(message);

//            var client = new HttpClient();
//            var pairs = new List<KeyValuePair<string, string>>
//                {
//                    new KeyValuePair<string, string>("AppSid", cfg.AccountSid),
//                    new KeyValuePair<string, string>("Recipient", string.Join(',',recipients)),
//                    new KeyValuePair<string, string>("Body", text)
//                };

//            var content = new FormUrlEncodedContent(pairs);
//            try
//            {
//                var response = await client.PostAsync(cfg.ApiUrl, content, token);
//                var result = await response.Content.ReadAsStringAsync();
//                if (response.IsSuccessStatusCode && result.Contains("MessageID"))
//                {
//                    foreach (var recipient in message.Recipients)
//                    {
//                        _logger.LogInformation("SMS to number {0} has been sent", recipient.Phone, message);
//                    }

//                    var cmd = new ProviderLogsCmd
//                    {
//                        ProviderType = ProviderType.Unifonic,
//                        TenantId = _tenant.Id,
//                        Request = JsonConvert.SerializeObject(message),
//                        RequestDate = DateTime.UtcNow,
//                        Response = JsonConvert.SerializeObject(result),
//                        ResponseDate = DateTime.UtcNow,
//                        CampaignInvitationId = message.CampaignInvitationId
//                    };

//                    await _mediator.Send(cmd, token);
//                    return true;
//                }
//                else
//                {
//                    _logger.LogError("SMS sending failed, Unifonic error: " + response);
//                    return false;
//                }
//            }
//            catch (Exception ex)
//            {
//                _logger.LogError("SMS sending failed, Unifonic error: " + ex.Message);
//                return false;
//            }
//        }

//        private static string FillPlaceholders(SmsMessage message)
//        {
//            var messageText = message.MessageTemplate;
//            foreach (var variable in message.Recipients.First().TemplateVariables)
//            {
//                messageText = messageText.Replace(variable.Name, variable.Value);
//            }

//            return messageText;
//        }
//    }
//}
