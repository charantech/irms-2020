﻿using System;
using Irms.Application.Abstract.Services;
using Irms.Infrastructure.MimeDetective;

namespace Irms.Infrastructure.Services
{
    public class FileTypeValidator : IFileTypeValidator
    {
        public bool CheckFileType(byte[] data, string declaredExtension)
        {
            var type = data.DetectMimeType();
            if (type == null)
            {
                return false;
            }

            //if (type.Equals(MimeTypes.JPEG))
            if (type.Equals(MimeTypes.JPEG)
                   || type.Equals(MimeTypes.PNG)
                   || type.Equals(MimeTypes.GIF)
                   || type.Equals(MimeTypes.BMP))
            {
                //return declaredExtension.Equals("jpg", StringComparison.OrdinalIgnoreCase)
                //       || declaredExtension.Equals("jpeg", StringComparison.OrdinalIgnoreCase);
                return true;
            }

            if (type.Equals(MimeTypes.TIFF))
            {
                //return declaredExtension.Equals("tif", StringComparison.OrdinalIgnoreCase)
                //       || declaredExtension.Equals("tiff", StringComparison.OrdinalIgnoreCase);
                return true;
            }

            return false; // type.Extension.Equals(declaredExtension, StringComparison.OrdinalIgnoreCase);
        }

        public bool IsPicture(byte[] data)
        {
            var type = data.DetectMimeType();

            return type.Equals(MimeTypes.JPEG)
                   || type.Equals(MimeTypes.PNG)
                   || type.Equals(MimeTypes.GIF)
                   || type.Equals(MimeTypes.BMP);
        }

        public bool IsDocument(byte[] data)
        {
            var type = data.DetectMimeType();

            return type.Equals(MimeTypes.WORD)
                   || type.Equals(MimeTypes.EXCEL)
                   || type.Equals(MimeTypes.PPT)
                   || type.Equals(MimeTypes.WORDX)
                   || type.Equals(MimeTypes.EXCELX)
                   || type.Equals(MimeTypes.ODT)
                   || type.Equals(MimeTypes.ODS)
                   || type.Equals(MimeTypes.MSDOC)
                   || type.Equals(MimeTypes.RTF);
        }

        public bool IsPdf(byte[] data)
        {
            var type = data.DetectMimeType();

            return type.Equals(MimeTypes.PDF);
        }
    }
}
