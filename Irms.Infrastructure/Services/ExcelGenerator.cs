﻿using Irms.Application;
using Irms.Application.Abstract.Services;
using Irms.Infrastructure.Services.ExcelExport;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Irms.Infrastructure.Services
{
    public class ExcelGenerator : IExcelGenerator
    {
        private const string HeaderColor = "#70ad47";
        private const string EvenRowColor = "#e2efda";

        /// <summary>
        /// Generates basic report
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exportItems"></param>
        /// <param name="outputStream"></param>
        /// <param name="sheetName"></param>
        /// <param name="fields"></param>
        public void GenerateReport<T>(IEnumerable<T> exportItems, Stream outputStream, string sheetName, HashSet<string> fields = null)
        {
            if (exportItems == null)
            {
                throw new ArgumentNullException(nameof(exportItems));
            }

            using (var package = new ExcelPackage(outputStream))
            {
                var worksheet = package.Workbook.Worksheets.Add(sheetName);

                FillReport(exportItems, worksheet, fields);

                package.Save();
            }
        }



        /// <summary>
        /// Generates report for rfi forms
        /// </summary>
        /// <typeparam name="Q">Question model</typeparam>
        /// <typeparam name="A">Answer model</typeparam>
        /// <param name="inputQuestions">Questions input list</param>
        /// <param name="inputAnswers">Answers input list</param>
        /// <param name="outputStream">Output stream for excel file</param>
        /// <param name="sheetName">Name of the sheet in excel file</param>
        /// <param name="hasGuestNameSelected">Was guest name selected?</param>
        /// <param name="hasSubmissionDateSelected">Was submission date selected?</param>
        public void GenerateRfiExportReport<Q, A>(List<Q> inputQuestions,
            List<A> inputAnswers,
            Stream outputStream,
            string sheetName,
            bool hasGuestNameSelected = true,
            bool hasSubmissionDateSelected = true)
        {
            var questions = inputQuestions as List<RfiFormQuestionsModel>;
            var answers = inputAnswers as List<RfiFormAnswersExportModel>;
            int answersOFfset = 0;
            var groupedAnswers = answers.GroupBy(x => x.ContactId);

            using (var package = new ExcelPackage(outputStream))
            {
                var worksheet = package.Workbook.Worksheets.Add(sheetName);
                var headerColor = ColorTranslator.FromHtml(HeaderColor);
                var rowColor = ColorTranslator.FromHtml(EvenRowColor);

                if (hasGuestNameSelected)
                {
                    var cell = worksheet.Cells[1, answersOFfset + 1];
                    cell.Value = "Guest Name";
                    cell.Style.Font.Bold = true;
                    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(headerColor.R, headerColor.G, headerColor.B));
                    cell.Style.Font.Color.SetColor(Color.White);
                    answersOFfset++;

                    for (int i = 0; i < groupedAnswers.Count(); ++i)
                    {
                        cell = worksheet.Cells[i + 2, answersOFfset];
                        cell.Value = groupedAnswers.ElementAt(i).First().GuestName;
                        cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(rowColor.R, rowColor.G, rowColor.B));
                    }
                }



                if (hasSubmissionDateSelected)
                {
                    var cell = worksheet.Cells[1, answersOFfset + 1];
                    cell.Value = "Submission Date";
                    cell.Style.Font.Bold = true;
                    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(headerColor.R, headerColor.G, headerColor.B));
                    cell.Style.Font.Color.SetColor(Color.White);
                    answersOFfset++;

                    for (int i = 0; i < groupedAnswers.Count(); ++i)
                    {
                        cell = worksheet.Cells[i + 2, answersOFfset];
                        cell.Value = groupedAnswers.ElementAt(i).First().SubmissionDate;
                        cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(rowColor.R, rowColor.G, rowColor.B));
                    }
                }

                // fill offsets
                var keys = answers
                    .GroupBy(x => x.QuestionId)
                    .Select(x => x.Key);

                questions = questions
                    .Where(x => keys.Contains(x.Id))
                    .OrderBy(x => x.SortOrder).ToList();

                var allCells = worksheet.Cells[1, 1, groupedAnswers.Count() + 1, answersOFfset + questions.Count()];
                allCells.AutoFitColumns();
                allCells.Style.Border.Top.Style = allCells.Style.Border.Right.Style = allCells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                // fill headers
                for (var i = answersOFfset; i < questions.Count + answersOFfset; ++i)
                {
                    var cell = worksheet.Cells[1, i + 1];
                    cell.Value = questions[i - answersOFfset].Question;
                    cell.Style.Font.Bold = true;
                    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(headerColor.R, headerColor.G, headerColor.B));
                    cell.Style.Font.Color.SetColor(Color.White);

                    cell.Style.Border.Top.Style = cell.Style.Border.Left.Style =
                        cell.Style.Border.Right.Style = cell.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }

                // fill answers
                int offsetFromTop = 2;


                foreach (var answer in groupedAnswers)
                {
                    for (var i = 0; i < answer.Count(); ++i)
                    {
                        var cell = worksheet.Cells[offsetFromTop, i + answersOFfset + 1];
                        cell.Value = answer.ElementAt(i).Answer;
                        cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(rowColor.R, rowColor.G, rowColor.B));
                    }

                    ++offsetFromTop;
                }

                package.Save();
            }
        }

        /// <summary>
        /// Generates report to export users
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="outputStream"></param>
        /// <param name="sheetName"></param>
        /// <param name="fields"></param>
        public void GenerateContactsReport<T>(T model, Stream outputStream, string sheetName, HashSet<string> fields = null)
        {
            var exportModel = model as ContactsExportModel;
            if (exportModel.Contacts == null)
            {
                throw new ArgumentNullException(nameof(exportModel.Contacts));
            }

            var headerColor = ColorTranslator.FromHtml(HeaderColor);
            var rowColor = ColorTranslator.FromHtml(EvenRowColor);

            using (var package = new ExcelPackage(outputStream))
            {
                var worksheet = package.Workbook.Worksheets.Add(sheetName);
                int customFieldsOffset = 1;
                // Fill basic report
                if (fields.Count > 0)
                {
                    customFieldsOffset = FillReport(exportModel.Contacts, worksheet, fields);
                    customFieldsOffset++;
                }
                else if (fields.Count == 0 && exportModel.CustomFields.Count() == 0)
                {
                    customFieldsOffset = FillReport(exportModel.Contacts, worksheet, fields);
                    customFieldsOffset++;
                }
                else if (fields.Count == 0 && exportModel.CustomFieldsCount == 0)
                {
                    customFieldsOffset = FillReport(exportModel.Contacts, worksheet, fields);
                    customFieldsOffset++;
                }

                if (exportModel.CustomFields.Count() > 0)
                {
                    int customFieldsOffsetHeader = customFieldsOffset;
                    // Fill custom field headers
                    foreach (var customField in exportModel.CustomFields)
                    {
                        var cell = worksheet.Cells[1, customFieldsOffsetHeader++];
                        cell.Value = customField.Name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(headerColor.R, headerColor.G, headerColor.B));
                        cell.Style.Font.Color.SetColor(Color.White);

                        cell.Style.Border.Top.Style = cell.Style.Border.Left.Style =
                            cell.Style.Border.Right.Style = cell.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    // Fill contact custom field values
                    var forAutoFit = worksheet.Cells[1, customFieldsOffset, 1 + exportModel.Contacts.Count(), customFieldsOffset + exportModel.CustomFields.Count()];
                    forAutoFit.AutoFitColumns();

                    var allCells = worksheet.Cells[2, customFieldsOffset, 1 + exportModel.Contacts.Count(), customFieldsOffset + exportModel.CustomFields.Count() - 1];
                    allCells.Style.Border.Top.Style = allCells.Style.Border.Right.Style = allCells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    var contactOffset = 2;

                    foreach (var modelItem in exportModel.Contacts)
                    {
                        var contact = modelItem as GuestExportModel;
                        int rowCustomFieldsOffset = customFieldsOffset;

                        foreach (var customField in exportModel.CustomFields)
                        {

                            var contactCustomField = exportModel.ContactCustomFields
                                .FirstOrDefault(x => x.ContactId == contact.Id
                                && x.CustomFieldId == customField.Id);

                            var cell = worksheet.Cells[contactOffset, rowCustomFieldsOffset];
                            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;

                            if (contactOffset % 2 == 0)
                                cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(rowColor.R, rowColor.G, rowColor.B));
                            else
                                cell.Style.Fill.BackgroundColor.SetColor(Color.White);

                            if (contactCustomField != null)
                            {
                                cell.Value = contactCustomField.Value;
                            }
                            rowCustomFieldsOffset++;
                        }

                        contactOffset++;
                    }
                }


                package.Save();
            }
        }

        private int FillReport<T>(IEnumerable<T> exportItems, ExcelWorksheet worksheet, HashSet<string> fields = null)
        {
            var exportArray = exportItems
                .Where(x => x != null)
                .ToArray();

            var properties = exportItems.GetType().GetGenericArguments()[0].GetProperties().Where(_ => _.Name != "Id").ToArray();
            if (fields != null && fields.Any())
            {
                properties = properties.Where(x => fields.Contains(x.Name)).ToArray();
            }

            var columnNames = properties.Select(x =>
            {
                var name = x.GetCustomAttribute<DisplayNameAttribute>(true)?.DisplayName;
                return name ?? x.Name;
            }).ToArray();

            var headerColor = ColorTranslator.FromHtml(HeaderColor);
            var rowColor = ColorTranslator.FromHtml(EvenRowColor);

            // fill headers
            for (var i = 0; i < columnNames.Length; ++i)
            {
                var cell = worksheet.Cells[1, i + 1];
                cell.Value = columnNames[i];
                cell.Style.Font.Bold = true;
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(headerColor.R, headerColor.G, headerColor.B));
                cell.Style.Font.Color.SetColor(Color.White);

                cell.Style.Border.Top.Style = cell.Style.Border.Left.Style =
                    cell.Style.Border.Right.Style = cell.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            }

            var totalRows = exportArray.Length;
            var exportedValuesMatrix = new List<object[]>();

            foreach (var exportModel in exportArray)
            {
                var exportedValues = new List<object>();

                foreach (var prop in properties)
                {
                    var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                    var value = prop.GetValue(exportModel, null);
                    var textValue = value?.ToString();

                    if (type.IsEnum)
                    {
                        textValue = Enum.GetName(type, value)?.SplitCamelCase();
                    }
                    else if (value == null &&
                        (type == typeof(byte) || type == typeof(sbyte) || type == typeof(short) || type == typeof(ushort) || type == typeof(int) ||
                        type == typeof(uint) || type == typeof(long) || type == typeof(ulong)))
                    {
                        textValue = "0";
                    }
                    else if (textValue == null && type == typeof(bool))
                    {
                        textValue = "false";
                    }
                    else if (type == typeof(DateTime))
                    {
                        textValue = ((DateTime?)value)?.AddHours(3).ToString("yyyy'-'MM'-'dd HH':'mm':'ss");
                    }
                    else if (type != typeof(string) && type.IsClass)
                    {
                        textValue = string.Empty;
                    }

                    exportedValues.Add(textValue);
                }

                exportedValuesMatrix.Add(exportedValues.ToArray());
            }

            if (exportedValuesMatrix.Any())
            {
                worksheet.Cells[2, 1].LoadFromArrays(exportedValuesMatrix);
            }

            var allCells = worksheet.Cells[1, 1, totalRows + 1, properties.Length];
            allCells.AutoFitColumns();
            allCells.Style.Border.Top.Style = allCells.Style.Border.Right.Style = allCells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            for (var i = 2; i <= totalRows + 1; i += 2)
            {
                var cells = worksheet.Cells[i, 1, i, properties.Length];
                cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cells.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(rowColor.R, rowColor.G, rowColor.B));
            }

            return columnNames.Length;
        }

    }
}
