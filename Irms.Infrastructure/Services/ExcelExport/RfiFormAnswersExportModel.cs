﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Infrastructure.Services.ExcelExport
{
    public class RfiFormAnswersExportModel
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public Guid ContactId { get; set; }
        public string Answer { get; set; }
        public string GuestName { get; set; }
        public string SubmissionDate { get; set; }
        public int SortOrder { get; set; }
    }
}
