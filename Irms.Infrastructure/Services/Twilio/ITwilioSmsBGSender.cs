﻿using Irms.Application.Abstract.Services.Notifications;

namespace Irms.Infrastructure.Services.Twilio
{
    public interface ITwilioSmsBGSender : ISmsBGSender
    {
    }
}
