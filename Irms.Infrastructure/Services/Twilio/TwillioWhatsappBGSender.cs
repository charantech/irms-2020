﻿using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Events.Commands;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using static Twilio.Rest.Api.V2010.Account.Call.FeedbackSummaryResource;

namespace Irms.Infrastructure.Services.Twilio
{
    public class TwillioWhatsappBGSender : IWhatsappBGSender
    {
        private readonly ITwilioBGConfigurationProvider _configurationProvider;
        private readonly ILogger<TwillioWhatsappSender> _logger;
        private readonly IConfiguration _config;
        private readonly IMediator _mediator;

        public TwillioWhatsappBGSender(
            ITwilioBGConfigurationProvider configurationProvider,
            ILogger<TwillioWhatsappSender> logger,
            IConfiguration config,
            IMediator mediator)
        {
            _configurationProvider = configurationProvider;
            _logger = logger;
            _mediator = mediator;
            _config = config;
        }

        public async Task<bool> SendWhatsappMessage(Guid tenantId, WhatsappMessage message, CancellationToken token)
        {
            try
            {
                var cfg = await _configurationProvider.GetConfiguration(tenantId, token);
                if (!cfg.IsValid)
                {
                    throw new Exception("Twilio is not configured");
                }
                var recipients = message.Recipients.Select(x => x.Phone).First();
                var body = FillPlaceholders(message)
                    .Replace("{", "")
                    .Replace("}", "")
                    .Replace("\r\n", "\n");


                TwilioClient.Init(cfg.AccountSid, cfg.AuthToken);
                var msg = new CreateMessageOptions(new PhoneNumber(ToWhatsappNumber(recipients)));
                msg.From = new PhoneNumber(ToWhatsappNumber(cfg.NotificationServiceId));
                msg.Body = Encoding.UTF8.GetString(Encoding.Default.GetBytes(body));

                var messageResponse = MessageResource.Create(msg);

                if (messageResponse.Status != StatusEnum.Failed)
                {
                    foreach (var recipient in message.Recipients)
                    {
                        _logger.LogInformation("Whatsapp message to number {0} has been sent", recipient.Phone, message);
                    }

                    var cmd = new ProviderLogsCmd
                    {
                        ProviderType = ProviderType.Twilio,
                        TenantId = tenantId,
                        Request = JsonConvert.SerializeObject(message),
                        RequestDate = DateTime.UtcNow,
                        Response = JsonConvert.SerializeObject(msg),
                        ResponseDate = DateTime.UtcNow,
                        CampaignInvitationId = message.CampaignInvitationId
                    };

                    await _mediator.Send(cmd, token);
                    return true;
                }
                else
                {
                    _logger.LogError("Whatsapp sending failed, Twilio error: " + messageResponse.ErrorMessage);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Whatsapp sending failed, Twilio error: " + ex.Message);
                return false;
            }
        }

        public async Task<bool> SendWhatsappWebhookMessage(Guid tenantId, WhatsappMessage message, CancellationToken token)
        {
            try
            {
                var cfg = await _configurationProvider.GetConfiguration(tenantId, token);
                if (!cfg.IsValid)
                {
                    throw new Exception("Twilio is not configured");
                }
                var recipients = message.Recipients.Select(x => x.Phone).First();
                var body = FillPlaceholders(message)
                    .Replace("{", "")
                    .Replace("}", "")
                    .Replace("\r\n", "\n");

                TwilioClient.Init(cfg.AccountSid, cfg.AuthToken);
                var msg = new CreateMessageOptions(new PhoneNumber(ToWhatsappNumber(recipients)));
                msg.From = new PhoneNumber(ToWhatsappNumber(cfg.NotificationServiceId));
                msg.Body = Encoding.UTF8.GetString(Encoding.Default.GetBytes(body));
                msg.StatusCallback = new Uri($"{_config["JwtAuthority:WebSite"]}/api/webhook/twilio");

                var messageResponse = MessageResource.Create(msg);
                if (messageResponse.Status != StatusEnum.Failed)
                {
                    message.MessageId = messageResponse.Sid;
                    message.ProviderType = ProviderType.Twilio;

                    foreach (var recipient in message.Recipients)
                    {
                        _logger.LogInformation("Whatsapp message to number {0} has been sent", recipient.Phone, message);
                    }

                    var cmd = new ProviderLogsCmd
                    {
                        ProviderType = ProviderType.Twilio,
                        TenantId = tenantId,
                        Request = JsonConvert.SerializeObject(message),
                        RequestDate = DateTime.UtcNow,
                        Response = JsonConvert.SerializeObject(msg),
                        ResponseDate = DateTime.UtcNow,
                        CampaignInvitationId = message.CampaignInvitationId
                    };

                    await _mediator.Send(cmd, token);
                    return true;
                }
                else
                {
                    _logger.LogError("Whatsapp sending failed, Twilio error: " + messageResponse.ErrorMessage);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Whatsapp sending failed, Twilio error: " + ex.Message);
                return false;
            }
        }

        private static string FillPlaceholders(WhatsappMessage message)
        {
            var messageText = message.MessageTemplate;
            foreach (var variable in message.Recipients.First().TemplateVariables)
            {
                messageText = messageText.Replace(variable.Name, variable.Value);
            }

            return messageText;
        }

        private static string ToWhatsappNumber(string number)
        {
            return $"whatsapp:{number}";
        }
    }
}
