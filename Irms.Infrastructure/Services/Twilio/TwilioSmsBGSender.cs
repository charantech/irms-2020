﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Irms.Application.Abstract.Services.Notifications;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using static Twilio.Rest.Api.V2010.Account.Call.FeedbackSummaryResource;
using Microsoft.Extensions.Configuration;
using Irms.Application.Events.Commands;
using Irms.Domain.Entities;
using Newtonsoft.Json;
using MediatR;

namespace Irms.Infrastructure.Services.Twilio
{
    public class TwilioSmsBGSender : ITwilioSmsBGSender
    {
        private readonly Regex _notNumber = new Regex("[^0-9]+", RegexOptions.Compiled);
        private readonly Regex _isUnicode = new Regex("[^\x00-\x7F]+", RegexOptions.Compiled);

        private readonly ITwilioBGConfigurationProvider _configurationProvider;
        private readonly ILogger<TwilioSmsBGSender> _logger;
        private readonly IConfiguration _config;
        private readonly IMediator _mediator;
        
        public TwilioSmsBGSender(
            ITwilioBGConfigurationProvider configurationProvider,
            ILogger<TwilioSmsBGSender> logger,
            IConfiguration config,
            IMediator mediator)
        {
            _configurationProvider = configurationProvider;
            _logger = logger;
            _mediator = mediator;
            _config = config;
        }

        public async Task<bool> SendSms(Guid tenantId, SmsMessage message, CancellationToken token)
        {
            var cfg = await _configurationProvider.GetConfiguration(tenantId, token);
            if (!cfg.IsValid)
            {
                throw new Exception("Twilio SMS is not configured");
            }

            var recipients = message.Recipients.Select(x => x.Phone).First();
            var text = FillPlaceholders(message);

            try
            {
                TwilioClient.Init(cfg.AccountSid, cfg.AuthToken);
                var msg = MessageResource.Create(
                    body: text,
                    from: new PhoneNumber(cfg.NotificationServiceId),
                    to: new PhoneNumber(recipients));

                if (msg.Status != StatusEnum.Failed)
                {
                    message.MessageId = msg.Sid;
                    message.ProviderType = ProviderType.Twilio;

                    foreach (var recipient in message.Recipients)
                    {
                        _logger.LogInformation("SMS to number {0} has been sent", recipient.Phone, message);
                    }

                    var cmd = new ProviderLogsCmd
                    {
                        ProviderType = ProviderType.Twilio,
                        TenantId = tenantId,
                        Request = JsonConvert.SerializeObject(message),
                        RequestDate = DateTime.UtcNow,
                        Response = JsonConvert.SerializeObject(msg),
                        ResponseDate = DateTime.UtcNow,
                        CampaignInvitationId = message.CampaignInvitationId
                    };

                    await _mediator.Send(cmd, token);
                    return true;
                }
                else
                {
                    _logger.LogError("SMS sending failed, Twilio error: " + msg.ErrorMessage);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("SMS sending failed, Twilio error: " + ex.Message);
                return false;
            }
        }

        public async Task<bool> SendWebhookSms(Guid tenantId, SmsMessage message, CancellationToken token)
        {
            var cfg = await _configurationProvider.GetConfiguration(tenantId, token);
            if (!cfg.IsValid)
            {
                throw new Exception("Twilio SMS is not configured");
            }

            var recipients = message.Recipients.Select(x => x.Phone).First();
            var text = FillPlaceholders(message);

            try
            {
                TwilioClient.Init(cfg.AccountSid, cfg.AuthToken);
                var msg = MessageResource.Create(
                    body: text,
                    statusCallback: new Uri($"{_config["JwtAuthority:WebSite"]}/api/webhook/twilio"),
                    from: new PhoneNumber(cfg.NotificationServiceId),
                    to: new PhoneNumber(recipients));

                if (msg.Status != StatusEnum.Failed)
                {
                    message.MessageId = msg.Sid;
                    message.ProviderType = ProviderType.Twilio;

                    foreach (var recipient in message.Recipients)
                    {
                        _logger.LogInformation("SMS to number {0} has been sent", recipient.Phone, message);
                    }

                    var cmd = new ProviderLogsCmd
                    {
                        ProviderType = ProviderType.Twilio,
                        TenantId = tenantId,
                        Request = JsonConvert.SerializeObject(message),
                        RequestDate = DateTime.UtcNow,
                        Response = JsonConvert.SerializeObject(msg),
                        ResponseDate = DateTime.UtcNow,
                        CampaignInvitationId = message.CampaignInvitationId
                    };

                    await _mediator.Send(cmd, token);
                    return true;
                }
                else
                {
                    _logger.LogError("SMS sending failed, Twilio error: " + msg.ErrorMessage);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("SMS sending failed, Twilio error: " + ex.Message);
                return false;
            }
        }


        private static string FillPlaceholders(SmsMessage message)
        {
            var messageText = message.MessageTemplate;
            foreach (var variable in message.Recipients.First().TemplateVariables)
            {
                messageText = messageText.Replace(variable.Name, variable.Value);
            }

            return messageText;
        }
    }
}
