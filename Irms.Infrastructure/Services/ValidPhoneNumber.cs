﻿using Irms.Application.Abstract.Services;

namespace Irms.Infrastructure.Services
{
    internal class ValidPhoneNumber : IValidPhoneNumber
    {
        public ValidPhoneNumber(string number)
        {
            Number = number;
        }

        public string Number { get; }
    }
}
