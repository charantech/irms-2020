﻿using Irms.Application.Abstract;
using Microsoft.Azure.Management.Dns;
using Microsoft.Azure.Management.Dns.Models;
using Microsoft.Rest.Azure.Authentication;
using Nager.PublicSuffix;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public class DNSRecordsManager : IDNSRecordsManager
    {
        private readonly Microsoft.Extensions.Configuration.IConfiguration _config;
        private readonly IDomainParser _domainParser;

        public DNSRecordsManager(Microsoft.Extensions.Configuration.IConfiguration config,
            IDomainParser domainParser)
        {
            _config = config;
            _domainParser = domainParser;
        }

        /// <summary>
        /// add subdomain to azure dns zone
        /// </summary>
        /// <param name="subDomain"></param>
        /// <returns></returns>
        public async Task AddSubDomain(string subDomain)
        {
            subDomain = _domainParser.GetSubDomain(subDomain);

            var dnsClient = await GetDNSClient();

            // Create record set parameters
            var recordSetParams = new RecordSet();
            recordSetParams.TTL = 3600;

            // Add records to the record set parameter object.  In this case, we'll add a record of type 'A'
            recordSetParams.ARecords = new List<ARecord>();
            recordSetParams.ARecords.Add(new ARecord(_config["AzureDNSZone:AppIP"]));

            _ = await dnsClient.RecordSets.CreateOrUpdateAsync(_config["AzureDNSZone:ResourceGroupName"], _config["AzureDNSZone:ZoneName"], subDomain, RecordType.A, recordSetParams);
        }

        /// <summary>
        /// delete sub domain
        /// </summary>
        /// <param name="clientUrl"></param>
        /// <returns></returns>
        public async Task DeleteSubDomain(string sub)
        {
            sub = _domainParser.GetSubDomain(sub);
            var dnsClient = await GetDNSClient();

            //delete old sub domain
            await dnsClient.RecordSets.DeleteAsync(_config["AzureDNSZone:ResourceGroupName"], _config["AzureDNSZone:ZoneName"], sub, RecordType.A);
        }

        /// <summary>
        /// update sub domain record
        /// </summary>
        /// <param name="subDomain"></param>
        /// <returns></returns>
        public async Task UpdateSubDomain(string oldSubDomain, string newSubDomain)
        {
            //delete sub domain
            await DeleteSubDomain(oldSubDomain);

            //addd new sub domain
            await AddSubDomain(newSubDomain);
        }

        private async Task<DnsManagementClient> GetDNSClient()
        {
            // Build the service credentials and DNS management client
            var serviceCreds = await ApplicationTokenProvider.LoginSilentAsync(_config["AzureDNSZone:TenantId"], _config["AzureDNSZone:ClientId"], _config["AzureDNSZone:Secret"]);
            var dnsClient = new DnsManagementClient(serviceCreds);
            dnsClient.SubscriptionId = _config["AzureDNSZone:SubscriptionId"];
            return dnsClient;
        }
    }
}
