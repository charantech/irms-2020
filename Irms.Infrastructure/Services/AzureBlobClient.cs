﻿using Irms.Domain;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    internal sealed class AzureBlobClient
    {
        private CloudBlobClient _blobClient;

        /// <summary>
        /// construct to setup configurations
        /// </summary>
        /// <param name="connectionstring"></param>
        /// <param name="storeBlobContentMD5"></param>
        public AzureBlobClient(string connectionstring, bool storeBlobContentMD5 = false)
        {
            var storageAccount = CloudStorageAccount.Parse(connectionstring);
            _blobClient = storageAccount.CreateCloudBlobClient();
            _blobClient.DefaultRequestOptions.StoreBlobContentMD5 = storeBlobContentMD5;
        }

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <param name="containerName">Name of the container. (should contains only lower case letters a-z)</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileContent">Content of the file.</param>
        /// <returns></returns>
        public async Task UploadFile(string containerName, string fileName, Stream fileContent)
        {
            try
            {
                fileContent.Position = 0;
                var block = await GetFileBlock(containerName, fileName);
                await block.UploadFromStreamAsync(fileContent);
            }
            catch
            {
                throw new IncorrectRequestException("Error during uploading file to storage.");
            }
        }


        /// <summary>
        /// Downloads the file.
        /// </summary>
        /// <param name="containerName">Name of the container. (should contains only lower case letters a-z)</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public async Task<byte[]> DownloadFile(string containerName, string fileName)
        {
            var block = await GetFileBlock(containerName, fileName);

            using (var stream = new MemoryStream())
            {
                await block.DownloadToStreamAsync(stream);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="containerName">Name of the container. (should contains only lower case letters a-z)</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public async Task DeleteFile(string containerName, string fileName)
        {
            var block = await GetFileBlock(containerName, fileName);
            try
            {
                await block.DeleteAsync();
            }
            catch { }
        }

        /// <summary>
        /// private method get file block/container in azure
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private async Task<CloudBlockBlob> GetFileBlock(string containerName, string fileName)
        {
            var container = _blobClient.GetContainerReference(containerName);
            await container.CreateIfNotExistsAsync();
            return container.GetBlockBlobReference(fileName);
        }
    }
}