﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Irms.Data")]
[assembly: InternalsVisibleTo("Irms.Data.Read")]
[assembly: InternalsVisibleTo("Irms.Tests.Unit")]
[assembly: InternalsVisibleTo("Irms.Tests.Integration")]
namespace Irms.Domain
{
    public interface IMarker
    {
    }
}
