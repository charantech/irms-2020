﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Irms.Domain.Entities.Templates
{
    public class Template : IEntity<Guid>
    {
        public Template(string name,
            bool emailCompatible,
            bool smsCompatible,
            string emailBody,
            string emailSubject,
            string smsText,
            TemplateType type,
            IEnumerable<TemplateTranslation> translations)
        {
            Id = Guid.NewGuid();
            Name = name;
            EmailCompatible = emailCompatible;
            SmsCompatible = smsCompatible;
            EmailBody = emailBody;
            EmailSubject = emailSubject;
            SmsText = smsText;
            Type = type;
            Translations = translations.ToImmutableList();
        }

        internal Template(
            Guid id,
            string name,
            bool emailCompatible,
            bool smsCompatible,
            string emailBody,
            string emailSubject,
            string smsText,
            TemplateType type,
            IEnumerable<TemplateTranslation> translations)
        {
            Id = id;
            Name = name;
            EmailCompatible = emailCompatible;
            SmsCompatible = smsCompatible;
            EmailBody = emailBody;
            EmailSubject = emailSubject;
            SmsText = smsText;
            Type = type;
            Translations = translations.ToImmutableList();
        }

        public Template(Guid id, string name, bool smsCompatible, string smsText, TemplateType type, IEnumerable<TemplateTranslation> translations)
        {
            Id = id;
            Name = name;
            SmsCompatible = smsCompatible;
            SmsText = smsText;
            Type = type;
            Translations = translations.ToImmutableList();
        }

        public Template(Guid id, string name, bool emailCompatible, string emailBody, string emailSubject, TemplateType type, IEnumerable<TemplateTranslation> translations)
        {
            Id = id;
            Name = name;
            EmailCompatible = emailCompatible;
            EmailBody = emailBody;
            EmailSubject = emailSubject;
            Type = type;
            Translations = translations.ToImmutableList();
        }

        public Guid Id { get; }
        public string Name { get; private set; }

        public bool EmailCompatible { get; private set; }
        public bool SmsCompatible { get; private set; }

        public string EmailBody { get; private set; }
        public string EmailSubject { get; private set; }

        public string SmsText { get; private set; }

        public TemplateType Type { get; }

        public ImmutableList<TemplateTranslation> Translations { get; private set; }

        public IEnumerable<TemplateTranslation> TranslationList => Translations;

        public void Update(string name,
            bool emailCompatible,
            bool smsCompatible,
            string emailBody,
            string emailSubject,
            string smsText,
            IEnumerable<TemplateTranslation> translations)
        {
            Name = name;
            EmailCompatible = emailCompatible;
            SmsCompatible = smsCompatible;
            EmailBody = emailBody;
            EmailSubject = emailSubject;
            SmsText = smsText;
            Translations = translations.ToImmutableList();
        }

        public void Translate(Guid? languageId)
        {
            if (!languageId.HasValue)
            {
                return;
            }

            var tr = Translations.FirstOrDefault(x => x.LanguageId == languageId.Value);
            if (tr == null)
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(tr.EmailBody))
            {
                EmailBody = tr.EmailBody;
            }

            if (!string.IsNullOrWhiteSpace(tr.EmailSubject))
            {
                EmailSubject = tr.EmailSubject;
            }

            if (!string.IsNullOrWhiteSpace(tr.EmailSubject))
            {
                SmsText = tr.SmsText;
            }
        }
    }
}
