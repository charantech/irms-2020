﻿namespace Irms.Domain.Entities
{
    public enum CampaignType : int
    {
        Campaign = 0,
        ListAnalysis = 1,
    }
}
