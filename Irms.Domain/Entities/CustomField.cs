﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class CustomField : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string FieldName { get; set; }
        public CustomFieldType CustomFieldType { get; set; }
        public long? MinValue { get; set; }
        public long? MaxValue { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
        }

        public void Update()
        {
            ModifiedOn = DateTime.UtcNow;
        }
    }
}
