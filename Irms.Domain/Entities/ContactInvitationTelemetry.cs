﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class ContactInvitationTelemetry : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public Guid ContactId { get; set; }
        public DateTime OpenedOn { get; set; }
        public MediaType MediaType { get; set; }

        public ContactInvitationTelemetry()
        {
            Id = Guid.NewGuid();
            OpenedOn = DateTime.UtcNow;
        }
    }
}
