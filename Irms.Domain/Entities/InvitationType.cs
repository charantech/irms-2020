﻿namespace Irms.Domain.Entities
{
    public enum InvitationType
    {
        Rsvp = 0,
        Pending = 1,
        Accepted = 2,
        Rejected = 3,
        ListAnalysis = 4
    }
}
