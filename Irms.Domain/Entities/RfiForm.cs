﻿using System;
using System.Collections.Immutable;
using System.Linq;
using Irms.Domain.Abstract;
using Newtonsoft.Json;

namespace Irms.Domain.Entities
{
    public class RfiForm : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid? CampaignInvitationId { get; set; }
        public string WelcomeHtml { get; set; }
        public string SubmitHtml { get; set; }
        public string ThanksHtml { get; set; }
        public string FormTheme { get; set; }
        public string FormSettings { get; set; }
        public string ThemeBackgroundImagePath { get; set; }
        public ImmutableList<RfiFormQuestion> RfiFormQuestions { get; set; }
        public ImmutableList<RfiFormResponse> RfiFormResponses { get; set; }

        /// <summary>
        /// intializing data
        /// </summary>
        public void Create(string questionsJson)
        {
            Id = Guid.NewGuid();
            RfiFormQuestions = JsonConvert.DeserializeObject<ImmutableList<RfiFormQuestion>>(questionsJson);

            if (!RfiFormQuestions.Any())
            {
                throw new IncorrectRequestException("No question found");
            }
        }

        public void Update(string questionsJson)
        {
            RfiFormQuestions = JsonConvert.DeserializeObject<ImmutableList<RfiFormQuestion>>(questionsJson);

            if (!RfiFormQuestions.Any())
            {
                throw new IncorrectRequestException("No question found");
            }
        }
    }

    public class RfiFormQuestion
    {
        public Guid Id { get; set; }
        public Guid RfiFormId { get; set; }
        public string Question { get; set; }
        public bool Mapped { get; set; }
        public string MappedField { get; set; }
        public int SortOrder { get; set; }
    }

    public class RfiFormResponse
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid RfiFormId { get; set; }
        public Guid QuestionId { get; set; }
        public Guid ContactId { get; set; }
        public string Answer { get; set; }
        public DateTime ResponseDate { get; set; }
    }
}
