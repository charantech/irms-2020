﻿using System;

namespace Irms.Domain.Entities
{
    public class CampaignInvitationMessageLog
    {
        public Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public int? InvitationTemplateTypeId { get; set; }
        public int? ProviderTypeId { get; set; }
        public int MessageStatus { get; set; }

        public string MessageText { get; set; }
        public DateTime TimeStamp { get; set; }
        public string MessageId { get; set; }
    }
}
