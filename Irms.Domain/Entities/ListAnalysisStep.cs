﻿namespace Irms.Domain.Entities
{
    public enum ListAnalysisStep
    {
        ImportantFields = 0,
        ListAnalysis = 1,
        CollectImportantFields = 2
    }
}
