﻿namespace Irms.Domain.Entities
{
    public enum InvitationMessageStatus
    {
        Draft = 0,
        InProgress = 1,
        Triggered = 2,
        Sent = 3,
        Delivered = 4,
        Processed = 5,
        Open = 6,
        Click = 7,
        Dropped = 8,
        Deferred = 9,
        Bounce = 10,
        Unsubscribed = 11,
        SpamReported = 12,
        Undelivered = 13,
        Queued = 14,
        Rejected = 15,
        Failed = 16,
        Undeliverable = 17,
        Blocked = 18,
        NoResponse = 19,
        Read = 20
    }
}
