﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class ContactListToContact : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactListId { get; set; }
        public Guid ContactId { get; set; }
     
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// intializing ids, dates
        /// </summary>
        public void Create(Guid tenantId)
        {
            Id = Guid.NewGuid();
            TenantId = tenantId;
            CreatedOn = DateTime.UtcNow;
        }

        public void SetContactListId(Guid contactListId)
        {
            ContactListId = contactListId;
        }

        public void SetContactId(Guid contactId)
        {
            ContactId = contactId;
        }

        public void Update()
        {
            ModifiedOn = DateTime.UtcNow;
        }
    }
}
