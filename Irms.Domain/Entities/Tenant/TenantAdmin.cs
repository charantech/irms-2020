﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities.Tenant
{
    public class TenantAdmin : IEntity<Guid>
    {
        public TenantAdmin(
            Guid tenantId,
            string email,
            string phone,
            string fullName,
            Guid userId)
        {
            Id = Guid.NewGuid();
            Email = email;
            FullName = fullName;
            UserId = userId;
            TenantId = tenantId;
            Phone = phone;
        }

        internal TenantAdmin(
            Guid id,
            Guid tenantId,
            string email,
            string phone,
            string fullName,
            Guid userId)
        {
            Id = id;
            Email = email;
            FullName = fullName;
            UserId = userId;
            TenantId = tenantId;
            Phone = phone;
        }

        public Guid Id { get; }
        public Guid TenantId { get; }
        public string Email { get; }
        public string Phone { get; }
        public string FullName { get; }
        public Guid UserId { get; }
    }
}
