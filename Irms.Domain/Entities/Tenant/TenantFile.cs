﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities.Tenant
{
    public class TenantFile : IEntity<Guid>
    {
        public TenantFile(
            string name,
            string mimeType,
            string description,
            Guid createdBy,
            byte[] data,
            byte[] thumbnail,
            Guid tenantId)
        {
            Id = Guid.NewGuid();
            Name = name;
            MimeType = mimeType;
            Description = description;
            CreatedBy = createdBy;
            Data = data;
            Thumbnail = thumbnail;
            TenantId = tenantId;
        }

        public Guid Id { get; }
        public Guid TenantId { get; }
        public string Name { get; }
        public string MimeType { get; }
        public string Description { get; }
        public Guid CreatedBy { get; }
        public byte[] Data { get; }
        public byte[] Thumbnail { get; }
    }
}
