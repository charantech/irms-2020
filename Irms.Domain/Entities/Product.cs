﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace Irms.Domain.Entities
{
    public class Product : IEntity<Guid>
    {
        public Product()
        {
            Id = Guid.NewGuid();
        }

        public Product(
            Guid id,
            string title,
            decimal price,
            decimal? discountPercentage,
            bool isShowOnWeb,
            byte currencyId,
            LicensePeriod license,
            List<ServiceCatalog> services = null)
        {
            Id = id;
            Title = title;
            Price = price;
            DiscountPercentage = discountPercentage;
            IsShowOnWeb = isShowOnWeb;
            CurrencyId = currencyId;
            ServicesCatalog = services ?? new List<ServiceCatalog>();
            License = license;
        }

        public Product(
            Guid id,
            string title,
            decimal price,
            decimal? discountPercentage,
            decimal? discountAmount,
            decimal finalPrice,
            bool isShowOnWeb,
            Guid? createdById,
            DateTime? createdOn,
            Guid? modifiedById,
            DateTime? modifiedOn,
            byte currencyId,
            bool isDeleted,
            List<ServiceCatalog> services = null)
        {
            Id = id;
            Title = title;
            Price = price;
            DiscountPercentage = discountPercentage;
            DiscountAmount = discountAmount;
            FinalPrice = finalPrice;
            IsShowOnWeb = isShowOnWeb;
            CreatedById = createdById;
            CreatedOn = createdOn;
            ModifiedById = modifiedById;
            ModifiedOn = modifiedOn;
            CurrencyId = currencyId;
            ServicesCatalog = services ?? new List<ServiceCatalog>();
            IsDeleted = isDeleted;
        }

        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public decimal Price { get; private set; }
        public decimal? DiscountPercentage { get; private set; }
        public decimal? DiscountAmount { get; private set; }
        public decimal FinalPrice { get; private set; }
        public bool IsShowOnWeb { get; private set; }
        public Guid? CreatedById { get; private set; }
        public DateTime? CreatedOn { get; private set; }
        public Guid? ModifiedById { get; private set; }
        public DateTime? ModifiedOn { get; private set; }
        public LicensePeriod License { get; private set; }
        public bool IsDeleted { get; private set; }
        public byte CurrencyId { get; private set; }
        public List<ServiceCatalog> ServicesCatalog { get; private set; }

        public void AddServiceToProduct(ServiceCatalog service)
        {
            if (ServicesCatalog == null)
                ServicesCatalog = new List<ServiceCatalog>();

            ServicesCatalog.Add(service);
        }
        public void SetServicesCatalog(List<ServiceCatalog> servicesCatalog)
        {
            if (ServicesCatalog == null)
                ServicesCatalog = new List<ServiceCatalog>();

            ServicesCatalog = servicesCatalog;
        }

        public void SetCurrency(Currency currency)
        {
            CurrencyId = currency.Id;
        }

        public void SetCurrency(byte currencyId)
        {
            CurrencyId = currencyId;
        }
        public void SetIdOfUserCreatedBy(Guid? userId)
        {
            CreatedById = userId;
        }
        public void SetIdOfUserUpdatedBy(Guid userId)
        {
            ModifiedById = userId;
        }
        public void SetLicense(LicensePeriod license)
        {
            License = license;
        }
        public void SetLicense(byte licenseId, string tytle, int? days)
        {
            License = new LicensePeriod(licenseId, tytle, days);
        }
    }
}
