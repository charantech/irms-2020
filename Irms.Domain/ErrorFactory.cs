﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain
{
    public static class ErrorFactory
    {
        public static IncorrectRequestException MissingOrIncorrectInformation =>
            new IncorrectRequestException(3, "Missing or incorrect information");

        public static IncorrectRequestException AvailabilityTimingDoesNotMatchEventSchedule =>
            new IncorrectRequestException(6, "Your availability timing does not match the event schedule. Please change your availability timing through your profile management if you would like to enroll in this event");

        public static IncorrectRequestException ThereArePrerequisiteEvent =>
            new IncorrectRequestException(7, "There are prerequisite event/s which you have not completed. Please complete the prerequisite event/s before enrolling to this event");

        public static IncorrectRequestException YouAreAlreadyEnrolledToEvent =>
            new IncorrectRequestException(8, "You are already enrolled to an event which is at the same timing as this event. You cannot enroll to this event due to time conflict");

        public static IncorrectRequestException MedicalConsentIsPrerequisite =>
            new IncorrectRequestException(9, "Medical consent is a prerequisite for this event. Please complete your online medical consent through your profile management before enrolling to this event");

        public static IncorrectRequestException MedicalCheckupsArePrerequisite =>
            new IncorrectRequestException(10, "Medical checkups are a prerequisite for this event. Please manage your medical checkup appointment through your profile management before enrolling to this event");

        public static IncorrectRequestException TrainingIsPrerequisite =>
            new IncorrectRequestException(11, "Training is a prerequisite for this event. Please manage the required training through your profile management before enrolling to this event");

        public static IncorrectRequestException TransactionFailed =>
            new IncorrectRequestException(13, "Transaction failed");
    }
}
